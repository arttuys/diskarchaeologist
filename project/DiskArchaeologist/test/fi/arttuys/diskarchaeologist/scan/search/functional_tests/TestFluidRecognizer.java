/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.search.functional_tests;

import fi.arttuys.diskarchaeologist.scan.search.FileTypeRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.functional.AbstractFluidRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.functional.AnalysisProperty;
import fi.arttuys.diskarchaeologist.scan.search.functional.FluidFileRecognizer;

/**
 *
 * A simple test recognizer; rejects all in-depth analyses, but accepts all potential and basic ones
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
@FluidFileRecognizer(priority = FileTypeRecognizer.Priority.ADVANCED)
public class TestFluidRecognizer extends AbstractFluidRecognizer {
    
    /**
     * Returns F1 always as a property
     * @return "F1"
     */
    @AnalysisProperty(name = "T1", category = "X")
    public String testRes() {
        return "F1";
    }
 
    /**
     * Always returns true as a property; with no category defined
     * @return True
     */
    @AnalysisProperty(name = "T2")
    public boolean test2() {
        return true;
    }

    /**
     * This property always throws an exceptiom but should not be inquired in basic analysis
     */
    @AnalysisProperty(name = "T3", inDepthOnly = true)
    public void xyz() {
        throw new RuntimeException("Should not be ran!");
    }
    
    @Override
    protected String getTypeIdentifier() {
        return "TestFile1";
    }

    @Override
    protected FileTypeRecognizer.AnalysisResultType analyze(FileTypeRecognizer.AnalysisLevel level) throws Exception {
        //Throw a fit if an InputStream is not available
        assertTrue(this.getBaseNode() != null);
        
        assertFalse(false);
        
        //This should always pass
        requireSize((t) -> {
            return t.equals(this.getBaseNode().getLocalSizeProperty().get()); //To change body of generated lambdas, choose Tools | Templates.
        });
        
        if (this.getInputStream() == null) throw new NullPointerException("Input stream unavailable!");
        return level == FileTypeRecognizer.AnalysisLevel.INDEPTH ? FileTypeRecognizer.AnalysisResultType.FILE_REJECTED : FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_FULL;
    }
    
}
