/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.search.functional_tests;

import fi.arttuys.diskarchaeologist.scan.search.FileTypeRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.functional.AbstractFluidRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.functional.FluidFileRecognizer;

/**
 *
 * Simple recognizer that triggers an assertion when potential, and an exception otherwise by returning a null
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
@FluidFileRecognizer(priority = FileTypeRecognizer.Priority.MULTITYPE)
public class TestFluidRecognizer_2 extends AbstractFluidRecognizer {

    @Override
    protected FileTypeRecognizer.AnalysisResultType analyze(FileTypeRecognizer.AnalysisLevel level) throws Exception {
        if (level == FileTypeRecognizer.AnalysisLevel.POTENTIAL) assertFalse(true);
        if (level == FileTypeRecognizer.AnalysisLevel.INDEPTH) requireSize((t) -> {
            return false; //To change body of generated lambdas, choose Tools | Templates.
        });
        return null;
    }

    @Override
    protected String getTypeIdentifier() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
}
