/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.search;

import fi.arttuys.diskarchaeologist.scan.node.FSNode;
import fi.arttuys.diskarchaeologist.scan.node.FSNodeBasicTest;
import java.io.FileNotFoundException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * Tests that the catchall file type recognizer seems to behave properly in a simple case
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class CatchallRecognizerTest {
    
    /**
     * Tests that a catchall recognizer does indeed recognize a file
     * @throws java.io.FileNotFoundException File not found is a fail
     */
    @Test
    public void testCatchallRecognizer() throws FileNotFoundException {
        FSNode subnode = FSNodeBasicTest.generateFileNode();
        
        CatchallFileNameRecognizer cr = new CatchallFileNameRecognizer();
        
        //Must be a catchall
        assertEquals(FileTypeRecognizer.Priority.CATCHALL, cr.getPriority());
        
        AnalysisResultObject aro = cr.analyze(FileTypeRecognizer.AnalysisLevel.POTENTIAL, subnode);
        
        //Must accept
        assertEquals(FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_POTENTIAL, aro.getResultType());
        
        //Must be a TXT file, as assumed for the FSNodeBasicTests files
        assertEquals("TXT file", aro.getTypeIdentifier());
    }
    
}
