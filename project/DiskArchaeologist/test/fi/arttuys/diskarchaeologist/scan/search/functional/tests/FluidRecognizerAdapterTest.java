/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.search.functional.tests;

import fi.arttuys.diskarchaeologist.scan.node.FSNodeBasicTest;
import fi.arttuys.diskarchaeologist.scan.search.AnalysisResultObject;
import fi.arttuys.diskarchaeologist.scan.search.functional_tests.TestFluidRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.FileTypeRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.functional.FluidRecognizerAdapter;
import fi.arttuys.diskarchaeologist.scan.search.functional_tests.TestFluidRecognizer_2;
import java.io.FileNotFoundException;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests that basic fluid recognizer properties function
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class FluidRecognizerAdapterTest {

    /**
     * Test that invalid classes are not accepted
     */
    @Test
    public void testInvalidClasses() {
        for (Class c : new Class[]{null, String.class}) {
            try {
                new FluidRecognizerAdapter(c);
                fail("Could instantiate a fluid recognizer adapter with an invalid class!");
            } catch (IllegalArgumentException iae) {
                //OK!
            }
        }
    }
    
    /**
     * Tests that the priority of a class is properly detected
     */
    @Test
    public void testPriority() {
        FluidRecognizerAdapter fra = new FluidRecognizerAdapter(TestFluidRecognizer.class);
        assertEquals(FileTypeRecognizer.Priority.ADVANCED, fra.getPriority());
        
        
    }
    
    /**
     * Tests that instantiation is possible and analysis does work
     * @throws java.io.FileNotFoundException No file-not-founds should occur
     */
    @Test
    public void testAnalyze() throws FileNotFoundException {
        FluidRecognizerAdapter fra = new FluidRecognizerAdapter(TestFluidRecognizer.class);
        
        Set<AnalysisResultObject.AnalysisEntry> keySet = fra.getPossibleSearchKeys();
        
        assertTrue(keySet.contains(new AnalysisResultObject.AnalysisEntry("X", "T1")));
        assertTrue(keySet.contains(new AnalysisResultObject.AnalysisEntry("", "T2")));
        assertTrue(keySet.contains(new AnalysisResultObject.AnalysisEntry("", "T3")));
        
        
        
        AnalysisResultObject aro = fra.analyze(FileTypeRecognizer.AnalysisLevel.BASIC, FSNodeBasicTest.generateFileNode());
        //TestFluidRecognizer is so simple that this should always hold
        assertEquals("Two subsequent scans of the same file should be consistent and equal", aro, fra.analyze(FileTypeRecognizer.AnalysisLevel.BASIC, FSNodeBasicTest.generateFileNode()));
        assertEquals("Priority must be as expected",FileTypeRecognizer.Priority.ADVANCED, fra.getPriority());
        assertFalse("Result must not be an error", aro.getResultType() == FileTypeRecognizer.AnalysisResultType.ANALYSIS_ERROR);
        assertEquals("Result must be expected for T1", aro.getProperties().get(new AnalysisResultObject.AnalysisEntry("X", "T1")), "F1");
        assertEquals("Result must be expected for T2", aro.getProperties().get(new AnalysisResultObject.AnalysisEntry("", "T2")), true);
        //No result for T3, as running basic, not in-depth
        assertFalse(aro.getProperties().containsKey(new AnalysisResultObject.AnalysisEntry("", "T3")));
        assertEquals("Type must be expected", "TestFile1", aro.getTypeIdentifier());
        //Let's try again
        aro = fra.analyze(FileTypeRecognizer.AnalysisLevel.INDEPTH, FSNodeBasicTest.generateFileNode());
        assertEquals("Result must be a rejection", FileTypeRecognizer.AnalysisResultType.FILE_REJECTED, aro.getResultType());
        assertTrue("Map must be a null", aro.getProperties() == null);
        assertTrue("Type identifier must be a null", aro.getTypeIdentifier() == null);
    }
    /**
     * Tests that errors are properly detected
     */
    @Test
    public void testErrors() throws FileNotFoundException {
        FluidRecognizerAdapter fra = new FluidRecognizerAdapter(TestFluidRecognizer_2.class);
        AnalysisResultObject aro = fra.analyze(FileTypeRecognizer.AnalysisLevel.POTENTIAL, FSNodeBasicTest.generateFileNode());
        assertEquals("Priority must be as expected",FileTypeRecognizer.Priority.MULTITYPE, fra.getPriority());
        assertTrue("Result must be a rejection", aro.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_REJECTED);
        
        aro = fra.analyze(FileTypeRecognizer.AnalysisLevel.BASIC, FSNodeBasicTest.generateFileNode());
        assertTrue("Result must be a error", aro.getResultType() == FileTypeRecognizer.AnalysisResultType.ANALYSIS_ERROR);
        
        aro = fra.analyze(FileTypeRecognizer.AnalysisLevel.INDEPTH, FSNodeBasicTest.generateFileNode());
        assertTrue("Result must be a refusal", aro.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_REJECTED);
    }
}
