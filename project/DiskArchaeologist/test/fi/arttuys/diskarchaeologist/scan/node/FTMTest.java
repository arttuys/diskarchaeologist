/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.node;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableMap;
import javafx.util.Pair;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * Elementary tests for FileTreeManager
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class FTMTest {
    
    private static FileTreeManager ftm;
    /**
     * Initialize the FTM before tests commence
     * @throws java.io.FileNotFoundException Should not occur
     */
    @BeforeClass
    public static void setUpClass() throws FileNotFoundException, IOException {
        ftm = new FileTreeManager(new File("."), null);
    }
    

    /**
     * Recursively asserts that a certain item is found via the tree; and if it is found, returns it
     * @param startNode Starting node
     * @param path Path
     * @return The node found
     */
    private FSNode assertSubnode(FSNode startNode, boolean targetIsDirectory, String... path) {
        
        System.out.println("Verifying: " + Arrays.toString(path));
        
        FSNode foundNode = null;
        
        for (FSNode subnode : startNode.getSubnodeProperty().get()) {
            if (subnode.getUnderlyingFile().getName().equals(path[0])) {
                //Found it
                foundNode = subnode;
                break;
            }
        }
        
        if (foundNode == null) fail("Could not find " + path[0] + " from " + startNode.getUnderlyingFile().getAbsolutePath());
        
        if (path.length > 1) {
            //Recursively verify.
            String[] newArray = Arrays.copyOfRange(path, 1, path.length);
            return assertSubnode(foundNode, targetIsDirectory, newArray);
        } else {
            //Assert that it is the same type
            assertEquals("Target must be of the same type", targetIsDirectory, foundNode.isDirectory());
        }
        
        return foundNode;
    }
    
    /**
     * Tests that FTM is able to accurately map and process a small set of folders. This includes several kinds of postprocessing functionality
     * @throws java.lang.Exception Absolutely NO exceptions should occur here
     */
    @Test
    public void testFunctionality() throws Exception {
        AtomicInteger resultInteger = new AtomicInteger(0);
        
        ftm.getStateProperty().addListener(new ChangeListener<FileTreeManager.State>() {
            @Override
            public void changed(ObservableValue<? extends FileTreeManager.State> observable, FileTreeManager.State oldValue, FileTreeManager.State newValue) {
                //First change should be to ENUMERATING
                if (resultInteger.intValue() == 0 && newValue == FileTreeManager.State.ENUMERATING) resultInteger.addAndGet(1);
                //2nd one, success
                if (resultInteger.intValue() == 1 && newValue == FileTreeManager.State.IDLE_SUCCESS) resultInteger.addAndGet(1);
            }
        });
        
        // Try to commit analysis before scanned
        try {
            ftm.wrapperSafeDirectoryPostProcessing(new BiFunction<FSNode, Map<FSNode, ObservableMap<String, Object>>, Callable>() {
                @Override
                public Callable apply(FSNode t, Map<FSNode, ObservableMap<String, Object>> u) {
                    //No-op
                    return null;
                }
            }, true, "OK", null, null);
            fail("Could trigger post-processing before scanning");
        } catch (IllegalStateException ie) {
            // OK
        }
        
         // Same with a simpler function
        try {
            ftm.simpleFilePostProcessing((t) -> {
            }, "OK", null);
            fail("Could trigger post-processing before scanning");
        } catch (IllegalStateException ie) {
            // OK
        }
        
        ftm.startScanAndAnalyze();
        ftm.waitUntilStopped();
        
        // Try to do a false restart
        try {
            ftm.startScanAndAnalyze();
            fail("FTM allowed a double restart!");
        } catch (IllegalStateException ie) {
            // OK
        }
        
        //while (!ftm.rootTaskCompletedSuccessfully()) {
        //    if (ftm.getRootTaskException() != null) {
        //        ftm.resetPool();
        //        throw ftm.getRootTaskException();
        //    }
        //}
        
        FSNode root = ftm.getRootNode();
        System.out.println("Size of subnode list: " + root.getSubnodeProperty().size());
        
        assertEquals("Enum must have changed to a valid value",2, resultInteger.intValue());
        
        //See FSNodeBasicTests for the construction! These calls assert that one can recursively find a subnode of the specified name and type
        assertSubnode(root, true, "t1");
        assertSubnode(root, true, "t2");
        
        FSNode f1 = assertSubnode(root, false, "t1", "f1.txt");
        assertEquals("assertSubnode returned the correct file", "f1.txt", f1.getUnderlyingFile().getName());
        FSNode f2 = assertSubnode(root, false, "t1", "f2.txt");
        FSNode f3 = assertSubnode(root, false, "t2", "f3.txt");
        
        // Also, be assured of the correct amount of subnodes for the root node
        
        assertEquals("Counting folder and file nodes, there must be exactly 5 nodes", "5", root.getSubnodeCount().get().toString());
        
        // Now, call for type data
        
        AtomicBoolean bool = new AtomicBoolean(false);
        
        ftm.executeFurtherAnalysis(() -> {
            bool.set(true);
        });
        
        ftm.waitUntilStopped();
        
        
        assertEquals( "Postprocessing call runs properly for further analysis", true, bool.get());
        
        // We should have 3 files - test the functionality of simple postprocessing
        
        AtomicInteger ix = new AtomicInteger(0);
        bool.set(false);
        
        ftm.simpleFilePostProcessing((t) -> {System.out.println(t.getUnderlyingFile().getAbsolutePath());ix.addAndGet(1);
        }, "", () -> {
            bool.set(true);
        });
        
        ftm.waitUntilStopped();
        
        assertEquals("Simple postprocessing counted all files", 3, ix.get());
        assertEquals( "Postprocessing call runs properly for simple postprocessing", true, bool.get());
        
        // Let's try something a bit more complicated. Generate a grid!
        
        // Try to generate an invalid grid
        try {
            ftm.generateNewGrid(0, 0, null);
            fail("Allowed a zero-length grid");
        } catch (IllegalArgumentException iae) {
            //OK
        }
        
        ftm.generateNewGrid(50, 50, null);
        ftm.waitUntilStopped();
        
        assertNotNull("FileGrid was non-null", ftm.getLatestGrid());
        
        // Check that our files have got properly placed
        FileGrid fg = ftm.getLatestGrid();
        assertEquals("FileGrid correctly counted files", 3, fg.getNodeToGridMap().keySet().size());
        
        // Each of these must have a nonzero amount of space used
        for (List<Pair<Integer, Integer>> list : fg.getNodeToGridMap().values()) {
            assertTrue("A nonzero amount of space used", !list.isEmpty());
        }
        
        // Now, let's assert some properties related to the grid's contents
        Set<FSNode> gridContents = new HashSet<>();
        
        // A good aligner should not result in more than 5% empty space. Let's test this
        int nodeLimit = (int) (0.05 * (50*50));
        int foundEmptyNodes = 0;
        for (int x = 0; x < 50; x++) {
            for (int y = 0; y < 50; y++) {
                FSNode node = fg.pointAtGrid(x, y);
                if (node == null) {
                    foundEmptyNodes++;
                    assertTrue("No more empty nodes than allowed", foundEmptyNodes <= nodeLimit);
                } else {
                    gridContents.add(node);
                }
            }
        }
        
        assertEquals("All files were found in the resulting grid content list", 3, gridContents.size());
        
        // Also, try to trigger advanced post-processing. This should not result in an error state
        ftm.executeFurtherAnalysis(null);
        ftm.waitUntilStopped();
        
        assertTrue("Advanced analylsis completed successfully", ftm.hasCleanState());
        
        // Let's test the search
        
        ftm.executeSearchQueries("[Name]: <f.\\.txt>"); // As searches are intepreted using regex, this should trigger all f*txt-files
        ftm.waitUntilStopped();
        
        for (FSNode node : new FSNode[]{f1,f2,f3}) {
            assertEquals("File nodes got picked up properly in the search", Boolean.TRUE, node.getExtendedPropertyMap().get(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY));
        }
        
        // Reset the results
        
        ftm.executeSearchQueries(""); // This should set things back to null
        ftm.waitUntilStopped();
        
        for (FSNode node : new FSNode[]{f1,f2,f3}) {
            assertNull("All results have reset", node.getReadOnlyExtendedPropertyMap().get(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY));
        }
        
         // Test that sizes are human readable - must contain "bytes" somewhere
        
        ftm.executeSearchQueries("[Size]: <^(0|1|2|3|4|5|6|7|8|9)+.*bytes$>"); // Require that sizes are in bytes
        ftm.waitUntilStopped();
        
        for (FSNode node : new FSNode[]{f1,f2,f3}) {
            assertEquals("File nodes got picked up properly in the search", Boolean.TRUE, node.getExtendedPropertyMap().get(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY));
        }
        
        // How about invalid content for key?
        
        ftm.executeSearchQueries("[Size]: <bytes>\n[Words]: <dsfsf>");
        ftm.waitUntilStopped();
        
        for (FSNode node : new FSNode[]{f1,f2,f3}) {
            assertEquals("All results negative, as expected", Boolean.FALSE, node.getExtendedPropertyMap().get(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY));
        }
        
        // How about bad keys?
        
        ftm.executeSearchQueries("[Size]: <bytes>\n[BLA]: <>");
        ftm.waitUntilStopped();
        
        for (FSNode node : new FSNode[]{f1,f2,f3}) {
            assertEquals("All results negative, as expected", Boolean.FALSE, node.getExtendedPropertyMap().get(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY));
        }
        
        // Combined queries must work too; this enforces both the size and the special filename
        
        ftm.executeSearchQueries("[Name]: <f(1|2).txt>\n[Size]: <^(0|1|2|3|4|5|6|7|8|9)+.*bytes$>"); // Require that sizes are in bytes
        ftm.waitUntilStopped();
        
        for (FSNode node : new FSNode[]{f1,f2}) {
            assertEquals("File nodes got picked up properly in the search", Boolean.TRUE, node.getExtendedPropertyMap().get(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY));
        }
        
        for (FSNode node : new FSNode[]{f3}) {
            assertEquals("File nodes got picked up properly in the search", Boolean.FALSE, node.getExtendedPropertyMap().get(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY));
        }
        
        // How about an invalid size?
        
        ftm.executeSearchQueries("[Size]: <yottabytes>");
        ftm.waitUntilStopped();
        
        for (FSNode node : new FSNode[]{f1,f2,f3}) {
            assertEquals("All results negative, as expected", Boolean.FALSE, node.getExtendedPropertyMap().get(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY));
        }
    }
}
