/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra <arttuys@arttuys.fi>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.node;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * Basic tests for FSNode; tests certain properties expected of them
 * <p>
 * Unlike most tests, FSNodeBasicTests require certain preparations. There must be following things:
 * <p>
 * <pre>
 * - Folders t1, t2 must exist, t3 must not exist
 * - In t1, files f1.txt and f2.txt must exist
 * - In t2, file f3.txt must exist
 * </pre>
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra <arttuys@arttuys.fi>
 */
public class FSNodeBasicTest {

    
    /**
     * The root path for finding files
     */
    public static final String ROOT_PATH_PREFIX = "./";
    
    /**
     * Folders that are required to be existent
     */
    public static final String[] VALID_FOLDERS = {"t1", "t2"};
    /**
     * Folders that are required to be nonexistent
     */
    public static final String[] INVALID_FOLDERS = {"t3"};
    
    /**
     * Files that are required to exist; first 2 are related to the first folder, 3rd to the second
     */
    public static final String[] VALID_NORMAL_FILES = {"t1/f1.txt", "t1/f2.txt", "t2/f3.txt"};
   
    /**
     * Check if a certain pathname is existent; and if it is a file, that it is nonzero in length
     * @param path Path to a file
     * @param folder Must this item be a folder (if existent?)
     * @param mustExist TRUE if this item must be existent, FALSE if it must be nonexistent
     */
    private static void checkIfValidItem(String path, boolean folder, boolean mustExist) {
        File testableFile = new File(path);
        
        assertEquals("Existence of '" + path + "' must match to the parameter", mustExist, testableFile.exists());
        
        if (!mustExist) return; //Nonexistent, end here
        
        //Check that is a folder or a file
        assertEquals("Must be a " + (folder ? "folder" : "file"), folder, testableFile.isDirectory());
        
        if (!folder) {
            assertTrue("File must be not be empty ('" + path + "')", testableFile.length() > 0);
        }
    }
    
    /**
     * Generates a single file node under a subnode
     * @return A single subnode, with name found from VALID_NORMAL_FILES[0]
     * @throws java.io.FileNotFoundException If the test environment is improperly fitted
     */
    public static FSNode generateFileNode() throws FileNotFoundException {
        FSNode root = FSNode.createRootNode(new File(ROOT_PATH_PREFIX + VALID_FOLDERS[0]));
        FSNode subnode = root.addNewSubnode(new File(ROOT_PATH_PREFIX + VALID_NORMAL_FILES[0]));
        
        return subnode;
    }
    
    /**
     * Verify that the prerequisite directory tree exists using proven-good Java standard library functions
     */
    @BeforeClass
    public static void setUpClass() {
        File rootFile = new File(".");
        String absolutePath = rootFile.getAbsolutePath();
        
        assertTrue("Working directory is 'test-temp' (actual: '" + absolutePath + "')", absolutePath.contains("test-temp"));
        
        
        //Check for valid folders
        for (String p : VALID_FOLDERS) {
            checkIfValidItem(ROOT_PATH_PREFIX + p, true, true);
        }
        //.. valid files
        for (String p : VALID_NORMAL_FILES) {
            checkIfValidItem(ROOT_PATH_PREFIX + p, false, true);
        }
        //.. invalid folders
        for (String p : INVALID_FOLDERS) {
            checkIfValidItem(ROOT_PATH_PREFIX + p, true, false);
        }
    }

    /**
     * Must not be able to generate a null root node
     * @throws java.io.FileNotFoundException Should not occur
     */
    @Test
    public void testNullRootMustFail() throws FileNotFoundException {
        try {
                FSNode fr = FSNode.createRootNode(null);
                fail("Managed to create a root node out of a file!");
            } catch (IllegalArgumentException iae) {
                //Success
            }
    }
    
    
    /**
     * Valid files must not be usable as root nodes
     * @throws java.io.FileNotFoundException Should not occur
     */
    @Test
    public void testValidFilesAsRootMustFail() throws FileNotFoundException {
        for (String p : VALID_NORMAL_FILES) {
            try {
                FSNode fr = FSNode.createRootNode(new File(ROOT_PATH_PREFIX + p));
                fail("Managed to create a root node out of a file!");
            } catch (IllegalArgumentException iae) {
                //Success
            }
        }
    }
 
    /**
     * Test that nonexistent paths/files must fail to be created
     */
    @Test
    public void testNonexistentRootMustFail() {
       //.. invalid folders
        for (String p : INVALID_FOLDERS) {
            try {
                FSNode fr = FSNode.createRootNode(new File(ROOT_PATH_PREFIX + p));
                fail("Managed to create a node for a nonexistent file!");
            } catch (FileNotFoundException iae) {
                //Success
            }
        }
    }
    
    /**
     * Prints information about a specific level at a tree, as described by a node
     * @param tree 
     */
    public void printTreeLevel(FSNode tree) {
        System.out.println("Directory node: " + tree.getUnderlyingFile().getName() + ", subnode count: " + tree.getSubnodeCount().get().toString());
        for (FSNode subnode : tree.getSubnodeProperty().get()) {
            System.out.println("Sub node: " + subnode.getUnderlyingFile().getName() + ", subnode count: " + subnode.getSubnodeCount().get().toString());
        }
    }
    
    /**
     * Tests that our FSNode structures stay consistent even during multithreaded usage
     * @throws java.lang.InterruptedException Should not occur
     * @throws java.io.FileNotFoundException Should not occur
     */
    @Test
    public void testThreadStability() throws InterruptedException, FileNotFoundException {
        for (int i = 0; i < 1; i++) {
            final FSNode root = FSNode.createRootNode(new File(ROOT_PATH_PREFIX + VALID_FOLDERS[0]));
            final File file1 = new File(ROOT_PATH_PREFIX + VALID_NORMAL_FILES[0]);
            final File file2 = new File(ROOT_PATH_PREFIX + VALID_NORMAL_FILES[1]);
            
            final File subnodeFolder = new File(ROOT_PATH_PREFIX + VALID_FOLDERS[1]);
            
            //Generate a whole heap of random subfolders
            final FSNode subroot1 = root.addNewSubnode(subnodeFolder);
            final FSNode subroot2 = root.addNewSubnode(subnodeFolder);
            final FSNode subroot3 = subroot1.addNewSubnode(subnodeFolder);
            final FSNode subroot4 = subroot2.addNewSubnode(subnodeFolder);
            final FSNode subroot5 = subroot1.addNewSubnode(subnodeFolder);
            final FSNode subroot6 = subroot2.addNewSubnode(subnodeFolder);
            
            //Nicely in an array
            final FSNode[] randomFolders = {root, subroot1, subroot2, subroot3, subroot4, subroot5, subroot6};
            Thread[] threadStack = new Thread[1]; //10 threads each
            
            for (int j = 0; j < threadStack.length; j++) {
                final int sFolderIndex = j % randomFolders.length;
                //Create a new Runnable that does random alterations
                Runnable dirMixer = new Runnable() {
                    @Override
                    public void run() {
                        
                        final int folderIndex = sFolderIndex;
                        
                        Random r = new Random();
                        List<FSNode> nodes = new ArrayList<>();
                        
                        FSNode targetNode = randomFolders[folderIndex];
                        
                        // Do some random operations
                        for (int x = 0; x < 1000; x++) {
                       
                            // Randomly choose add or delete
                            if (r.nextBoolean() || nodes.size() < 10) {
                                try {
                                    nodes.add(targetNode.addNewSubnode(r.nextBoolean() ? file1 : file2));
                                    
                                } catch (FileNotFoundException ex) {
                                    fail("File not found! Should not happen!");
                                }
                            } else {
                                //Remove one random node from us
                                FSNode fsr = nodes.remove(r.nextInt(nodes.size()));
                                targetNode.removeSubnode(fsr);
                            }
                        }
                    }
                };
                
                threadStack[j] = new Thread(dirMixer);
            }
            
            //Start all threads
            for (Thread t : threadStack) {
                t.start();
            }
            
            //Join all, effectively waiting for the operational part to conclude
            for (Thread t : threadStack) {
                t.join();
            }
            
            //Now, verify consistency! Each node must be the size of its subnodes..
            //If this passes, we can conclude that FSNode is at least at some level safe to use multithreaded
            for (FSNode directoryRoot : randomFolders) {
                BigInteger totalSize = directoryRoot.getTotalSizeProperty().get();
                BigInteger totalSubnodes = directoryRoot.getSubnodeCount().get();
                BigInteger sizeCounter = BigInteger.ZERO;
                BigInteger subdirectoryCounter = BigInteger.ZERO;
                for (FSNode subnode :  directoryRoot.getSubnodeProperty().get()) {
                    sizeCounter = sizeCounter.add(subnode.getTotalSizeProperty().get());
                    subdirectoryCounter = subdirectoryCounter.add(subnode.getSubnodeCount().get()).add(BigInteger.ONE);
                }
                
                assertEquals("Total size of a node must match the size of its subnodes", totalSize, sizeCounter);
                printTreeLevel(directoryRoot);
                assertEquals("Total count of subnodes must be equal to the sum of counts for nodes contained in the list of subnodes", totalSubnodes, subdirectoryCounter);
            }
        }
    }
    
    /**
     * Test that even complex changes in total sizes propagate accurately
     * @throws java.io.FileNotFoundException Should not occur
     */
    @Test
    public void testSizeStaysAccurate() throws FileNotFoundException {
        FSNode root = FSNode.createRootNode(new File(ROOT_PATH_PREFIX + VALID_FOLDERS[0]));
        
        SimpleIntegerProperty timesChanged = new SimpleIntegerProperty(0);
        
        //Observe that the size actually gets changed
        root.getTotalSizeProperty().addListener(new ChangeListener<BigInteger>() {
            @Override
            public void changed(ObservableValue<? extends BigInteger> observable, BigInteger oldValue, BigInteger newValue) {
                timesChanged.set(timesChanged.get() + 1);
            }
        });
        
        ReadOnlyListProperty<FSNode> root_subnodes = root.getSubnodeProperty();
        
        assertEquals("No changes since init", 0, timesChanged.get());
        assertEquals("Size of a directory is zero", BigInteger.ZERO, root.getTotalSizeProperty().get());
        
        assertEquals("Empty folder has a subnode list size of zero", 0, root_subnodes.size());
        assertEquals("Empty folder has subnode count of zero", BigInteger.ZERO, root.getSubnodeCount().get());
        FSNode subnode1 = root.addNewSubnode(new File(ROOT_PATH_PREFIX + VALID_NORMAL_FILES[0]));
        
        assertEquals("root is the parent node of subnode2", root, subnode1.getParentNode());
        
        assertEquals("One change since init", 1, timesChanged.get());
        assertEquals("Total size of directory with one file is the size of the file", subnode1.getTotalSizeProperty().get(), root.getTotalSizeProperty().get());
    
        assertEquals("Folder with one file has a subnode list size of one", 1, root_subnodes.size());
        assertEquals("One file, one subnode", BigInteger.ONE, root.getSubnodeCount().get());
        
        FSNode subnode2 = root.addNewSubnode(new File(ROOT_PATH_PREFIX + VALID_NORMAL_FILES[1]));
        
        //Intermission: try adding a child to a file node
        try {
            subnode1.addNewSubnode(new File(ROOT_PATH_PREFIX + VALID_NORMAL_FILES[1]));
            fail("Could add a child node to a file!");
        } catch (IllegalArgumentException iae) {
            
        }
        
        assertEquals("root is the parent node of subnode2", root, subnode2.getParentNode());
        
        assertEquals("Two changes since init", 2, timesChanged.get());
        
        assertEquals("Total size of directory with two files file is the size of the files", subnode1.getTotalSizeProperty().get().add(subnode2.getTotalSizeProperty().get()), root.getTotalSizeProperty().get());
        //Intermission: try to add a false subnode node!
        try {
            root.addNewSubnode(new File(ROOT_PATH_PREFIX + INVALID_FOLDERS[0]));
        } catch (FileNotFoundException iae) {
            //Success
        }
        
        //NOTE: FSNode purposefully does NOT validate the actual directorial relationships. Therefore, we MAY construct fake systems. IF they are based on actual files.
        
        //Fake subfolder (actually the root folder)
        FSNode f_root = root.addNewSubnode(root.getUnderlyingFile());
        
        //Zero size, no changes
        assertEquals("Two changes since init", 2, timesChanged.get());
        
        //Add a file to the fake root
        FSNode f_f1 = f_root.addNewSubnode(subnode1.getUnderlyingFile());
        
        assertEquals("Three changes since init", 3, timesChanged.get());
        
        //Our size is now 2xF1 plus F2
        
        assertEquals("Total size of directory with three files (nested) is the size of the files", subnode1.getTotalSizeProperty().get().multiply(new BigInteger("2")).add(subnode2.getTotalSizeProperty().get()), root.getTotalSizeProperty().get());
    
        //Remove F2
        root.removeSubnode(subnode2);
        
        //Size is now 2xF1
        assertEquals("Total size of directory with two files (nested) is the size of the files", subnode1.getTotalSizeProperty().get().multiply(new BigInteger("2")), root.getTotalSizeProperty().get());
        
        assertEquals("Four changes since init", 4, timesChanged.get());
        
        //Attempt to remove F2 twice; should fail
        
        try {
            root.removeSubnode(subnode2);
            fail("Was able to remove subnode twice!");
        } catch (IllegalArgumentException iae) {
            //Good
        }
        
        assertEquals("Parent of a removed node is null", null, subnode2.getParentNode());
        
        assertEquals("Four changes since init (false removal attempt)", 4, timesChanged.get());
        
        //Remove the fake roots
        
        root.removeSubnode(f_root);
        
        assertEquals("Parent detachment does not propagate deeper than to the immediate child", f_root, f_f1.getParentNode());
        
        assertEquals("Five changes since init (false removal attempt)", 5, timesChanged.get());
        
        //Empty out totally
        root.removeAllSubnodes();
        
        assertEquals("Size of root must be zero after emptying", BigInteger.ZERO, root.getTotalSizeProperty().get());
    }
 
    /**
     * Tests that extended property map rejects invalid keys, and accepts valid ones
     * @throws java.io.FileNotFoundException Should not occur
     */
    @Test
    public void testInvalidValidExtendedPropertyKeys() throws FileNotFoundException {
        final FSNode root = FSNode.createRootNode(new File(ROOT_PATH_PREFIX + VALID_FOLDERS[0]));
        
        try {
            root.getExtendedPropertyMap().put(null, "aojoi");
            fail("Null key could be added!");
        } catch (NullPointerException npe) {
            
        }
        
        try {
            root.getExtendedPropertyMap().put("..2", "90uj90ad");
            fail("Could add an invalid key!");
        } catch (ClassCastException cce) {
            
        }
        //Values are not verified, so this should pass.
        for (String key : new String[]{"key", "AnotherKey.key.key", "key.key"}) root.getExtendedPropertyMap().put(key, key);
    }
    
}
