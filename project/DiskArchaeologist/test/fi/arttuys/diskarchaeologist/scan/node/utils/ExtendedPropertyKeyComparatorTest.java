/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.node.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * Tests that NodePropertyKeyMapTools works properly
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class ExtendedPropertyKeyComparatorTest {
    
    /**
     * Tests that valid keys are accepted, invalid rejected
     */
    @Test
    public void testValidInvalidKeys() {
        String[] validKeys = new String[] {"AB", "DE.FE", "Dea, De.Be Be"};
        String[] invalidKeys = new String[]{"", ".", ".Bla.Bla", "EEa.", "OKK.XX..JI.EE"};
        
        for (String key : validKeys) assertTrue(ExtendedPropertyKeyComparator.validKeyString(key));
        
        
        for (String key : invalidKeys) assertFalse(ExtendedPropertyKeyComparator.validKeyString(key));
    }
    /**
     * Tests that the comparer can sort keys properly
     */
    @Test
    public void testKeyComparer() {
        ExtendedPropertyKeyComparator epkc = new ExtendedPropertyKeyComparator();
        
        //NULLs
        try {
            epkc.compare(null, "a");
        } catch (NullPointerException npe) {
            //OK
        }
        
        try {
            epkc.compare("ada", null);
        } catch (NullPointerException npe) {
            //OK
        }
        
        ////
        
        try {
            epkc.compare("oap", "a..");
        } catch (ClassCastException cae) {
            //OK
        }
        
        try {
            epkc.compare("", "a.b");
        } catch (ClassCastException cae) {
            //OK
        }
        
        ////
        
        String[] validKeyList = new String[]{"b", "c", "c.a", "c.b", "c.b.aaa", "c.d", "c.d.e", "c.e", "d"};
        List<String> randomizedList = new ArrayList<>();
        randomizedList.addAll(Arrays.asList(validKeyList));
        
        while (!randomizedList.get(0).equals(validKeyList[0])) {
            Collections.shuffle(randomizedList);
        }
        
        Collections.sort(randomizedList, epkc);
        
        String[] randSorted = randomizedList.toArray(new String[randomizedList.size()]);
        
        assertEquals(Arrays.toString(validKeyList), Arrays.toString(randSorted));
    }
}
