/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra <arttuys@arttuys.fi>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.utils;

import com.sun.javafx.collections.ObservableListWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import javafx.collections.ObservableList;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * Very elementary and basic tests for MappingList; tests that some very elementary functions work properly, nothing more.
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class MappingListBasicsTest {
    
    /**
     * The string as concatenated by the transformer
     */
    private static final String ADDED_STRING = "ABCDEF";
    
    /**
     * The single string used for normal tests
     */
    private static final String TEST_STRING = "xyz";
    
    /**
     * This string is rendered as is by the transformation!
     */
    private static final String ASIS_STRING = "abc";
    
    /**
     * The backing list for the MappingList
     */
    private static ObservableList<String> baseList;
    /**
     * The instance of MappingList to be tested
     */
    private static MappingList<String, String> testList;
    
    /**
     * The string transformer function; if a special string, leave as is, otherwise concatenate the additional string.
     * @param raw The string to be transformed
     * @return Result as specified
     */
    private static String stringTransformer(String raw) {
        return raw.equals(ASIS_STRING) ? raw : raw + ADDED_STRING;
    }
    
    /**
     * Initialize the lists shared for all test cases
     */
    @BeforeClass
    public static void setUpClass() {
        baseList = new ObservableListWrapper<>(new ArrayList<>());
        testList = new MappingList<>(baseList, (t) -> (stringTransformer(t)));
        baseList.add(TEST_STRING);
        baseList.add(ASIS_STRING);
    }
  
    /**
     * Test that additions are not possible via the mapping
     */
    @Test
    public void testAdditionMustFail() {
        try {
            testList.add("te");
            fail("Could add to the mapped list - not working properly!");
        } catch (UnsupportedOperationException uoe) {
            //Succeeded
        }
    }
    
    /**
     * Tests that it is impossible to create a mapping list a null function
     */
    @Test
    public void testNullMapperMustFail() {
        try {
            new MappingList(baseList, null);
            fail("Could construct a mapping list with a null function!");
        } catch (IllegalArgumentException iae) {
            //Success
        }
    }
    
    /**
     * Test that removals are not possible via the mapping - in this case, with the as-is string
     */
    @Test
    public void testRemovalMustFail() {
        try {
            testList.remove(ASIS_STRING);
            fail("Could remove from the mapped list - not working properly!");
      
        } catch (UnsupportedOperationException uoe) {
            //Succeeded
        }
    }
    
    /**
     * Test that sorting fails; our mapping is by definition read-only, and not sortable in-place
     */
    @Test
    public void testSortMustFail() {
        try {
            testList.sort(null);
            fail("Could sort the mapping; modifiable!");
      
        } catch (UnsupportedOperationException uoe) {
            //Succeeded
        }
    }
    
    /**
     * Tests that the source index is always the index given, as the mapping is 1-to-1
     */
    @Test
    public void testSourceIndexMustMatch() {
        Random r = new Random();
        
        for (int i = 0; i < 10; i++) {
            int randomPositiveInt = r.nextInt(1000);
            assertEquals(randomPositiveInt, testList.getSourceIndex(randomPositiveInt));
        }
    }
    
    /**
     * Tests that the source is available as required
     */
    @Test
    public void testSourceMustMatch() {
        assertEquals(baseList, testList.getSource());
    }
    
    /**
     * Tests that contents of the list are as expected in a simple case
     */
    @Test
    public void testBasicContentsMustMatch() {
        assertEquals(2, baseList.size());
        assertEquals(baseList.size(), testList.size());
        
        assertEquals(stringTransformer(TEST_STRING), testList.get(0));
        assertEquals(stringTransformer(ASIS_STRING), testList.get(1));
        
        Iterator<String> it= testList.iterator();
        
        assertEquals(true, it.hasNext());
        assertEquals(stringTransformer(TEST_STRING), it.next());
        assertEquals(stringTransformer(ASIS_STRING), it.next());
        assertEquals(false, it.hasNext());
    }
}
