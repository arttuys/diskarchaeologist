/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra <arttuys@arttuys.fi>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.utils;

import com.sun.javafx.collections.ObservableListWrapper;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * A slightly more advanced test for MappingList; tests randomized additions, removals, permutations and so forth from the source list, and does invariant testing after each cycle aswell
 * This implementation forces events to lockstep with actions to ensure testability. That is achieved via a semaphore
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class MappingListEventTest {
    
    /**
     * The backing list for the MappingList
     */
    private static ObservableList<Integer> baseList;
    /**
     * The instance of MappingList to be tested
     */
    private static MappingList<Integer, String> testList;
    
    /**
     * String builder, as to be used by the testing method
     */
    private static StringBuilder testerStringBuilder;
    /**
     * String builder, as to be used by the events triggered
     */
    private static StringBuilder eventStringBuilder;
    
    private static Semaphore eventSemaphore;
    
    /**
     * Transforms an integer to an arbitrary string; this transformation is a bijection
     * @param i Integer
     * @return String
     */
    protected static String integerToString(Integer i) {
        return i.toString() + " " + i.toString().length();
    }
    
    /**
     * Returns a test string for an addition or replacement of an item
     * @param replace Replace instead of add?
     * @param expectedTransform What the final item in the mapping is meant to look like?
     * @return A string
     */
    protected static String additionOfItem(boolean replace, String expectedTransform) {
        return (replace ? "REPLACED:" : "ADDED:") + expectedTransform + "\n";
    } 
    
    /**
     * Returns a test string for a removal
     * @param expectedTransform What was the removed item's form?
     * @return A string
     */
    protected static String removalOfItem(String expectedTransform) {
        return "REMOVAL:" + expectedTransform + " \n";
    } 
    
    /**
     * Set up the scaffolding for this test
     */
    @BeforeClass
    
    public static void setUpClass() {
        baseList = new ObservableListWrapper<>(new ArrayList<>());
        testList = new MappingList<>(baseList, (x) -> integerToString(x));
        
        //Init string builders
        testerStringBuilder = new StringBuilder();
        eventStringBuilder = new StringBuilder();
        
        //Initialize a semaphore
        eventSemaphore = new Semaphore(0);
    }
    
    /**
     * Tests that invariant conditions hold after each cycle. 
     * In this case, the action logs must be consistent, sizes equal and all items must match to their expected counterparts.
     */
    private void testInvariant() {
        //Sizes must be equal
        assertEquals(baseList.size(), testList.size());
        
        //Assert equality with a for-loop
        for (int i = 0; i < baseList.size(); i++) {
            assertEquals(integerToString(baseList.get(i)), testList.get(i));
        }
               
        
        //Assert equality of the StringBuilder states, as with events
        assertEquals(testerStringBuilder.toString(), eventStringBuilder.toString());
    }
    
    /**
     * Test event handling; to pass this test, the mapping must be able to correctly forward events such that the transformations are transparently executed
     * whilst forwarding the event
     */
    @Test
    public void testEvents() {
        testList.addListener(new ListChangeListener<String>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends String> c) {
                c.next();
                
                if (c.wasReplaced()) {
                    //Removed contains what was replaced!
                    eventStringBuilder.append(additionOfItem(true, c.getRemoved().get(0)));
                } else if (c.wasAdded()) {
                    assertEquals("Must have added 1", 1, c.getAddedSize());
                    eventStringBuilder.append(additionOfItem(false, c.getAddedSubList().get(0)));
                } else if (c.wasRemoved()) {
                    assertEquals("Must have removed 1", 1, c.getRemovedSize());
                    eventStringBuilder.append(removalOfItem(c.getRemoved().get(0)));
                } else if (c.wasPermutated()) {
                    //As defined below, we did sort the base array. We can trivially see from the code that the permutation-related methods are a simple
                    //passthrough to the underlying event; therefore, I'd believe it is sufficient to test that our mapping list produces consistent indexes,
                    //and that they actually exbihit the ordering the test requires
                    
                    ArrayList<Integer> indexesAlreadySeen = new ArrayList<>();
                    
                    for (int i = 0; i < baseList.size(); i++) {
                        Integer newIndexOfItem = c.getPermutation(i);
                        assertEquals("Two old indexes lead to same new index in permutation; permutation is not a bijection!", -1, indexesAlreadySeen.indexOf(newIndexOfItem));
                        
                        indexesAlreadySeen.add(newIndexOfItem);
                        //Also, test that the sorting seems to work properly; as in, if the new index is larger than the old one, then the respective items in the base list must be larger aswell.
                        
                        if (newIndexOfItem == i) {
                            //Equality. No testing needed, as not moved anywhere
                        } else if (newIndexOfItem < i) {
                            //New index of item smaller than the original index
                            assertTrue("Sorting consistent; smaller index leads to item smaller than in a larger index", baseList.get(newIndexOfItem) <= baseList.get(i));
                        } else {
                            //New index of item larger than the original index
                            assertTrue("Sorting consistent; larger index leads to item larger than in a smaller index", baseList.get(newIndexOfItem) >= baseList.get(i));
                        }
                        
                    }
                    
                    eventStringBuilder.append("SORTED\n");
                }
                
                //Release one step from the event semaphore
                eventSemaphore.release();
            }
            
            
        });
        
        //Initialize a random
        
        Random r = new Random();
        
        for (int i = 0; i < 1000; i++) {
            int rand = baseList.size() < 10 ? 0 : r.nextInt(4); //Demand that addition must happen until we are 10 items or larger
            switch (rand) {
                case 0:
                    //Addition. Add a random number
                    int randomNumber = r.nextInt();
                    baseList.add(randomNumber);
                    
                    testerStringBuilder.append(additionOfItem(false, integerToString(randomNumber)));
                    break;
                case 1:
                    //Removal. Remove a random item
                    int index = r.nextInt(baseList.size());
                    
                    testerStringBuilder.append(removalOfItem(testList.get(index)));
                    baseList.remove(index);
                    break;
                case 2:
                    //Permutation. Sort the base array
                    
                    testerStringBuilder.append("SORTED\n");
                    baseList.sort(null);
                    break;
                case 3:
                    //Replacement. Replace one and just one item
                    index = r.nextInt(baseList.size());
                    randomNumber = r.nextInt();
                  
                    testerStringBuilder.append(additionOfItem(true, integerToString(baseList.set(index, randomNumber))));
                    
                    
            }
            
            //Wait that the event takes place
            eventSemaphore.acquireUninterruptibly();
            
            //Test for invariants
            testInvariant();
        }
        
        for (String s : new String[]{"ADDED", "REMOVAL", "SORTED", "REPLACED"}) {
            //Test that all types have taken place
            assertTrue("At least one " + s + " test was completed", eventStringBuilder.toString().contains(s) && testerStringBuilder.toString().contains(s)); 
        }
                    
        
    }
}
