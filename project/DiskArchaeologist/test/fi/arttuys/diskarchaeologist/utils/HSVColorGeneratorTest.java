/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.utils;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import javafx.scene.paint.Color;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * Simple tests for HSVColorGenerator
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class HSVColorGeneratorTest {
    

    /**
     * Test that a valid function is returned
     */
    @Test
    public void testUnitToColorFunction() {
        assertNotNull("HSVColorGenerator must return a valid function", HSVColorGenerator.unitToColorFunction());
    }

    /**
     * Test that for a set of distinct parameters, a distinct set of colors is returned
     */
    @Test
    public void testNextColor() {
        Function<Integer, Color> colorFunction = HSVColorGenerator.unitToColorFunction();
        Set<Color> colorsReceived = new HashSet<>();
        
        int distinctSaturations = (int)((1.0 - HSVColorGenerator.SATURATION_MIN) / HSVColorGenerator.SATURATION_STEP) - 1;
        int distinctValues = (int)((HSVColorGenerator.VALUE_MAX - HSVColorGenerator.VALUE_MIN) / HSVColorGenerator.VALUE_STEP) - 1;
        int distinctRequired = HSVColorGenerator.DISTINCT_COLORS * Math.max(1, distinctValues) * Math.max(1, distinctSaturations);
        
        // First, get the set of colors
        for (int i = 0; i < distinctRequired; i++) {
            colorsReceived.add(colorFunction.apply(i));
        }
        
        assertEquals("Must have received exact amount of colors, as Set enforces uniqueness", distinctRequired, colorsReceived.size());
        
        // Now, do it again; this should not result in new colors
        for (int i = 0; i < distinctRequired; i++) {
            colorsReceived.add(colorFunction.apply(i));
        }
        
         assertEquals("No new colors returned with same values", distinctRequired, colorsReceived.size());
    }
    
}
