/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.utils;

import java.math.BigInteger;
import javafx.beans.property.SimpleObjectProperty;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Some simple tests for SizeToStringConverter
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class SizeToStringConverterTest {
    
    /**
     * Checks that the string-to-size conversion works
     */
    @Test
    public void testStringToSize() {
        assertEquals(BigInteger.ONE, SizeToStringConverter.stringToBigInteger("1 bytes"));
        assertEquals(new BigInteger("2048"), SizeToStringConverter.stringToBigInteger("2 kilobytes"));
        assertEquals(new BigInteger("-2560"), SizeToStringConverter.stringToBigInteger("-2.5 kilobytes"));
        assertEquals(BigInteger.ZERO,  SizeToStringConverter.stringToBigInteger("Guardians of the Galaxy"));
    }
  
    /**
     * Checks that SizeToString produces readable strings for some common cases
     */
    @Test
    public void testSizeToString() 
    {
        assertEquals("1.00 bytes", SizeToStringConverter.bigIntegerToSize(new SimpleObjectProperty<>(BigInteger.ONE)).get());
        assertEquals("2.50 kilobytes", SizeToStringConverter.bigIntegerToSize(new SimpleObjectProperty<>(new BigInteger(Long.toString(2*1024 + 512)))).get());
    }
}
