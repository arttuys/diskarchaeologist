/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.utils;

// Generated by ComTest BEGIN
import java.util.Map;
import static org.junit.Assert.*;
import org.junit.*;
// Generated by ComTest END

/**
 * Test class made by ComTest
 * @version 2016.04.11 15:29:42 // Generated by ComTest
 * 
 * Further adapted by Arttu Ylä-Sahra
 * 
 * @date 26/5/2017
 */
public class EditBoxParserTest {



  // Generated by ComTest BEGIN
  /** testIsFieldDelimiterOrAssignChar107 */
  @Test
  public void testIsFieldDelimiterOrAssignChar107() {    // EditBoxParser: 107
    assertEquals("From: EditBoxParser line: 112", true, EditBoxParser.isFieldDelimiterOrAssignChar(EditBoxParser.FIELD_KEY_START_CHAR));  
    assertEquals("From: EditBoxParser line: 113", true, EditBoxParser.isFieldDelimiterOrAssignChar(EditBoxParser.FIELD_KEY_END_CHAR));  
    assertEquals("From: EditBoxParser line: 114", true, EditBoxParser.isFieldDelimiterOrAssignChar(EditBoxParser.VALUE_START_CHAR));  
    assertEquals("From: EditBoxParser line: 115", true, EditBoxParser.isFieldDelimiterOrAssignChar(EditBoxParser.VALUE_END_CHAR));  
    assertEquals("From: EditBoxParser line: 116", true, EditBoxParser.isFieldDelimiterOrAssignChar(EditBoxParser.ASSIGN_CHAR));  
    assertEquals("From: EditBoxParser line: 117", false, EditBoxParser.isFieldDelimiterOrAssignChar(EditBoxParser.SPACE_CHAR));  
    assertEquals("From: EditBoxParser line: 118", true, EditBoxParser.isFieldDelimiterOrAssignChar(EditBoxParser.COMMENT_CHAR));  
    assertEquals("From: EditBoxParser line: 119", false, EditBoxParser.isFieldDelimiterOrAssignChar('c'));  
    assertEquals("From: EditBoxParser line: 120", false, EditBoxParser.isFieldDelimiterOrAssignChar('\r'));  
    assertEquals("From: EditBoxParser line: 121", false, EditBoxParser.isFieldDelimiterOrAssignChar('\n'));  
  } // Generated by ComTest END


  // Generated by ComTest BEGIN
  /** testIsFieldDelimiterChar134 */
  @Test
  public void testIsFieldDelimiterChar134() {    // EditBoxParser: 134
    assertEquals("From: EditBoxParser line: 139", true, EditBoxParser.isFieldDelimiterChar(EditBoxParser.FIELD_KEY_START_CHAR));  
    assertEquals("From: EditBoxParser line: 140", true, EditBoxParser.isFieldDelimiterChar(EditBoxParser.FIELD_KEY_END_CHAR));  
    assertEquals("From: EditBoxParser line: 141", true, EditBoxParser.isFieldDelimiterChar(EditBoxParser.VALUE_START_CHAR));  
    assertEquals("From: EditBoxParser line: 142", true, EditBoxParser.isFieldDelimiterChar(EditBoxParser.VALUE_END_CHAR));  
    assertEquals("From: EditBoxParser line: 143", false, EditBoxParser.isFieldDelimiterChar(EditBoxParser.ASSIGN_CHAR));  
    assertEquals("From: EditBoxParser line: 144", false, EditBoxParser.isFieldDelimiterChar(EditBoxParser.SPACE_CHAR));  
    assertEquals("From: EditBoxParser line: 145", true, EditBoxParser.isFieldDelimiterChar(EditBoxParser.COMMENT_CHAR));  
    assertEquals("From: EditBoxParser line: 146", false, EditBoxParser.isFieldDelimiterChar('c'));  
    assertEquals("From: EditBoxParser line: 147", false, EditBoxParser.isFieldDelimiterChar('\r'));  
    assertEquals("From: EditBoxParser line: 148", false, EditBoxParser.isFieldDelimiterChar('\n'));  
  } // Generated by ComTest END


  // Generated by ComTest BEGIN
  /** testIsNewlineChar161 */
  @Test
  public void testIsNewlineChar161() {    // EditBoxParser: 161
    assertEquals("From: EditBoxParser line: 166", false, EditBoxParser.isNewlineChar(EditBoxParser.FIELD_KEY_START_CHAR)); 
    assertEquals("From: EditBoxParser line: 167", false, EditBoxParser.isNewlineChar(EditBoxParser.FIELD_KEY_END_CHAR)); 
    assertEquals("From: EditBoxParser line: 168", false, EditBoxParser.isNewlineChar(EditBoxParser.VALUE_START_CHAR)); 
    assertEquals("From: EditBoxParser line: 169", false, EditBoxParser.isNewlineChar(EditBoxParser.VALUE_END_CHAR)); 
    assertEquals("From: EditBoxParser line: 170", false, EditBoxParser.isNewlineChar(EditBoxParser.ASSIGN_CHAR)); 
    assertEquals("From: EditBoxParser line: 171", false, EditBoxParser.isNewlineChar(EditBoxParser.SPACE_CHAR)); 
    assertEquals("From: EditBoxParser line: 172", false, EditBoxParser.isNewlineChar(EditBoxParser.COMMENT_CHAR)); 
    assertEquals("From: EditBoxParser line: 173", false, EditBoxParser.isNewlineChar('c')); 
    assertEquals("From: EditBoxParser line: 174", true, EditBoxParser.isNewlineChar('\r')); 
    assertEquals("From: EditBoxParser line: 175", true, EditBoxParser.isNewlineChar('\n')); 
  } // Generated by ComTest END


  // Generated by ComTest BEGIN
  /** testIsWhitespaceChar189 */
  @Test
  public void testIsWhitespaceChar189() {    // EditBoxParser: 189
    assertEquals("From: EditBoxParser line: 194", false, EditBoxParser.isWhitespaceChar(EditBoxParser.FIELD_KEY_START_CHAR)); 
    assertEquals("From: EditBoxParser line: 195", false, EditBoxParser.isWhitespaceChar(EditBoxParser.FIELD_KEY_END_CHAR)); 
    assertEquals("From: EditBoxParser line: 196", false, EditBoxParser.isWhitespaceChar(EditBoxParser.VALUE_START_CHAR)); 
    assertEquals("From: EditBoxParser line: 197", false, EditBoxParser.isWhitespaceChar(EditBoxParser.VALUE_END_CHAR)); 
    assertEquals("From: EditBoxParser line: 198", false, EditBoxParser.isWhitespaceChar(EditBoxParser.ASSIGN_CHAR)); 
    assertEquals("From: EditBoxParser line: 199", true, EditBoxParser.isWhitespaceChar(EditBoxParser.SPACE_CHAR)); 
    assertEquals("From: EditBoxParser line: 200", false, EditBoxParser.isWhitespaceChar(EditBoxParser.COMMENT_CHAR)); 
    assertEquals("From: EditBoxParser line: 201", false, EditBoxParser.isWhitespaceChar('c')); 
    assertEquals("From: EditBoxParser line: 202", true, EditBoxParser.isWhitespaceChar('\r')); 
    assertEquals("From: EditBoxParser line: 203", true, EditBoxParser.isWhitespaceChar('\n')); 
  } // Generated by ComTest END


  /**
   * Test parsing functionality
   */
 @Test
 public void testParsingFunctionality() {
         try { EditBoxParser.parseStringToMap(null); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	//Not a valid declaration
	 try {EditBoxParser.parseStringToMap("X"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
 //Unmatched braces
	 try {EditBoxParser.parseStringToMap("<"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	try { EditBoxParser.parseStringToMap("["); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	 //Improper assignment character usage
	try { EditBoxParser.parseStringToMap("[A] : <DEF>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	//Usage of newline
	try { EditBoxParser.parseStringToMap("[A\n]: <DEF>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	 //Nested braces
	try { EditBoxParser.parseStringToMap("[[A]]: <DEF>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	try { EditBoxParser.parseStringToMap("[A]: <<DEF>>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	try { EditBoxParser.parseStringToMap("[[A]: <DEF>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	try { EditBoxParser.parseStringToMap("[A]: <<DEF>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	try { EditBoxParser.parseStringToMap("[A]]: <DEF>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	try { EditBoxParser.parseStringToMap("[A]: <DEF>>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	 //Empty key
	try { EditBoxParser.parseStringToMap("[]: <DEF>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	  //Characters inbetween parts of declaration
	try { EditBoxParser.parseStringToMap("[A]: X<DEF>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	 //Newline mid-declaration
	try { EditBoxParser.parseStringToMap("[A]: \n<DEF>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	  //Key defined twice
	try { EditBoxParser.parseStringToMap("[A]: <DEF>\n[A]: <DEF>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	  //Special characters in key or declaration
	try { EditBoxParser.parseStringToMap("[A]: <[DEF>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	try { EditBoxParser.parseStringToMap("[A]: <DEF]>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	try { EditBoxParser.parseStringToMap("[<A]: <DEF>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	try { EditBoxParser.parseStringToMap("[A>]: <DEF>\n[B]:<XYZ:>"); fail("Exception not thrown where needed!"); } catch (IllegalArgumentException iae) { /* OK */ } 
	  //.. This should work
	  Map<String, String> dataMap = EditBoxParser.parseStringToMap("[A]: <>\n \n[B]:<XYZ:>#Derp<>[]:");
	  dataMap = EditBoxParser.parseStringToMap("[A]: <>\n#Hi! \n[B]:<XYZ:>"); //Also this should work
	  assertEquals(2, dataMap.size()); 
	  assertEquals("", dataMap.get("A"));
	  assertEquals("XYZ:", dataMap.get("B"));
	 //Resulting map must allow editing
	 dataMap.put("A", "EE");
	 assertEquals("EE", dataMap.get("A"));
	 dataMap.remove("A");
	    
 }
  
}
