/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.WeakInvalidationListener;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ReadOnlyProperty;

/**
 *
 * An assistant class converting arbitrary byte sizes to human-readable. This class allows you to generate bindings to observable BigIntegers that will transparently
 * transform into easy-to-read size descriptions
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class SizeToStringConverter {
    
    /**
     * The postfix strings for sizes
     */
    private static final String[] SIZE_POSTFIX_STRS = new String[]{"bytes", "kilobytes", "megabytes", "gigabytes", "terabytes", "petabytes"};
    /**
     * In the same order as postfix strings, the scales compared to bytes
     */
    private static final BigInteger[] SIZE_SCALES = new BigInteger[SIZE_POSTFIX_STRS.length];
    /**
     * A map between the postfix scales and BigIntegers
     */
    private static final HashMap<String, BigInteger> POSTFIX_TO_SCALE_MAP;
    
    static {
        POSTFIX_TO_SCALE_MAP = new HashMap<>();
        
        //Initialize the base values we want to compare on, and also populate the hashmap for quick string-to-scale conversions
        for (int i = 0; i < SIZE_POSTFIX_STRS.length; i++) {
            BigInteger powSize = new BigInteger("1024").pow(i);
            SIZE_SCALES[i] = powSize;
            POSTFIX_TO_SCALE_MAP.put(SIZE_POSTFIX_STRS[i], powSize);
        }

    }
    
    private static class SizeBindingImpl extends StringBinding {

        private final ReadOnlyProperty<BigInteger> sizeProperty;
        private final InvalidationListener localInvListener;
        
        public SizeBindingImpl(ReadOnlyProperty<BigInteger> property) {
            this.sizeProperty = property;
            this.localInvListener = new InvalidationListener() {
                @Override
                public void invalidated(Observable observable) {
                    SizeBindingImpl.this.invalidate();
                }
            };
            
            //Add a new invalidation binding to the property
            property.addListener(new WeakInvalidationListener(localInvListener));
        }
        
        @Override
        protected String computeValue() {
            //Get the value from the property   
            BigInteger val = sizeProperty.getValue();
            
            // If it is null, return no value
            if (val == null) return "-";
            BigInteger absVal = val.abs();
            //No value, return '-'
            
            if (absVal.compareTo(BigInteger.ONE) == -1) return "0 bytes"; //Must be a zero
            
            
            for (int i = SIZE_SCALES.length-1; i >= 0; i--) {
                BigInteger baseSize = SIZE_SCALES[i];
                if (absVal.compareTo(baseSize) == -1) continue; //Smaller than this, no good
                
                BigDecimal decimalSize = new BigDecimal(absVal);
                BigDecimal resultSize = decimalSize.divide(new BigDecimal(baseSize), 2, RoundingMode.HALF_UP);
                
                return String.format("%s%s %s", val.compareTo(BigInteger.ZERO) == -1 ? "-" : "", resultSize.toPlainString(), SIZE_POSTFIX_STRS[i]);
            }
            
            return "-";
        }
        
    }
    
    /**
     * Returns an approximation on what the human-readable value would be in actual bytes.
     * @param str String
     * @return 0 if not valid, otherwise a size in BigInteger
     */
    public static BigInteger stringToBigInteger(String str) {
        // First, generate a regex to check for the shape
        Pattern p = Pattern.compile("(?<val>\\-?[0-9]+(\\.[0-9]+)?) (?<unit>[A-Za-z]+)");
        Matcher m = p.matcher(str);
        
        // If there's no match, return a zero
        if (!m.matches()) return BigInteger.ZERO;
        
        BigDecimal num = new BigDecimal(m.group("val")); // We have guaranteed in the regex that if there's a match, there must be a valid form of a BigDecimal
        BigInteger scale = POSTFIX_TO_SCALE_MAP.get(m.group("unit"));
        
        if (scale == null) return BigInteger.ZERO;
        
        // Alright, now we have sensible parameters to calculate the result with
        return num.multiply(new BigDecimal(scale)).toBigInteger();
    }
    
    /**
     * Generates a dynamic binding, returning a human-readable size for a given above-zero BigInteger
     * @param property A property containing a positive size
     * @return A property showing a human-readable value for such
     */
    public static StringBinding bigIntegerToSize(ReadOnlyProperty<BigInteger> property) {
        return new SizeBindingImpl(property);
    }
    
}
