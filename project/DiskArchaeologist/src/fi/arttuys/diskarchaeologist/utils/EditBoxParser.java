/*
 * The MIT License
 *
 * Copyright 2016-2017 Arttu Ylä-Sahra, Jaakko Lipas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.utils;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * Readapted version of an old university course assignment utility component. This component was used to parse freefrom text into a hashmap, and vis a versa.
 * To avoid reinventing the wheel and save time, a decision was made to lightly adapt the code already proven to work.
 * <br>
 * Unlike the older version, this version is not strict about the keys being parsed, and will accept anything plausible. The function to convert maps back to text has also been removed.
 * <br>
 * Permission of all authors have been obtained to include this code as a part of this project.
 * <br>
 * While this file contains the ComTest tests, a premade JUnit test file has also been included for simplicity
 * 
 * @date 26/5/2017 (original class 4/2016)
 * @author Arttu Ylä-Sahra, Jaakko Lipas
 */
public class EditBoxParser {
 
        /**
	 * Symbols denoting a newline
	 */
	private static final char[] NEWLINE_CHARS = new char[]{'\n', '\r'};
	
	/**
	 * The parser could be described as an deterministic finite automaton, with following states.
	 * 
	 * NEUTRAL 					: Waiting for the start of a declaration; only start symbol, spaces and newlines permitted
	 *
	 * READING_KEY				: Reading the key for an assignment. No newlines permitted, all non-special symbols are considered to be a part of a key
	 * EXPECT_ASSIGNMENT_CHAR 	: Assignment symbol expected; if not found, declaration is erroneous
	 *
	 * WAITING_FIELD			: Waiting for a start of the value; only spaces permitted before a start of a field symbol
	 * READING_FIELD 			: Same as READING_KEY, but for the value of a declaration. After this, return to NEUTRAL
	 * 
	 * COMMENT_ROW				: All symbols after this BEFORE a newline are to be intepreted as a comment, and therefore ignored
	 * @author arttuys
	 * @version 7.3.2016
	 *
	 */
	private enum ParserState {
		NEUTRAL, READING_KEY, EXPECT_ASSIGNMENT_CHAR, WAITING_VALUE, READING_VALUE, COMMENT_ROW
	}
    

	/**
	 * Space; permitted between key definition and value definition, and between key-value pairs in text
	 */
	public static final char SPACE_CHAR = ' ';
	
	/**
	 * The symbol starting a key declaration
	 */
	public static final char FIELD_KEY_START_CHAR = '[';
	
	/**
	 * The symbol ending a key declaration
	 */
	public static final char FIELD_KEY_END_CHAR = ']';
	
	/**
	 * The starting character for a value
	 */
	public static final char VALUE_START_CHAR = '<';
	
	/**
	 * The ending character for a value
	 */
	public static final char VALUE_END_CHAR = '>';
	
	/**
	 * Assignment character must always be placed right after the symbol for an end of the key declaration
	 */
	public static final char ASSIGN_CHAR = ':';
	
	/**
	 * Comment symbol
	 */
	public static final char COMMENT_CHAR = '#';
        
        /**
	 * Is this symbol any kind of a special character delimiting a key or a value declaration, or the assignment symbol?
	 * 
         * <pre>
	 * EditBoxParser.isFieldDelimiterOrAssignChar($character) === $result;
	 * 
	 * $character                            |  $result          
	 * ---------------------------------------------
	 * EditBoxParser.FIELD_KEY_START_CHAR |  true     
	 * EditBoxParser.FIELD_KEY_END_CHAR   |  true
	 * EditBoxParser.VALUE_START_CHAR     |  true
	 * EditBoxParser.VALUE_END_CHAR       |  true
	 * EditBoxParser.ASSIGN_CHAR          |  true
	 * EditBoxParser.SPACE_CHAR           |  false
	 * EditBoxParser.COMMENT_CHAR         |  true
	 * 'c'                                |  false
	 * '\r'                               |  false
	 * '\n'                               |  false
	 * </pre>
         * 
	 * @param c Character
	 * @return TRUE if yes, FALSE if no
	 * 
	 * 
	 */
	public static boolean isFieldDelimiterOrAssignChar(char c) {
		return (isFieldDelimiterChar(c) || c == ASSIGN_CHAR);
	}
	
	/**
	 * Is this character any kind of an field delimiter (key or value declaration) character
	 * 
         * <pre>
	 * EditBoxParser.isFieldDelimiterChar($character) === $result;
	 * 
	 * $character                            |  $result          
	 * ---------------------------------------------
	 * EditBoxParser.FIELD_KEY_START_CHAR |  true     
	 * EditBoxParser.FIELD_KEY_END_CHAR   |  true
	 * EditBoxParser.VALUE_START_CHAR     |  true
	 * EditBoxParser.VALUE_END_CHAR       |  true
	 * EditBoxParser.ASSIGN_CHAR          |  false
	 * EditBoxParser.SPACE_CHAR           |  false
	 * EditBoxParser.COMMENT_CHAR         |  true
	 * 'c'                                |  false
	 * '\r'                               |  false
	 * '\n'                               |  false
	 * </pre>
         * 
	 * @param c Character
	 * @return TRUE if yes, FALSE if no
	 * 
	 */
	public static boolean isFieldDelimiterChar(char c) {
		return (c == FIELD_KEY_START_CHAR || c == FIELD_KEY_END_CHAR || c == VALUE_START_CHAR || c == VALUE_END_CHAR || c == COMMENT_CHAR);
	}
	
	/**
	 * Is this character a newline character?
	 * 
         * <pre>
	 * EditBoxParser.isNewlineChar($character) === $result
	 * 
	 * $character                            |  $result          
	 * ---------------------------------------------
	 * EditBoxParser.FIELD_KEY_START_CHAR |  false     
	 * EditBoxParser.FIELD_KEY_END_CHAR   |  false
	 * EditBoxParser.VALUE_START_CHAR     |  false
	 * EditBoxParser.VALUE_END_CHAR       |  false
	 * EditBoxParser.ASSIGN_CHAR          |  false
	 * EditBoxParser.SPACE_CHAR           |  false
	 * EditBoxParser.COMMENT_CHAR         |  false
	 * 'c'                                |  false
	 * '\r'                               |  true
	 * '\n'                               |  true
	 * </pre>
         * 
	 * @param c Character
	 * @return TRUE if yes, FALSE if no
	 * 
	 */
	public static boolean isNewlineChar(char c) {
		for (char ch : NEWLINE_CHARS) if (c == ch) return true;
		
		return false;
	}
	
	/**
	 * Is this a whitespace character (a.k.a a newline, or a space character)
	 * 
         * <pre>
	 * EditBoxParser.isWhitespaceChar($character) === $result
	 * 
	 * $character                            |  $result          
	 * ---------------------------------------------
	 * EditBoxParser.FIELD_KEY_START_CHAR |  false     
	 * EditBoxParser.FIELD_KEY_END_CHAR   |  false
	 * EditBoxParser.VALUE_START_CHAR     |  false
	 * EditBoxParser.VALUE_END_CHAR       |  false
	 * EditBoxParser.ASSIGN_CHAR          |  false
	 * EditBoxParser.SPACE_CHAR           |  true
	 * EditBoxParser.COMMENT_CHAR         |  false
	 * 'c'                                |  false
	 * '\r'                               |  true
	 * '\n'                               |  true
	 * </pre>
         * 
	 * @param c Character
	 * @return TRUE if yes, FALSE if no
	 * 
	 */
	public static boolean isWhitespaceChar(char c) {
		return (c == SPACE_CHAR || isNewlineChar(c));
	}
        
        /**
         * This is intended to be a static class, and therefore not publicly instantiable
         */
        private EditBoxParser() {
            // No operations
        }
        
        /**
	 * 
	 * Reads a string, and returns a Map of key-value pairs containing the relevant data
	 * 
	 * @param input Input string
	 * @return A map containing the key-value pairs. Editing of this Map will not have side effects elsewhere
	 */
	public static Map<String, String> parseStringToMap(String input) {
		if (input == null) throw new IllegalArgumentException("Null input string is not allowed!");
		HashMap<String, String> dataMap = new HashMap<String, String>();
		
		//Starting the parsing. Initialize the starting states
		ParserState state = ParserState.NEUTRAL;
		StringBuilder keyBuffer = new StringBuilder();
		StringBuilder valueBuffer = new StringBuilder();
		
		for (char c : input.toCharArray()) {
			//Due to simplicity, we do not need lookback. Therefore, we can use a simple foreach loop
			switch (state) {
			case NEUTRAL:
				//Is this a field key starting character?
				if (c == FIELD_KEY_START_CHAR) {
					state = ParserState.READING_KEY; //Change to key reading mode
					continue; //Next character
				}
				//Onko kommenttimerkki?
				if (c == COMMENT_CHAR) {
					state = ParserState.COMMENT_ROW; //Move to comment state
					continue;
				}
				if (isWhitespaceChar(c)) continue; //Empty? Continue
				
				throw new IllegalArgumentException("Expected start of declaration or a comment, but something else came through");
          
			case COMMENT_ROW:
				if (isNewlineChar(c)) { //Newline, change to neutral
					state = ParserState.NEUTRAL;
				}
				
				//Resume
				break;
			case READING_KEY:
				if (c == FIELD_KEY_END_CHAR) {
					//Ending symbol, change to assignment char expectation
					state = ParserState.EXPECT_ASSIGNMENT_CHAR;
					continue;
				}
				if (isFieldDelimiterChar(c) || isNewlineChar(c)) { //Other special character or a newline? Not permitted
					throw new IllegalArgumentException("Keys may not have special characters or newlines!");
				}
				//No problems, add to the buffer
				keyBuffer.append(c);
				break;
			case EXPECT_ASSIGNMENT_CHAR:
				if (c == ASSIGN_CHAR) {
					//OK
					state = ParserState.WAITING_VALUE;
				} else {
					throw new IllegalArgumentException(String.format("You must put an assignment character right after the end of a key declaration ('%c')", ASSIGN_CHAR));
				}
				break;
			case WAITING_VALUE:
				if (c == VALUE_START_CHAR) {
					state = ParserState.READING_VALUE;
					continue; //Start to read a value, and resume
				}
				if (c == SPACE_CHAR) continue; // Spaces are allowed here
				throw new IllegalArgumentException("Expected the start of a value declaration, something else came through");
			case READING_VALUE:
				if (c == VALUE_END_CHAR) {
					//Ending character; save the data, and do a few sanity checks
					String keyStr = keyBuffer.toString();
					String valueStr = valueBuffer.toString();
					
					if (keyStr.length() == 0) throw new IllegalArgumentException("Key is empty!");
					
					if (dataMap.containsKey(keyStr)) throw new IllegalArgumentException("Duplicate definitions - each key must have only one declaration each.");
					
					dataMap.put(keyStr, valueStr); //Save to the map
					//Empty buffers
					keyBuffer.setLength(0);
					valueBuffer.setLength(0);
					//Reset to neutral
					state = ParserState.NEUTRAL;
					continue; //And resume
				}
				if (isFieldDelimiterChar(c) || isNewlineChar(c)) { // A special character or a newline?
					throw new IllegalArgumentException(String.format("Value may not have special characters or newlines (found '%c')!", c));
				}
				// No trouble, append to the buffer
				valueBuffer.append(c);
				break;
			default:
				throw new IllegalArgumentException("Internal error; parser ended up in an unknown state, and cannot continue.");
			}
			
		}
		
		//Not in netural when exiting?
		if (state != ParserState.NEUTRAL && state != ParserState.COMMENT_ROW) throw new IllegalArgumentException("Incomplete declaration detected - please check your input");
		
		//Valmis.
		return dataMap;
	}

}
