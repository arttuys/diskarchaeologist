/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.utils;

import java.util.Random;
import java.util.function.Function;
import javafx.scene.paint.Color;

/**
 *
 * When generating colors randomly, it is often pertinent that the contents of a
 * generated set are distinct from each other.
 * <br>
 * This class uses the convenient properties of the HSV (also known as HSB)
 * color representation to achieve that end. When asked for a new color, it
 * first tries to ensure that all colors are distinct, progressively allowing
 * closer and closer colors. When it runs out of ideas, it simply starts issuing
 * out random colors
 *
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class HSVColorGenerator {

    /**
     * When going through the Hue axis, how many distinct colors we consider to exist?
     */
    public static final int DISTINCT_COLORS = 20;
    /**
     * How much we shall reduce the value when we run out of base colors?
     */
    public static final float VALUE_STEP = 0.1f;
    /**
     * At what point we should not reduce value, and instead should reduce saturation?
     */
    public static final float VALUE_MIN = 0.3f;
    /**
     * At what point we start when we've resetted value?
     */
    public static final float VALUE_MAX = 0.7f; 

    /**
     * How much saturation is reduced per cycle?
     */
    public static final float SATURATION_STEP = 0.25f;
    /**
     * At what point, we must give up and just randomly jitter out colors?
     */
    public static final float SATURATION_MIN = 0.0f;

    /**
     * Returns a new mapping function between some parameter of a given type and a color. This utilizes the caching function feature
     * @param <X> Type that this function should take
     * @return A function that generates colors for the given function
     */
    public static <X> Function<X, Color> unitToColorFunction() {
        HSVColorGenerator hsvcg = new HSVColorGenerator();
        
        Function<X, Color> func = new Function<X, Color>() {
            @Override
            public Color apply(X t) {
                return hsvcg.nextColor();
            }
        };
                
        return new CachingFunctionWrapper(func);
    }
    
    /////////////////////
    private final float hueAdjust;

    private float hueCounter;
    private float valueCounter;
    private float saturationCounter;

    private boolean outOfColors;

    private final Random rnd;

    private HSVColorGenerator() {
        hueAdjust = 1.0f / ((float) DISTINCT_COLORS) * 360;

        hueCounter = 0 - hueAdjust; // Start before the first color
        valueCounter = VALUE_MAX; // At roof
        saturationCounter = 1; // At one

        outOfColors = false;
        rnd = new Random();
    }

    /**
     * Returns the next color for this class; synchronized to keep distinct order
     * @return Next color from this class
     */
    public synchronized Color nextColor() {
        if (outOfColors) {
            return Color.hsb(rnd.nextFloat(), rnd.nextFloat(), rnd.nextFloat()); // Out of colors, give up.
        }

        hueCounter += hueAdjust;
        if (hueCounter >= 360) {
            hueCounter = 0; // Reset to zero
            valueCounter -= VALUE_STEP;
            if (valueCounter < VALUE_MIN) {
                valueCounter = VALUE_MAX; // Alright, so value went below the floor, reset it
                saturationCounter -= SATURATION_STEP; // Reduce saturation
                if (saturationCounter < SATURATION_MIN) {
                    // We give up, we totally ran out of colors.
                    outOfColors = true;
                    return Color.hsb(rnd.nextFloat(), rnd.nextFloat(), rnd.nextFloat()); // Out of colors, give up.
                }
            }

        }
        
        return Color.hsb(hueCounter, saturationCounter, valueCounter);
    }

}
