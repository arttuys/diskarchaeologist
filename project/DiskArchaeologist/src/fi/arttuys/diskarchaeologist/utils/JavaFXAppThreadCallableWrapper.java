/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.utils;

import java.util.concurrent.Callable;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;
import javafx.application.Platform;

/**
 * A wrapper object that wraps a given Callable; in some cases, you may have to
 * execute something in the JavaFX application thread, asynchronously. This
 * class wraps a callable, and when called, uses runLater() to run it so that
 * any UI changes are doable.
 *
 * @date 26/5/2017
 * @author Arttu
 */
public final class JavaFXAppThreadCallableWrapper implements CallableWrapper {

    @Override
    public <T> Callable<T> wrapCallable(Callable<T> callable) {
        return new JavaFXAppThreadCallable<>(callable);
    }

    /**
     * Private subclass for the <code>Callable</code> wrapping
     * @param <T> Type of the callable
     */
    private static class JavaFXAppThreadCallable<T> implements Callable<T> {

        /**
         * The actual callable that shall be called
         */
        private final Callable<T> sourceCallable;

        /**
         * Semaphore signaling that our call is complete
         */
        private final Semaphore completionSemaphore;
        /**
         * Reference to an exception thrown by the callable
         */
        private final AtomicReference<Exception> exceptionReference;
        /**
         * Reference to the possible result
         */
        private final AtomicReference<T> resultReference;

        /**
         * Initializes a new call wrapper
         *
         * @param callable
         */
        private JavaFXAppThreadCallable(Callable<T> callable) {
            if (callable == null) {
                throw new IllegalArgumentException("Callable is null!");
            }
            this.sourceCallable = callable;

            //No permits
            this.completionSemaphore = new Semaphore(0);
            //Atomic references
            this.exceptionReference = new AtomicReference<>();
            this.resultReference = new AtomicReference<>();
        }

        /**
         * Call the wrapped Callable in the JavaFX Application Thread
         *
         * @return Any result provided by the Callable
         * @throws Exception Any exception that may have arose
         */
        @Override
        public T call() throws Exception {
            this.exceptionReference.set(null); //No value
            this.resultReference.set(null);

            //Add a new anonymous Runnable to the event queue
             Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        //Call, and attempt to set the result reference
                        resultReference.set(sourceCallable.call());
                    } catch (Exception e) {
                        //Save the exception
                        exceptionReference.set(e);
                    } finally {
                        //Declare completion, nevermind how it completed
                        completionSemaphore.release();
                    }
                }
            };
             
            if (Platform.isFxApplicationThread()) { //If we are already on the thread, run directly
                runnable.run();
            } else {
                Platform.runLater(runnable);
            }

            //Attempt to wait for completion
            completionSemaphore.acquire();
            
            //Did we have an exception?
            Exception e;
            if ((e = exceptionReference.get()) != null) {
                throw e; //Rethrow
            }

            //Return our result
            return resultReference.get();
        }
    }

}
