/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.utils;

import java.util.HashMap;
import java.util.function.Function;

/**
 *
 * A simple caching function wrapper; this basically memory-caches results for given keys, returning a result from a hash array if existent
 * <p>
 * It is expected that for same value, the backing function should return equivalent results - or alternatively, not cause side effects so that caching does not matter. This is not required to mean equality, but is
 * a good target for it. This mapper may also be used to for cases where it is desirable to have single initialized canonical result object
 * for a given key.
 * <p>
 * <strong>Be warned that this cache NEVER deletes any of the stored key-value pairs. Therefore, this cache is only suitable for situations where the range of keys is reasonably small and keys are used for a significant time</strong>
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 * @param <A> Type of the key
 * @param <B> Type of the result
 */
public class CachingFunctionWrapper<A,B> implements Function<A,B> {

    /**
     * The underlying function for this caching wrapper
     */
    private final Function<A,B> baseFunction;
    /**
     * The hashmap of the cache
     */
    private final HashMap<A, B> cacheMap;
    
    /**
     * Synchronization object for applying functions
     */
    private final Object synchLockObject = new Object();
    
    /**
     * Initializes a new caching wrapper
     * @param baseFunction The function that we want to cache
     */
    public CachingFunctionWrapper(Function<A,B> baseFunction) {
        this.baseFunction = baseFunction;
        this.cacheMap = new HashMap();
    }
    
    @Override
    public B apply(A t) {
        synchronized (synchLockObject) {
            B result;
            if (!cacheMap.containsKey(t)) {
                result = baseFunction.apply(t);
                cacheMap.put(t, result);
            } else {
                result = cacheMap.get(t);
            }
            
            return result;
        }
    }
    
}
