/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.utils;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.collections.ListChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.collections.transformation.TransformationList;

/**
 * 
 * A mapping list wrapper for JavaFX observable lists. It takes a simple function A : B, and transforms the backing list's contents
 * according to that function
 * 
 * Certain remarks to be heeded:
 * <ul>
 * <li>This class does not cache; any caching should be implemented in some other way, this list only asks the mapping function</li>
 *      <li>The mapping function should therefore be comparatively light, as depending on the purpose, there may be quite a bit of calls in a short while</li>
 * 
 *      <li>This is a read-only mapping - attempting to figure out an inverse function mechanism would be complicated - and not required</li>
 * </ul>
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 * @param <A> Type of the base list
 * @param <B> Type of the target
 */
public class MappingList<A,B> extends TransformationList<B, A> {
    
    /**
     * Private reference to the mapping function
     */
    private final Function<A,B> transformationFunction;
    
    /**
     * Create a new mapping list, using a given function to map values from type A to B
     * @param baseList Backing list; the values of this list are given to the mapping function to produce the items of this list
     * @param mapper Mapping function; generates items for this list as called
     */
    public MappingList(ObservableList<? extends A> baseList, Function<A,B> mapper) {
        //Initialize superclass
        super(baseList);
        
        //Require that mapper be non-null
        if (mapper == null) throw new IllegalArgumentException("Mapper may not be null!");
        this.transformationFunction = mapper;
    }
    
    /**
     * A method for evaluating the mapping function. This is mostly intended for subclasses which wish to override the default behavior
     * @param key Parameter for the function
     * @return Result
     */
    protected B evaluate(A key) {
        return transformationFunction.apply(key);
    }
    
    /**
     * The backing list has changed in some way; here, we must forward the event in a way that describes the action in terms of
     * this list.
     * @param c Event from original list
     */
    @Override
    protected void sourceChanged(ListChangeListener.Change<? extends A> c) {
        //We need to fire a new change event, as our transformation does affect what the observer of this list should see!
        fireChange(new Change<B>(this) {
            @Override
            public boolean next() {
                return c.next(); //Ordering not affected
            }

            @Override
            public void reset() {
                c.reset(); //Ordering not affected
            }

            @Override
            public int getFrom() {
                return c.getFrom(); //Ordering not affected
            }

            @Override
            public int getTo() {
                return c.getTo(); //Ordering not affected
            }

            @Override
            public List<B> getRemoved() {
                //Let's utilize functional programming.
                Stream<? extends A> sourceRemoved = c.getRemoved().stream();
                Stream<B> resultStream = sourceRemoved.map((x) -> evaluate(x));
                
                //And tada, to a list!
                return resultStream.collect(Collectors.toList());
            }

            @Override
            protected int[] getPermutation() {
                throw new AssertionError("Should not be reached, not used anywhere where this method is accessible");
            }

            @Override
            public int getPermutation(int i) {
                return c.getPermutation(i); //Ordering follows the source list
            }

            @Override
            public int getAddedSize() {
                return c.getAddedSize(); //Count follows source list
            }

            @Override
            public int getRemovedSize() {
                return c.getRemovedSize(); //Count follows source list
            }

            @Override
            public List<B> getAddedSubList() {
                //Observe that since we have added them to this list, we can transparently map it using existing methods! Unlike with removed items
                return this.getList().subList(this.getFrom(), this.getTo()); //As referenced in Javadoc; our transformation affects this method
            }

            @Override
            public boolean wasUpdated() {
                return c.wasUpdated();
            }

            @Override
            public boolean wasReplaced() {
                return c.wasReplaced();
            }

            @Override
            public boolean wasRemoved() {
                return c.wasRemoved();
            }

            @Override
            public boolean wasAdded() {
                return c.wasAdded();
            }

            @Override
            public boolean wasPermutated() {
                return c.wasPermutated();
            }

    });
    }

    @Override
    public int getSourceIndex(int index) {
        //If this index is valid here, it is also on the source.
        return index;
    }

    @Override
    public B get(int index) {
        //Evaluate our function, and return the result
        return evaluate(this.getSource().get(index));
    }

    @Override
    public int size() {
        //Our transformation is linear
        return this.getSource().size();
    }

}
