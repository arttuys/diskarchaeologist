/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.gui;

import fi.arttuys.diskarchaeologist.scan.node.FSNode;
import fi.arttuys.diskarchaeologist.utils.CachingFunctionWrapper;
import fi.arttuys.diskarchaeologist.utils.MappingList;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

/**
 *
 * A special type of a TreeItem that dynamically generates children nodes from a base FSNode
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class FSNTreeItem extends TreeItem<FSNode> {

    /**
     * The base FSNode for this item
     */
    private final FSNode node;
    
    /**
     * As getChildren() may be called often, this variable stores a soft reference to the list
     */
    private volatile ObservableList<TreeItem<FSNode>> cachedList;
    
    /**
     * Creates a new FSNTreeItem, a specialized tree item designed to bind into FSNodes and automatically expose subnodes where available
     * @param node File node to be bound to this TreeItem
     */
    protected FSNTreeItem(FSNode node) {
        super(node);
        this.node = node;
        if (node == null) throw new IllegalArgumentException("Node must not be null!");
        cachedList = null;
        initializeChildListener();
    }
    
    /**
     * Initialize the caching list, and bind its changes to the changes of our designated children list
     */
    private void initializeChildListener() {
        // Create a special cached list; a list that maps subnodes directly to tree item  nodes, and a function that implicitly caches them so that same node is always the same item
        cachedList = new MappingList<>(node.getSubnodeProperty(), new CachingFunctionWrapper<>(x -> new FSNTreeItem(x)));
        ObservableList<TreeItem<FSNode>> baseList = super.getChildren();
        
        //Add listener for change/***
        cachedList.addListener(new ListChangeListener<TreeItem<FSNode>>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends TreeItem<FSNode>> c) {
                while (c.next()) {
                    if (c.wasAdded()) {
                        baseList.addAll(c.getAddedSubList());
                    }
                    if (c.wasRemoved()) {
                        baseList.removeAll(c.getRemoved());
                    }
                    
                    if (c.wasUpdated()) {
                        //This is quite complicated to handle in the most compact fashion. This event is also expected to be quite rare.
                        //It is a fair tradeoff in complexity to simply instruct the whole list to be refreshed
                        
                        //Simple solution:
                        baseList.setAll(cachedList);
                    }
                }
            }
        });
        
        //Set the list to its current state
        baseList.setAll(cachedList);
    }
    
   
    
    /**
     * Returns the children of this list. While the process of enumerating the children is a bit complicated, it is transparently handled elsewhere and therefore this suffices.
     * @return The list of subnodes
     */
    @Override
    public ObservableList<TreeItem<FSNode>> getChildren() {
        return super.getChildren();
    }

    /**
     * When given a stack, this method locates the item on the top of the stack, and returns it
     * @param stack Stack of nodes; on the top the next child being located
     * @return The bottom of the
     */
    protected TreeItem<FSNode> locateChild(Stack<FSNode> stack) {
        if (stack.empty()) return this; // Stack is empty, this is the one we want
        
        // Pop the one on the top
        FSNode node = stack.pop();
        TreeItem<FSNode> nextItem = this.getChildren().stream().filter((t) -> {
            return t.getValue() == node;
        }).findAny().orElse(null); // We can fairly safely assume that no duplicate elements should exist
        
        if (nextItem == null || !(nextItem instanceof FSNTreeItem)) return null; // If it turns out we can not determine the correct next element, just return null. Also the same if it turns out the next element does not support our search feature for some reason
        this.setExpanded(true);
        return ((FSNTreeItem)nextItem).locateChild(stack); // Otherwise, if all seems fine, just search further
    }
    
    /**
     * Triggers a cascading revealing operation as bound by a FSNode, and then finally returns the TreeItem found at the end
     * @param n The node to reveal
     * @return The tree item corresponding to the file given
     */
    public TreeItem<FSNode> revealSubnode(FSNode n) {
        if (n == null) return null; // No node, no item
        if (n.equals(this.node)) return this;
        
        // Now, try to construct a path from the node to this upper node
        Stack<FSNode> nodeStack = new Stack<>();
        nodeStack.add(n);
        
        FSNode parentNode = n.getParentNode();
        boolean connectionFound = false;
        while (parentNode != null) {
            if (parentNode == this.node) {
                connectionFound = true;
                break;
            }
            
            // Add to the stack
            nodeStack.push(parentNode);
            parentNode = parentNode.getParentNode();
        }
        
        if (!connectionFound) return null; // We were unable to locate a path
        
        return locateChild(nodeStack); // Hand over to the internal method doing the actual searching
    }
    
    /**
     * Returns TRUE if this is a leaf node (no subnodes possible). We can fairly safely assume that most directories aren't leaf nodes, and that all files are.
     * @return TRUE if yes (a file), FALSE if no (a directory)
     */
    @Override
    public boolean isLeaf() {
        return !this.node.isDirectory();
    }
    
}
