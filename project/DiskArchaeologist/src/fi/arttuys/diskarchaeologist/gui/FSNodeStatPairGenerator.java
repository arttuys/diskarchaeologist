/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.gui;

import fi.arttuys.diskarchaeologist.scan.node.FSNode;
import fi.arttuys.diskarchaeologist.scan.node.FTMConstants;
import fi.arttuys.diskarchaeologist.scan.search.AnalysisResultObject;
import fi.arttuys.diskarchaeologist.scan.search.AnalysisResultObject.AnalysisEntry;
import fi.arttuys.diskarchaeologist.scan.search.FileTypeRecognizer;
import java.io.File;
import javafx.scene.control.TreeItem;
import javafx.util.Pair;

/**
 *
 * A helper static class for generating information for the Stats display. This class generates premade <code>TreeItem</code>s with children; the data type in question is a simple JavaFX <code>Pair</code>, <code>String</code> to <code>String</code>.
 * It is up to the GUI model to actually display the data
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class FSNodeStatPairGenerator {
    private FSNodeStatPairGenerator() {
        // Solely to prevent instantiation
    }
    
    private static TreeItem<Pair<String,String>> pairToTreeItem(Pair<String, String> pair) {
        if (pair == null) throw new IllegalArgumentException("Pair must not be null!");
        return new TreeItem<>(pair);
    }
    
    private static TreeItem<Pair<String,String>> pairToTreeItem(String key, String val) {
        return pairToTreeItem(new Pair<>(key,val));
    }
    
    /**
     * Returns a new statistic-containing tree node for a given <code>FSNode</code>. These nodes do NOT observe the contents of a given <code>FSNode</code>, nor does this method change anything, so nominally this is safe to call from any thread
     * @param fsn Node
     * @return A tree item
     */
    public static TreeItem<Pair<String,String>> nodeToTreeItem(FSNode fsn) {
        TreeItem<Pair<String,String>> rootNode = pairToTreeItem(new Pair(fsn.getUnderlyingFile().getAbsolutePath(), ""));
        //First, general statistics
        try {
            File file = fsn.getUnderlyingFile();
                
            TreeItem<Pair<String,String>> fileNode = pairToTreeItem("File", "");
            fileNode.getChildren().add(pairToTreeItem("Name", file.getName()));
            fileNode.getChildren().add(pairToTreeItem("Path", file.getPath()));
            fileNode.getChildren().add(pairToTreeItem("Size in bytes on file system", Long.toString(file.length())));
            fileNode.getChildren().add(pairToTreeItem("Size including contents", fsn.getTotalSizeProperty().get().toString()));
            
            // Next, search query status
            if (fsn.getReadOnlyExtendedPropertyMap().get().containsKey(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY)) {
                Object o = fsn.getReadOnlyExtendedPropertyMap().get().get(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY);
                fileNode.getChildren().add(pairToTreeItem("Matches current search query?", o == null || !o.equals(true) ? "False" : "True"));
            } else {
                fileNode.getChildren().add(pairToTreeItem("Matches current search query?", "Undefined"));
            }
            
            rootNode.getChildren().add(fileNode);
            fileNode.setExpanded(true); // Expand this open
        } catch (Exception e) {
            
            // Catch any errors to prevent ungraceful crashes; instead, signal gracefully that there is something off
            rootNode.getChildren().add(pairToTreeItem(new Pair<>("File", "Unable to generate further statistics: " + e.toString())));
        }
        
        // Next, other collated statistics
        
        try {
            Object obj = fsn.getReadOnlyExtendedPropertyMap().get().get(FTMConstants.FSN_ANALYSIS_RESULT_OBJECT_KEY);
            if (obj == null || !(obj instanceof AnalysisResultObject)) {
                rootNode.getChildren().add(pairToTreeItem("Analysis", "No analysis results available"));
                // Stop here, nothing to do
            } else {
                TreeItem<Pair<String,String>> analysisNode = pairToTreeItem("Analysis", "");
                
                AnalysisResultObject aro = (AnalysisResultObject)obj;
                
                analysisNode.getChildren().add(pairToTreeItem("Result type", aro.getResultType().toString()));
                if (aro.getResultType() != FileTypeRecognizer.AnalysisResultType.ANALYSIS_ERROR && aro.getResultType() != FileTypeRecognizer.AnalysisResultType.FILE_REJECTED) {
                    analysisNode.getChildren().add(pairToTreeItem("Type identified", aro.getTypeIdentifier()));
                    if (aro.getResultType() != FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_POTENTIAL && aro.getProperties() != null) {
                        // Add more precise information if available
                        TreeItem<Pair<String,String>> subdataNode = pairToTreeItem("Fields", "");
                        
                        // This is a bit complicated, as identifiers also hold category data
                        for (AnalysisEntry key : aro.getProperties().keySet()) {
                            TreeItem<Pair<String,String>> entryNode = pairToTreeItem(key.getIdentifier(), "");
                            entryNode.getChildren().add(pairToTreeItem("Category", key.getCategory() == null ? "None" : key.getCategory()));
                            entryNode.getChildren().add(pairToTreeItem("Value", aro.getProperties().get(key).toString()));
                            
                            // Expand this node
                            entryNode.setExpanded(true);
                            //Add it to the subdata node
                            subdataNode.getChildren().add(entryNode);
                        }
                        // Expand this node
                        subdataNode.setExpanded(true);
                        
                        // Add to the analysis root
                        analysisNode.getChildren().add(subdataNode);
                    }
                }
                
                rootNode.getChildren().add(analysisNode);
                analysisNode.setExpanded(true);
            }
        } catch (Exception e) {
            rootNode.getChildren().add(pairToTreeItem("Analysis", "Unable to display: " + e.toString()));
        }
        
        // Finally, also expand the root node
        rootNode.setExpanded(true);
        
        
        
        return rootNode;
    }
}
