/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.gui;

import java.io.IOException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * The main class for DiskArchaeologist, the main place where everything starts
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class DiskArchaeologistFXMain extends Application {
    
    /**
     * Starts the JavaFX application
     * @param primaryStage The stage as given by the JavaFX framework
     */
    @Override
    public void start(Stage primaryStage) {
        
        try {
            
            FXMLLoader ldr = new FXMLLoader(this.getClass().getResource("DiskArchaeologistMainWindow.fxml"));
            BorderPane rootPane = (BorderPane)ldr.load();
            
            DiskArchaeologistMainWindowController ctrl = (DiskArchaeologistMainWindowController)ldr.getController();
            //Initialize the scene
            Scene scene = new Scene(rootPane);
            
            primaryStage.setScene(scene);
            primaryStage.setTitle("DiskArchaeologist");
            primaryStage.show();
            
            // To ensure that other threads quit as well, force JVM to exit upon closing of the main window
            primaryStage.setOnHidden((event) -> { System.exit(0);
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Entry point for the program; simply launch the JavaFX parts
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
