/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.gui;

import fi.arttuys.diskarchaeologist.scan.node.FSNode;
import fi.arttuys.diskarchaeologist.scan.node.FTMConstants;
import fi.arttuys.diskarchaeologist.scan.node.FileGrid;
import fi.arttuys.diskarchaeologist.scan.search.AnalysisResultObject;
import fi.arttuys.diskarchaeologist.scan.search.FileTypeRecognizer;
import javafx.scene.canvas.Canvas;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.util.Pair;

/**
 * Special grid rendering control for showing the grid boxes.
 *
 * While JavaFX does offer special skin and such functionality, a simpler
 * approach was taken as shown here <a href="http://docs.oracle.com/javafx/2/fxml_get_started/custom_control.htm">in the official JavaFX manual</a>
 *
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class GridRendererControl extends AnchorPane {

    /**
     * Color to be shown when there's no file on a grid
     */
    public static final Color EMPTY_COLOR = Color.BLACK;

    /**
     * Color to be shown when there is a file on the grid, but its type can not
     * be clearly determined
     */
    public static final Color INDETERMINANT_TYPE_COLOR = Color.GAINSBORO;

    /**
     * To indicate separate files, the pixels will gradually fade. This
     * indicates how much the factor is.
     */
    public static final float VALUE_ADJUSTMENT_FACTOR = 0.3f;

    @FXML
    private Label notRenderingLabel;

    /**
     * The canvas used to render
     */
    private Canvas renderCanvas;

    /**
     * The relevant grid for the current grid
     */
    private FileGrid grid;

    /**
     * We need to retain the original colors for each pixel
     */
    private Color[][] positionToOriginalColor;

    /**
     * The current selector for highlighting items
     */
    private Predicate<FSNode> selector;

    /**
     * The click handler that should be assigned to all canvases
     */
    private final EventHandler<MouseEvent> clickHandler;
    
    /**
     * This property holds the latest file clicked
     */
    private final ReadOnlyObjectWrapper<FSNode> clickedFileProperty;
    
    /**
     * Creates a new GridRenderer control
     */
    public GridRendererControl() {
        super();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GridRenderer.fxml"));
        
        // Initialize the FXMLLoader
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        renderCanvas = null;
        positionToOriginalColor = null;
        selector = null;
        
        clickHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getButton() != MouseButton.PRIMARY || event.getClickCount() != 1) return; // Not a proper click
                onGridClick((int)event.getX(), (int)event.getY());
            }
        };
        
        clickedFileProperty = new ReadOnlyObjectWrapper<>(null);
    }

    /**
     * Trigger a change in the latest clicked file property, when the user clicks on the grid
     * @param x X-location
     * @param y Y-location
     */
    private void onGridClick(int x, int y) {
        FSNode n = this.grid.pointAtGrid(x, y);
        this.clickedFileProperty.set(n);
    }
    
    /**
     * Invalidates the current grid, and hides the canvas
     */
    public synchronized void invalidateCurrentGrid() {
        invalidateSizeInternal();
    }

    private void invalidateSizeInternal() {
        if (renderCanvas != null) {
            renderCanvas.setOnMouseClicked(null);
            this.getChildren().remove(this.renderCanvas);
        }
        renderCanvas = null;
        positionToOriginalColor = null;
        grid = null;
        notRenderingLabel.setVisible(true);
    }

    /**
     * Populates the grid of original colors
     *
     * @param colorFunction
     */
    private void populateColorGrid(Function<String, Color> colorFunction) {
        this.positionToOriginalColor = new Color[grid.getGridWidth()][grid.getGridHeight()];

        // Preinitialize to empty.
        for (int x = 0; x < grid.getGridWidth(); x++) {
            for (int y = 0; y < grid.getGridHeight(); y++) {
                positionToOriginalColor[x][y] = EMPTY_COLOR; // Nothing here, empty
            }
        }

        // Populate per file
        for (FSNode node : grid.getNodeToGridMap().keySet()) {
            Object obj = node.getReadOnlyExtendedPropertyMap().get(FTMConstants.FSN_ANALYSIS_RESULT_OBJECT_KEY);
            Color commonColor = INDETERMINANT_TYPE_COLOR;
            if (obj != null && (obj instanceof AnalysisResultObject)) {

                AnalysisResultObject aro = (AnalysisResultObject) obj;
                if (!(aro.getResultType() == FileTypeRecognizer.AnalysisResultType.ANALYSIS_ERROR || aro.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_REJECTED)) {

                    String typeIdentifier = aro.getTypeIdentifier();
                    commonColor = colorFunction.apply(typeIdentifier);

                }
            }

            // Now, populate the grid. Let's calculate the starting color
            List<Pair<Integer, Integer>> listOfPixels = grid.getNodeToGridMap().get(node);
            double hue = commonColor.getHue();
            double saturation = commonColor.getSaturation();
            double value = Math.min(commonColor.getBrightness() + VALUE_ADJUSTMENT_FACTOR, 1);

            double reductionPerTurn = VALUE_ADJUSTMENT_FACTOR / ((double) Math.max(1, listOfPixels.size() - 1));

            for (int i = 0; i < listOfPixels.size(); i++) {
                Pair<Integer, Integer> pos = listOfPixels.get(i);
                positionToOriginalColor[pos.getKey()][pos.getValue()] = Color.hsb(hue, saturation, value);
                value -= reductionPerTurn;
            }

        }
    }

    /**
     * Actually repaints the grid
     */
    private void renderGrid() {
        // Start rendering

        GraphicsContext gc = renderCanvas.getGraphicsContext2D();

        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, renderCanvas.getWidth(), renderCanvas.getHeight());

        for (int ix = 0; ix < grid.getGridWidth(); ix++) {
            for (int iy = 0; iy < grid.getGridHeight(); iy++) {
                FSNode node = grid.pointAtGrid(ix, iy);

                Color c = positionToOriginalColor[ix][iy];
                
                // First, determine the selector highlight
                if (node != null) {
                    if (selector != null && selector.test(node)) c = c.interpolate(Color.WHITE, 0.75);          

                // Also, treat the global search query as a separate highlight. Be aware that it may be a null, so compare this way
                    if (Boolean.TRUE.equals(node.getReadOnlyExtendedPropertyMap().get(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY))) c = c.interpolate(Color.WHITE, 0.9);
                }
                gc.setFill(c);
                gc.fillRect(ix, iy, 1, 1);
            }
        }
    }

    /**
     * Updates the selector function, and rerenders the grid. Must be called
     * from the JavaFX thread!
     * <br>
     * The selector function is ran directly on the rendering thread, so the function should not take excess amounts of time
     * <br>
     * @param selector The function indicating which files should be displayed as selected.
     */
    public synchronized void updateSelectorFunction(Predicate<FSNode> selector) {
        this.selector = selector;
        if (renderCanvas != null) {
            renderGrid();
        }
    }

    /**
     * Returns the property describing the latest file clicked; it may be useful to bind to this, to cause UI changes elsewhere in the program in sync
     * @return The property; it may be NULl with no selection, or it may point to some FSNode
     */
    public ReadOnlyObjectProperty<FSNode> getClickedFileProperty() {
        return this.clickedFileProperty.getReadOnlyProperty();
    }
    
    /**
     * Simply requests to redraw the grid
     */
    public synchronized void redrawGrid() {
        if (renderCanvas != null) renderGrid();
    }
    
    /**
     * Requests to re-render the grid using the provided size, grid, and
     * color-function. The rendering size does not necessarily have to be the same as in the file grid, but it is recommended!
     *
     * @param x X-dimension of the grid
     * @param y Y-dimension of the grid
     * @param fg The file grid
     * @param typeToColor The color function for a given type identifier
     */
    public synchronized void generateAndRenderGrid(int x, int y, FileGrid fg, Function<String, Color> typeToColor) {
        invalidateSizeInternal();

        if (x < 1 || y < 1) {
            return; //Zero size, nothing to be done
        }
        if (fg == null || typeToColor == null) {
            throw new IllegalArgumentException("A valid grid must be provided, and a color function must be not-null!");
        }

        // Create a canvas
        renderCanvas = new javafx.scene.canvas.Canvas(x, y);
        renderCanvas.onMouseClickedProperty().setValue(clickHandler);
        // Populate internal variables
        this.grid = fg;
        populateColorGrid(typeToColor);

        // First render
        renderGrid();

        // Set anchor pane constraints, and add
        AnchorPane.setBottomAnchor(renderCanvas, 0.0);
        AnchorPane.setTopAnchor(renderCanvas, 0.0);
        AnchorPane.setLeftAnchor(renderCanvas, 0.0);
        AnchorPane.setRightAnchor(renderCanvas, 0.0);

        notRenderingLabel.setVisible(false);
        this.getChildren().add(renderCanvas);

    }
}
