/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.gui;

import fi.arttuys.diskarchaeologist.scan.node.FSNode;
import java.io.File;
import java.util.concurrent.Callable;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TreeTableView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;

/**
 * FXML Controller class. This is the more view-oriented part of the user interface code, as compared to {@link FTMGUIModel}
 *
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 * @see FTMGUIModel
 */
public class DiskArchaeologistMainWindowController {

    @FXML
    private AnchorPane treeviewParentAnchorPane;

    @FXML
    private AnchorPane renderParentAnchorPane;

    @FXML
    private AnchorPane statAnchorPane;

    @FXML
    private AnchorPane fileTypeAnchorPane;

    @FXML
    private Label statusTextBar;

    @FXML
    private Button selectFolderBtn;

    @FXML
    private Button deepAnalysisBtn;

    @FXML
    private Button interruptBtn;

    //Dynamically initialized controls
    /**
     * TreeTableView for objects
     */
    private TreeTableView<FSNode> fileTreeDisplayer;

    /**
     * TableView for file types
     */
    private TableView fileTypeDisplayer;
    /**
     * TreeTableView for statistics
     */
    private TreeTableView statsDisplayer;

    /**
     * Special grid control for visuals
     */
    private GridRendererControl gridDisplayer;

    /**
     * Stores the root file tree manager
     *
     */
    private FTMGUIModel rootFtm;

    @FXML
    private TextArea querySearchBox;

    @FXML
    private Button beginQueryButton;

    /**
     * Initializes the core functionality of this controller; as this class contains a considerable amount of dynamically instantiated stuff, <code>initialize</code> is also bound to be rather large
     */
    @FXML
    void initialize() {

        //Initialize the model class
        rootFtm = new FTMGUIModel();
        //Get the FTMGUIModel's provisioned dynamic elements
        fileTreeDisplayer = rootFtm.getFileTreeDisplayer();
        fileTypeDisplayer = rootFtm.getTypeDisplayer();
        statsDisplayer = rootFtm.getStatisticsView();
        gridDisplayer = rootFtm.getGridRenderer();

        for (Node n : new Node[]{fileTreeDisplayer, fileTypeDisplayer, statsDisplayer, gridDisplayer}) {
            AnchorPane.setTopAnchor(n, 0.0);
            AnchorPane.setBottomAnchor(n, 0.0);
            AnchorPane.setLeftAnchor(n, 0.0);
            AnchorPane.setRightAnchor(n, 0.0);
        }

        // Add dynamic elements
        this.treeviewParentAnchorPane.getChildren().add(fileTreeDisplayer);
        this.fileTypeAnchorPane.getChildren().add(fileTypeDisplayer);
        this.statAnchorPane.getChildren().add(statsDisplayer);
        this.renderParentAnchorPane.getChildren().add(gridDisplayer);

        // Create a new resize listener
        InvalidationListener gridSizeChanged = new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                rootFtm.triggerGridRecalculation();
            }
        };
        
        //Add resize triggers to the grid panel
        this.renderParentAnchorPane.widthProperty().addListener(gridSizeChanged);
        this.renderParentAnchorPane.heightProperty().addListener(gridSizeChanged);
        
        //Bind the status bar text
        statusTextBar.textProperty().bind(rootFtm.getStateString());

        //Bind the enabledness of Interrupt to the state
        //First, we need to generate an inverse property though
        BooleanBinding isProcessingBinding = rootFtm.getProcessingProperty();
        BooleanBinding isNotProcessingBinding = Bindings.createBooleanBinding(new Callable() {
            @Override
            public Object call() throws Exception {
                return !isProcessingBinding.get();
            }
        }, isProcessingBinding);

        BooleanBinding isNotPostprocessingBinding = Bindings.createBooleanBinding(() -> {
            return !rootFtm.getPostprocessingReadyProperty().get();
        }, rootFtm.getPostprocessingReadyProperty());

        // Bind disable properties so that buttons disable themselves automatically where not applicable
        interruptBtn.disableProperty().bind(isNotProcessingBinding);
        selectFolderBtn.disableProperty().bind(isProcessingBinding);
        deepAnalysisBtn.disableProperty().bind(isNotPostprocessingBinding);

        // Also bind the search features
        beginQueryButton.disableProperty().bind(isNotPostprocessingBinding);
        querySearchBox.disableProperty().bind(isNotPostprocessingBinding);

    }

    /**
     * Upon clicking "interrput", the currently ongoing task should be
     * interrupted
     *
     * @param event N/A
     */
    @FXML
    void onInterruptMenuItem(ActionEvent event) {
        try {
            rootFtm.requestInterruption(false);
        } catch (InterruptedException ie) {
            //
        }
    }

    /**
     * Upon clicking "search", a search query should be executed
     * @param event N/A
     */
    @FXML
    void onSearchQuery(ActionEvent event) {
        // Trigger a search query from the model
        rootFtm.triggerSearchQuery(this.querySearchBox.getText());
    }

    /**
     * Upon clicking "deep analyze", a deeper analysis task should be started
     * @param event N/A
     */
    @FXML
    void onDeepAnalysis(ActionEvent event) {
        // Trigger analysis from the model
        rootFtm.startDeepAnalysis();
    }

    /**
     * On clicking "open folder...", a folder should be inquired and then a scan started there
     * @param event N/A
     */
    @FXML
    void onScanAndEnumerate(ActionEvent event) {
        try {
            // Create a new directory chooser
            DirectoryChooser dirChooser = new DirectoryChooser();

            File f = dirChooser.showDialog(null);
            if (f == null) {
                return;
            }

            // All seems fine. Try to initialize the enumeration now
            rootFtm.initiateFileEnumeration(f);
        } catch (Exception ex) {
            showException(ex);
        }
    }

    /**
     * Simple wrapper to displaying an exception as an error messsage. 
     * <br>
     * There may be occasions that unexpected exceptions propagate up, so we need a nice
     * way to display them.
     *
     * @param e Exception to show
     */
    private void showException(Exception e) {
        showError("Unexpected exception", String.format("Oops, something did not go quite right. Following exception was thrown: %s, with cause: %s. It may be advisable to restart the program, as this situation was not explicitly handled otherwise by the programmer.", e.getClass().toGenericString(), e.getCause()));
    }

    /**
     * A simple wrapper to show an error
     *
     * @param header Header of the exception
     * @param description Description
     */
    private void showError(String header, String description) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.headerTextProperty().set(header);
        alert.contentTextProperty().set(description);
        alert.showAndWait(); //As there is only one choice, do not care
    }
}
