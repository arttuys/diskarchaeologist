/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.gui;

import fi.arttuys.diskarchaeologist.scan.node.FSNode;
import fi.arttuys.diskarchaeologist.scan.node.FTMConstants;
import fi.arttuys.diskarchaeologist.scan.node.FileTreeManager;
import fi.arttuys.diskarchaeologist.scan.search.AnalysisResultObject;
import fi.arttuys.diskarchaeologist.scan.search.FileTypeRecognizer;
import fi.arttuys.diskarchaeologist.utils.HSVColorGenerator;
import fi.arttuys.diskarchaeologist.utils.JavaFXAppThreadCallableWrapper;
import fi.arttuys.diskarchaeologist.utils.SizeToStringConverter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.WeakInvalidationListener;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Bounds;
import javafx.scene.control.Alert;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.util.Pair;

/**
 *
 * A helper class intended to bridge the interface as presented by the
 * FileTreeManager, and the UI representation of the results. This class is
 * intended to, for instance:
 * <ul>
 * <li>Ensure that FTM utilizes proper threads, and that any alterations to the
 * tree are properly reflected in the user interface</li>
 * <li>Provide easy-to-bind data sources for the user interface</li>
 * <li>Provide simple methods for different user interface operations (e.g start
 * scan, analyze file)</li>
 * </ul>
 * <br>
 * It is analogous to the viewmodel element seen in C# programs; it sits between
 * the model (in this case, FileTreeManager instances), and the view
 * (DiskArchaeologistMainWindow)
 * <br>
 * It is assumed that this class is only to be interacted directly within the
 * JavaFX application thread - any multithreaded usage should take this into
 * consideration! Also, it is safe to assume that all observable properties only
 * change in the JavaFX thread
 * <br>
 * Class is package private, due to nobody else needing to have direct access to
 * it
 *
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 * @see DiskArchaeologistMainWindowController
 */
class FTMGUIModel {

    /**
     * The observable string wrapper for the status bar string
     */
    private final ReadOnlyStringWrapper stateString;
    /**
     * The file tree displayer
     */
    private TreeTableView<FSNode> fileTreeDisplayer;
    /**
     * The statistics displayer
     */
    private TreeTableView<Pair<String, String>> statisticsDisplayer;
    /**
     * The type table component
     */
    private TableView<FSNodeTypeStatModel> typeTable;

    /**
     * The grid renderer component
     */
    private GridRendererControl gridRenderer;
    /**
     * The current FileTreeManager instance
     */
    private volatile FileTreeManager ftm;
    /**
     * A property to indicate if we are processing something; useful for GUI
     * bindings
     */
    private final BooleanBinding isProcessingProperty;

    /**
     * A property that indicates if we can start any postprocessing operations
     * now
     */
    private final BooleanBinding isPostprocessingReadyProperty;

    /**
     * This flag is set when the types should be updates visually after the
     * latest operation finishes
     */
    private volatile boolean needsToUpdateTypes = true;

    /**
     * This flag is set when there is a distinct need to update grid data, but
     * it can not be, for some reason, triggered directly after a type update
     */
    private volatile boolean needsToUpdateGrid = false;

    /**
     * The translator function from types to colors
     */
    private final Function<String, Color> typeColorTranslator;

    /**
     * It seems that JavaFX selections behave differently depending on which way
     * the selection was invoked. This variable forces the updating function to
     * disregard whatever the tree view itself tells, and updates according to
     * it.
     */
    private volatile FSNode gridOverrideNode;

    /**
     * The initializer for this class; this initializes most of the dynamic
     * properties as utilized by {@link DiskArchaeologistMainWindowController}
     */
    FTMGUIModel() {
        stateString = new ReadOnlyStringWrapper("Idle");
        typeColorTranslator = HSVColorGenerator.unitToColorFunction(); // Initialize a coloring function
        gridOverrideNode = null;

        prepareGridRenderer();
        prepareTreeView(); //Prepare the TreeView
        prepareTypeView();
        prepareStatisticsView();

        ftm = null; //By default, no FTM is to be prepared

        //Indicates if there's work currently going on
        isProcessingProperty = new BooleanBinding() {
            @Override
            protected boolean computeValue() {
                return !isCleanlyIdle(true);
            }
        };

        isPostprocessingReadyProperty = Bindings.createBooleanBinding(() -> {
            return ftm != null && isCleanlyIdle(false);
        }, isProcessingProperty); // Bind this as a dependent of isProcessingProperty, as this is a special case of it

    }

    /**
     * A helper class that binds to two String-resulting properties, primary and
     * secondary. Preferably, the primary property is used; but if it has no
     * value, then the secondary one is used
     * <br>
     * The class also has the convenient property that while it accepts any sort
     * of an observable value, it implicitly converts them to strings using the
     * inbuilt <code>toString()</code> method. <code>NULL</code>s are implictly
     * represented as "NULL"
     * <br>
     * As a consequence of this approach, this class also disregards the types
     * that may be bound to <code>ObservableValue</code>s
     */
    private static class AlternativeValueObjectStringBinding extends StringBinding {

        private final ObservableValue primary, secondary;
        private boolean isCurrentlyValid;

        private String cachedValue;

        private final InvalidationListener invalidationListener;

        /**
         * Initialize a new alternative-value binding
         *
         * @param primary The primary value that should be shown when called
         * @param secondary If the primary value is not available, show this
         * secondary value instead
         */
        public AlternativeValueObjectStringBinding(ObservableValue primary, ObservableValue secondary) {
            this.primary = primary;
            this.secondary = secondary;

            //Not valid from the start
            this.isCurrentlyValid = false;
            this.cachedValue = null;

            //Generate an invalidation listener that invalidates our value in case of changes
            invalidationListener = new InvalidationListener() {
                @Override
                public void invalidated(Observable observable) {
                    AlternativeValueObjectStringBinding.this.invalidate();
                }
            };

            //Generate a weak invalidation listener that will not retain a binding to this object, if it has fallen out of scope. A practical consequence is that
            //once all bindings utilizing this class have been eliminated, the listeners will also automatically remove themselves, avoiding memory leaks
            WeakInvalidationListener wil = new WeakInvalidationListener(invalidationListener);

            //Add those listeners.
            primary.addListener(wil);
            secondary.addListener(wil);
        }

        private String convertToString(Object o) {
            return o != null ? o.toString() : "NULL";
        }

        private void refreshValueInternal() {
            Object primaryValue = primary.getValue();
            cachedValue = convertToString(primaryValue != null ? primaryValue : secondary.getValue());

            isCurrentlyValid = true;
        }

        @Override
        protected String computeValue() {
            synchronized (this) { //Prevent multithreaded race conditions by ensuring that calculations are made only one at a time
                if (!isCurrentlyValid) {
                    refreshValueInternal();
                }
            }
            return cachedValue; //At this point, we always have a valid value
        }

    }

    /**
     * A model to use for binding type statistics to a table view. A random
     * color is also implicitly created for any new type stat
     */
    public static class FSNodeTypeStatModel {

        private final SimpleStringProperty typeName;
        private final SimpleIntegerProperty count;

        /**
         * Initialize a new type statistic
         *
         * @param typeName Name of the type
         */
        private FSNodeTypeStatModel(String typeName) {
            this.typeName = new SimpleStringProperty(typeName);
            this.count = new SimpleIntegerProperty(0);
        }

        /**
         * Get the name of the type
         *
         * @return Type
         */
        public String getTypeName() {
            return this.typeName.get();
        }

        /**
         * Get the current count of hits on this type
         *
         * @return Current count of hits (zero if none)
         */
        public int getCount() {
            return this.count.get();
        }

        /**
         * Increment the hit counter
         */
        public synchronized void incrementCount() {
            this.count.set(this.count.getValue() + 1);
        }
    }

    /**
     * This is called when the type statistics change.
     * <br>
     * This both updates the type table, which is the canonical storage for the
     * type statistics, and may also ask other elements to update as well. Also,
     * as the type statistics change, it also means that the grid changes.
     */
    private void updateTypes() {
        needsToUpdateTypes = false;
        needsToUpdateGrid = true;
        updateTypeTable();
        updateStatViewAndSingleSelection(false); // Update statistical view, this has likely changed
    }

    /**
     * Triggers a grid recalculation - either wait until we have became idle -
     * or if we are idle, ask FTM to recalculate it
     */
    public synchronized void triggerGridRecalculation() {
        if (ftm == null) {
            return; // No operation
        }
        if (this.isCleanlyIdle(false)) {
            // First, take the size of the bounds
            Bounds b = gridRenderer.getParent().getLayoutBounds();
            int x = (int) b.getWidth();
            int y = (int) b.getHeight();
            // Now, trigger the reanalysis
            needsToUpdateGrid = false;
            this.ftm.generateNewGrid(x, y, new Runnable() {
                @Override
                public void run() {
                    gridRenderer.generateAndRenderGrid(x, y, ftm.getLatestGrid(), typeColorTranslator);
                }
            });
        } else {
            this.needsToUpdateGrid = true; // Update later
        }
    }

    /**
     * Updates the type table by enumerating through all types.
     * <br>
     * This should be called when there have been any alterations to the type
     * data (e.g {@link CollatingFileTypeFinder} processing); this method is the
     * one which actually builds the type display shown via the user interface.
     */
    private void updateTypeTable() {

        final Map<String, FSNodeTypeStatModel> dataMap = new HashMap<>();

        this.ftm.simpleFilePostProcessing(new Consumer<FSNode>() {

            private void findAndIncrement(String type) {
                if (!dataMap.containsKey(type)) {
                    FSNodeTypeStatModel model = new FSNodeTypeStatModel(type);
                    model.incrementCount();
                    dataMap.put(type, model);
                } else {
                    dataMap.get(type).incrementCount();
                }
            }

            @Override
            public void accept(FSNode t) {
                Object o = t.getReadOnlyExtendedPropertyMap().get(FTMConstants.FSN_ANALYSIS_RESULT_OBJECT_KEY);
                if (o == null || !(o instanceof AnalysisResultObject)) {
                    findAndIncrement("Unidentifiable file (No result data)");
                } else {
                    AnalysisResultObject aro = (AnalysisResultObject) o;
                    String resultType = aro.getTypeIdentifier();

                    findAndIncrement(resultType == null ? "Unidentifiable file (No type provided)" : resultType);
                }
            }
        }, "Collecting statistics on file types...", new Runnable() {
            @Override
            public void run() {
                typeTable.getSelectionModel().clearSelection();
                typeTable.setItems(FXCollections.observableArrayList(dataMap.values()));
            }
        });

    }

    /**
     * This method is called when the user clicks on the grid, and the value
     * selected changes
     *
     * @param file The new file, or NULL if none
     */
    private void changedGridSelection(FSNode file) {
        // At this moment, we do not need to do anything else than just update the selection
        overridingSelectionUpdate(file);
    }

    /**
     * Initializes the grid renderer control
     */
    private void prepareGridRenderer() {
        this.gridRenderer = new GridRendererControl();
        this.gridRenderer.getClickedFileProperty().addListener((observable, oldValue, newValue) -> {
            if (ftm != null && isPostprocessingReadyProperty.get()) {
                changedGridSelection(newValue); // Must be post-processing ready
            }
        });
    }

    /**
     * Prepares the control where all statistical data will appear when a file
     * is selected
     */
    private void prepareStatisticsView() {
        statisticsDisplayer = new TreeTableView<>();
        statisticsDisplayer.setEditable(false);

        TreeTableColumn<Pair<String, String>, String> nameColumn = new TreeTableColumn<>("Name");
        nameColumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<Pair<String, String>, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Pair<String, String>, String> param) {
                return new SimpleStringProperty(param.getValue().getValue().getKey());
            }
        });

        TreeTableColumn<Pair<String, String>, String> valueColumn = new TreeTableColumn<>("Value");
        valueColumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<Pair<String, String>, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Pair<String, String>, String> param) {
                return new SimpleStringProperty(param.getValue().getValue().getValue());
            }
        });

        statisticsDisplayer.getColumns().setAll(nameColumn, valueColumn);
    }

    /**
     * Prepares a type view table; it is populated using an another method
     *
     * @see updateTypeTable
     */
    private void prepareTypeView() {
        typeTable = new TableView<>();
        typeTable.setEditable(false);

        TableColumn typeNameColumn = new TableColumn("Type name");
        TableColumn countColumn = new TableColumn("Count");

        typeNameColumn.setCellValueFactory(new PropertyValueFactory<FSNodeTypeStatModel, String>("typeName"));

        typeNameColumn.setCellFactory(item -> {
            return new TableCell<FSNodeTypeStatModel, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        this.setText(null);
                        this.setTextFill(Color.BLACK);
                        this.setStyle("");
                    } else {
                        this.setText(item);
                        Color c = typeColorTranslator.apply(item);
                        this.setTextFill(c);
                    }
                }
            };
        });

        countColumn.setCellValueFactory(new PropertyValueFactory<FSNodeTypeStatModel, Integer>("count"));

        // Add a selection change function which highlights the files with the selected type on the grid
        typeTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                return; // Null, needs no changes
            }
            // Otherwise, change the predicate to highlight all data with the same type
            gridRenderer.updateSelectorFunction(new Predicate<FSNode>() {
                @Override
                public boolean test(FSNode t) {
                    String typeIdentifier = newValue.getTypeName();
                    Object o = t.getReadOnlyExtendedPropertyMap().get(FTMConstants.FSN_ANALYSIS_RESULT_OBJECT_KEY);

                    if (o == null || !(o instanceof AnalysisResultObject)) {
                        return false; // Not valid, do not display
                    }
                    AnalysisResultObject aro = (AnalysisResultObject) o;
                    if (aro.getResultType() == FileTypeRecognizer.AnalysisResultType.ANALYSIS_ERROR || aro.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_REJECTED) {
                        return false; // Inconclusive identification
                    }
                    return typeIdentifier.equals(aro.getTypeIdentifier()); // If everything else permits, highlight if the type matches
                }
            });
        });

        typeTable.getColumns().addAll(typeNameColumn, countColumn);
        typeTable.setItems(null);
    }

    /**
     * Changes the file tree displaying component to show some other file than
     * specifically selected by user; this special method is required to work
     * around an inconsistency in JavaFX
     *
     * @param file The file selected
     */
    private void overridingSelectionUpdate(FSNode file) {
        if (file == null) {
            return; // Not applicable if nothing is selected
        }
        if (fileTreeDisplayer.getRoot() == null || !(fileTreeDisplayer.getRoot() instanceof FSNTreeItem)) {
            return; // Not a properly formed tree
        }
        TreeItem<FSNode> searchedNode = ((FSNTreeItem) fileTreeDisplayer.getRoot()).revealSubnode(file); // Locate the correct child

        // And select it
        if (searchedNode != null) {
            fileTreeDisplayer.getSelectionModel().clearSelection();
            // Set the override node right before the update
            gridOverrideNode = file;
            fileTreeDisplayer.getSelectionModel().select(searchedNode);

            int currentIndex = fileTreeDisplayer.getSelectionModel().getSelectedIndex();

            if (currentIndex < 0) {
                return; // Not OK
            }
            fileTreeDisplayer.scrollTo(currentIndex);
        }
    }

    /**
     * Updates the stat table, and highlights the grid where applicable
     *
     * @param resetToZero if TRUE, simply nulls out the view, otherwise tries to
     * take the latest item available
     */
    private void updateStatViewAndSingleSelection(boolean resetToZero) {
        FSNode actualItem = gridOverrideNode == null ? (fileTreeDisplayer.getSelectionModel().getSelectedItem() == null ? null : fileTreeDisplayer.getSelectionModel().getSelectedItem().getValue()) : gridOverrideNode;
        gridOverrideNode = null;
        if (ftm == null || resetToZero || actualItem == null) {
            statisticsDisplayer.setRoot(null);
            gridRenderer.updateSelectorFunction(null);
        } else {
            statisticsDisplayer.setRoot(FSNodeStatPairGenerator.nodeToTreeItem(actualItem));
            gridRenderer.updateSelectorFunction(new Predicate<FSNode>() {
                @Override
                public boolean test(FSNode t) {
                    FSNode targetNode = actualItem;
                    if (targetNode.isDirectory()) {
                        Set<FSNode> containedFiles = ftm.getLatestGrid().getIndirectReferences().get(targetNode);
                        if (containedFiles == null) {
                            return false;
                        }
                        return containedFiles.contains(t);
                    } else {
                        return t.equals(targetNode);
                    }
                }
            });
        }
    }

    /**
     * Prepares the <code>TreeTableView</code> element, which is intended to
     * contain the found <code>FSNode</code> items
     */
    private void prepareTreeView() {
        fileTreeDisplayer = new TreeTableView<>();
        fileTreeDisplayer.setRoot(null);

        fileTreeDisplayer.getSelectionModel().setCellSelectionEnabled(false);
        fileTreeDisplayer.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        fileTreeDisplayer.getSelectionModel().getSelectedItems().addListener((Observable observable) -> {
            // Upon change, ask to update
            updateStatViewAndSingleSelection(false);

        });

        //Generate columns
        TreeTableColumn<FSNode, String> nameColumn = new TreeTableColumn<>("Name");

        nameColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<FSNode, String> param) -> {
            FSNode node = param.getValue().getValue(); //Get the value of FSNode
            ObjectBinding binding = Bindings.valueAt(node.getReadOnlyExtendedPropertyMap().getValue(), FTMConstants.FSN_ALTERNATIVE_NAME_KEY);

            // Set up an alternative name binding; if this node has an alternative name set, display it; otherwise display the basic name
            AlternativeValueObjectStringBinding avosb = new AlternativeValueObjectStringBinding(binding, new SimpleStringProperty(node.getUnderlyingFile().getName()));

            return avosb;
        });

        //Size column
        TreeTableColumn<FSNode, String> sizeColumn = new TreeTableColumn<>("Total size");

        sizeColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<FSNode, String> param) -> {
            FSNode node = param.getValue().getValue();
            return SizeToStringConverter.bigIntegerToSize(node.getTotalSizeProperty());
        });

        sizeColumn.setComparator(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1 == null || o2 == null) {
                    return compare(o1 == null ? "" : o1, o2 == null ? "" : o2); // Implicitly treat a null as an empty string
                }
                return SizeToStringConverter.stringToBigInteger(o1).compareTo(SizeToStringConverter.stringToBigInteger(o2));
            }
        });

        //Item column
        TreeTableColumn<FSNode, BigInteger> countColumn = new TreeTableColumn<>("Subitems");

        countColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<FSNode, BigInteger> param) -> {
            FSNode node = param.getValue().getValue();
            return node.getSubnodeCount();
        });

        TreeTableColumn<FSNode, String> typeColumn = new TreeTableColumn<>("Type");

        typeColumn.setCellValueFactory((param) -> {
            FSNode node = param.getValue().getValue();
            // First, bind to value for the latest result object
            ObjectBinding keyBinding = Bindings.valueAt(node.getReadOnlyExtendedPropertyMap().getValue(), FTMConstants.FSN_ANALYSIS_RESULT_OBJECT_KEY);

            return Bindings.createObjectBinding(() -> {
                Object value = keyBinding.get();
                if (value == null) {
                    return ""; //No defined value
                }
                if (value instanceof AnalysisResultObject) {
                    AnalysisResultObject aro = (AnalysisResultObject) value;
                    if (aro.getResultType() == FileTypeRecognizer.AnalysisResultType.ANALYSIS_ERROR) {
                        return "[Analysis error]";
                    }
                    if (aro.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_REJECTED) {
                        return "[Rejected]";
                    }

                    // We have a valid value
                    return aro.getTypeIdentifier();
                } else {
                    return "[Invalid]"; // Return a placeholder value to indicate a problem; this allows the GUI to display it without causing a crash or forcing a stop
                }
            }, keyBinding);
        });

        TreeTableColumn<FSNode, String> searchMatchColumn = new TreeTableColumn<>("Query match");

        searchMatchColumn.setCellValueFactory((param) -> {
            FSNode node = param.getValue().getValue();
            // First, bind to value for the latest result object
            ObjectBinding keyBinding = Bindings.valueAt(node.getReadOnlyExtendedPropertyMap().getValue(), FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY);

            return Bindings.createObjectBinding(() -> {
                Object value = keyBinding.get();

                if (value == null) {
                    return "";
                }
                return value.toString();
            }, keyBinding);
        });

        fileTreeDisplayer.getColumns().add(nameColumn);
        fileTreeDisplayer.getColumns().add(sizeColumn);
        fileTreeDisplayer.getColumns().add(countColumn);
        fileTreeDisplayer.getColumns().add(typeColumn);
        fileTreeDisplayer.getColumns().add(searchMatchColumn);
        fileTreeDisplayer.setShowRoot(true);

    }

    /**
     * Requests an interruption of the current <code>FileTreeManager</code>
     * instance
     *
     * @param shouldWait TRUE if we should lock, FALSE if not. Be
     * <strong>EXTREMELY</strong>
     * careful of setting this to TRUE when interacting directly from a GUI
     * thread! It may cause deadlocks: we are waiting for a thread to stop (from
     * the GUI thread), and some other thread is waiting for us!
     * @throws InterruptedException If we are interrupted
     */
    public void requestInterruption(boolean shouldWait) throws InterruptedException {
        if (isCleanlyIdle(true)) {
            return; //Should not cause changes
        }
        stateString.set("Stopping...");
        if (ftm != null) {
            ftm.requestInterrupt(shouldWait);
        }

    }

    /**
     * Initiates a new file enumeration, if possible
     *
     * @param f File to initiate
     */
    public void initiateFileEnumeration(File f) throws IOException {
        if (!isCleanlyIdle(true)) {
            showError("Not ready for enumeration", "You either have an ongoing enumeration, or the program has experienced an error. If you wish to discard the current state, please select 'Reset' from the enumeration menu.");
            return;
        }

        prepareFileTreeManager(f.getAbsolutePath());
        startEnumeration();
    }

    /**
     * Starts a deep analysis if allowed from this point
     */
    public synchronized void startDeepAnalysis() {
        if (!isPostprocessingReadyProperty.get()) {
            throw new RuntimeException("Not ready for postprocessing yet!");
        }
        needsToUpdateTypes = true; // We will need to update types after this is done
        ftm.executeFurtherAnalysis(null); // We do not have direct UI dependencies, so do not provide a postprocessing function
    }

    /**
     * Prepares a new instance of a <code>FileTreeManager</code>; this will be
     * required at the first start, and in case of some serious errors
     *
     * @param path The root path for the <code>FileTreeManager</code>
     */
    private void prepareFileTreeManager(String path) throws FileNotFoundException, IOException {
        if (ftm != null) {
            try {
                ftm.requestInterrupt(true); //Request an interruption, and wait until everything has stopped
            } catch (InterruptedException ie) {
                // Transform the interruption as an ordinary runtime exception
                throw new RuntimeException("Interrupted thread, failed to reset", ie);
            }
        }

        fileTreeDisplayer.setRoot(null); // Can be changed safely, design rule says that this is only to be called from JavaFX
        typeTable.setItems(null); // Same
        gridRenderer.invalidateCurrentGrid();
        this.needsToUpdateTypes = true;
        ftm = new FileTreeManager(new File(path), new JavaFXAppThreadCallableWrapper());

        FileTreeManager extantFtm = ftm;

        ftm.getStateProperty().addListener(new ChangeListener<FileTreeManager.State>() {
            @Override
            public void changed(ObservableValue<? extends FileTreeManager.State> observable, FileTreeManager.State oldValue, FileTreeManager.State newValue) {
                if (ftm != extantFtm) {
                    //Our current FTM is different from the one this state wrapper has been bound to. Remove ourselves.
                    extantFtm.getStateProperty().removeListener(this);
                    return;
                }
                // As FTM guarantees that its state will not change outside of a wrapper (which is in our case the JavaFX one), we can safely call this directly
                stateChanged(oldValue, newValue);
            }
        });

        // Add a state string property
        ftm.getStateStringProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (ftm != extantFtm) {
                    //Our current FTM is different from the one this state wrapper has been bound to. Remove ourselves.
                    extantFtm.getStateStringProperty().removeListener(this);
                    return;
                }
                // As FTM guarantees that its state will not change outside of a wrapper (which is in our case the JavaFX one), we can safely call this directly
                stateStringChanged(oldValue, newValue);

            }
        });
    }

    /**
     * Actually change the displayed state string
     *
     * @param old_value Old string
     * @param new_value New string
     */
    private void stateStringChanged(String old_value, String new_value) {
        String resStr = new_value == null ? "" : new_value;
        stateString.set(resStr);
    }

    /**
     * Changes the apparent state visible from <code>FTMGUIModel</code>
     *
     * @param oldState Old state
     * @param newState New state
     */
    private synchronized void stateChanged(FileTreeManager.State oldState, FileTreeManager.State newState) {
        switch (newState) {
            case ENUMERATING:
                stateString.setValue("Enumeration in progress..");
                break;
            case STOPPED_MAY_RETRY: // These two are handled approximately similarly 
            case STOPPED_MUST_REINIT:
                if (ftm.getStateStringProperty().get() == null) {
                    stateString.set(newState == FileTreeManager.State.STOPPED_MAY_RETRY ? "Interrupted." : "Interrupted, please restart the file enumeration.");
                }
                // Warn the user if there is a special message
                if (ftm.getStateStringProperty().get() != null) {
                    showError("Processing stopped", "The current process has been stopped due to the following reason: " + ftm.getStateStringProperty().get());
                }
                break;
            case IDLE_SUCCESS:
                // On idle, trigger any events we may need to retrigger
                if (needsToUpdateTypes) { // We need to update both types and stats
                    updateTypes();
                } else if (needsToUpdateGrid) { // Types have updated, but the grid has not yet been recalculated
                    triggerGridRecalculation();
                } else { // Many operations alter the file-specific data. It is entirely reasonable to update the stat view, and request the grid rerender
                    gridRenderer.redrawGrid();
                    updateStatViewAndSingleSelection(false);
                }
                if (oldState == FileTreeManager.State.ENUMERATING) {
                    stateString.set("Successfully enumerated, ready for deeper analysis.");
                } else {
                    stateString.set("Idle.");
                }
                break;
        }

        isProcessingProperty.invalidate(); // This may no longer be valid, reset
        isPostprocessingReadyProperty.invalidate();

        // Most likely, there has been a lot of GCable crud generated at this point; hint to the JVM that it may be beneficial to GC soon
        System.gc();
    }

    /**
     * Requests a start of the enumeration
     */
    private void startEnumeration() {
        if (!canEnumerateNow()) {
            throw new RuntimeException("You must initialize a FTM first!");
        }

        ftm.startScanAndAnalyze();
        fileTreeDisplayer.setRoot(new FSNTreeItem(ftm.getRootNode()));

    }

    /**
     * Get the property describing what is happening currently; may be bound to,
     * say, a status bar
     *
     * @return A string property that may change upon occurences of events
     */
    public ReadOnlyStringProperty getStateString() {
        return stateString.getReadOnlyProperty();
    }

    /**
     * Checks if the <code>FileTreeManager</code> is ready to enumerate now; it
     * must exists, and must be in <code>NOT_STARTED</code> state
     *
     * @return True if yes, FALSE if no
     */
    private boolean canEnumerateNow() {
        return ftm != null && ftm.getStateProperty().get() == FileTreeManager.State.NOT_STARTED;
    }

    /**
     * Returns TRUE if <code>FileTreeManager</code> is cleanly idle (or not
     * initialized); stopped, and not in a conflicting state. By gating the
     * processing binding with this, a guarantee will be provided that is
     * restarting is allowed, threads have been stopped without use of force
     *
     * @param reinitAsAcceptableState If TRUE, state requiring a
     * reinitialization of FTM is considered to be acceptable
     * @return TRUE if cleanly idle, FALSE if not
     */
    public boolean isCleanlyIdle(boolean reinitAsAcceptableState) {
        return ftm == null || (ftm.isIdle(!reinitAsAcceptableState));
    }

    /**
     * Returns a file tree displayer JavaFX element; this should be added to the
     * GUI in a suitable spot, and it will be automatically updated
     *
     * @return A TreeTableView containing the prerequisite information
     */
    public TreeTableView<FSNode> getFileTreeDisplayer() {
        return fileTreeDisplayer;
    }

    /**
     * Returns a table view for found types.
     *
     * @return A <code>TableView</code> showing the types found for the analysis
     */
    public TableView getTypeDisplayer() {
        return this.typeTable;
    }

    /**
     * Returns a grid renderer control, which shows the files as a grid
     *
     * @return Grid renderer
     */
    public GridRendererControl getGridRenderer() {
        return this.gridRenderer;
    }

    /**
     * Returns a binding to the processing status; if this is true, the FTM is
     * busy or in a state of error.
     *
     * @return A binding which imparts a simple status
     */
    public BooleanBinding getProcessingProperty() {
        return this.isProcessingProperty;
    }

    /**
     * Returns a binding to the processing status - if this is true, the FTM is
     * ready to commit postprocessing operations
     *
     * @return A binding that imparts a simple status
     */
    public BooleanBinding getPostprocessingReadyProperty() {
        return this.isPostprocessingReadyProperty;
    }

    /**
     * Returns a statistics display for the currently selected file
     *
     * @return
     */
    public TreeTableView getStatisticsView() {
        return this.statisticsDisplayer;
    }

    /**
     * Triggers a search query
     *
     * @param queryBox Parameters in the prescribed format;
     * <pre>[Name]: \<value\></pre>
     */
    public void triggerSearchQuery(String queryBox) {
        if (!isPostprocessingReadyProperty.get()) {
            throw new RuntimeException("Not ready for postprocessing yet!");
        }

        try { // Try to execute a query
            ftm.executeSearchQueries(queryBox);
        } catch (IllegalArgumentException iae) {
            showError("Erroneous query", String.format("There was an error in your query: %s", iae.getMessage()));
        }
    }

    /**
     * Displays an alert window
     *
     * @param header Header text
     * @param description Content text
     */
    private void showError(String header, String description) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.headerTextProperty().set(header);
        alert.contentTextProperty().set(description);
        alert.showAndWait(); //As there is only one choice, do not care
    }
}
