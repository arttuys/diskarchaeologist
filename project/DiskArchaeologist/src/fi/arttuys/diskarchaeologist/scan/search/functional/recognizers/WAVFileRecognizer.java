/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.search.functional.recognizers;

import fi.arttuys.diskarchaeologist.scan.search.FileTypeRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.functional.AbstractFluidRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.functional.AnalysisProperty;
import fi.arttuys.diskarchaeologist.scan.search.functional.FluidFileRecognizer;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * A simple WAV file recognizer, to find out some specific properties about the file
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
@FluidFileRecognizer(priority = FileTypeRecognizer.Priority.ADVANCED) // Only one type, but knows a lot about it 
public class WAVFileRecognizer extends AbstractFluidRecognizer{

    /**
     * The discovered format
     */
    private AudioFileFormat foundFormat = null;
    
    /**
     * Returns the sample rate of this file
     * @return Sample rate
     */
    @AnalysisProperty(category = "File format", inDepthOnly = false, name = "Sample rate")
    public String sampleRate() {
        return String.format(" (%.3fHZ)", foundFormat.getFormat().getSampleRate());
    }
    
    /**
     * Returns the frame length of this file
     * @return Frame length
     */
    @AnalysisProperty(category = "Contents", inDepthOnly = false, name = "Frame length")
    public String frameLength() {
        return Integer.toString(foundFormat.getFrameLength()) + " franes";
    }
    
    @Override
    protected FileTypeRecognizer.AnalysisResultType analyze(FileTypeRecognizer.AnalysisLevel level) throws Exception {
        String fileName = this.getBaseNode().getUnderlyingFile().getName();
        
        if (!fileName.toLowerCase().endsWith(".wav")) return FileTypeRecognizer.AnalysisResultType.FILE_REJECTED; // Definitely not a WAV
        if (level == FileTypeRecognizer.AnalysisLevel.INDEPTH) return FileTypeRecognizer.AnalysisResultType.FILE_REJECTED; // We do not have an advanced distinction
        if (level == FileTypeRecognizer.AnalysisLevel.POTENTIAL) return FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_BASIC;
        
        //Otherwise, try to analyze further
        try {
        foundFormat = AudioSystem.getAudioFileFormat(this.getBaseNode().getUnderlyingFile());

        // It is now guaranteed that we have a valid format
        } catch (UnsupportedAudioFileException uafe) {
            // Not a valid audio file
            return FileTypeRecognizer.AnalysisResultType.FILE_REJECTED;
        }
        return FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_BASIC;
    }

    private String getFormatString() {
        if (foundFormat == null) return "";
        
        return String.format(" (%fHZ)", foundFormat.getFormat().getSampleRate());
    }
    
    @Override
    protected String getTypeIdentifier() {
        return "WAV audio file" + getFormatString();
    }
    
}
