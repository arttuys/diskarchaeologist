/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.search.functional;

import fi.arttuys.diskarchaeologist.scan.node.FSNode;
import fi.arttuys.diskarchaeologist.scan.search.AnalysisResultObject;
import fi.arttuys.diskarchaeologist.scan.search.FileTypeRecognizer;
import java.io.FileNotFoundException;
import java.lang.annotation.Annotation;
import java.util.Set;

/**
 *
 * An adapter class that bridges fluid recognizers with the API presented by <code>FileTypeRecognizer</code>.
 * 
 * Considering the reusable nature of fluid recognizers, this class MAY cache recognizers for the same file object, although this must not be taken for granted!
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 * @see AbstractFluidRecognizer
 */
public class FluidRecognizerAdapter implements FileTypeRecognizer {

    /**
     * The class of the fluid recognizer we want to instance
     */
    private Class baseClass;
    
    /**
     * The priority of this recognizer, as taken in from the annotation
     */
    private final FileTypeRecognizer.Priority recognizerPriority;
    
    /**
     * Initialize a fluid recognizer adapter. Fluid recognizers look very different when compared to standard recognizers, so this adapter was built to facilitate the usage of the standard API
     * @param baseClass Class of the fluid recognizer that should be used for this adpater
     */
    public FluidRecognizerAdapter(Class baseClass) {
        if (baseClass == null) throw new IllegalArgumentException("Base class must not be null!");
        if (!AbstractFluidRecognizer.class.isAssignableFrom(baseClass)) throw new IllegalArgumentException("The given base class must be a subclass of AbstractFluidRecognizer!");
        
        //Fetch metadata for our class
        Annotation metadata = baseClass.getDeclaredAnnotation(FluidFileRecognizer.class);
        
        if (metadata == null) throw new IllegalArgumentException("The specified class does not contain prerequisite metadata!");
        FluidFileRecognizer fluidMeta = (FluidFileRecognizer)metadata;
        
        //Save priority
        this.recognizerPriority = fluidMeta.priority();
        //Save the base class
        this.baseClass = baseClass;
    }
    
    /**
     * Instantiates a new instance of the given recognizer. It is assumed that we have a parameterless constructor available
     * @return A new instance
     */
    private AbstractFluidRecognizer instantiateNewRecognizer() {
        try {
            return (AbstractFluidRecognizer)baseClass.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException("Unable to instantiate", ex);
        } 
    }
    
    @Override
    /**
     * Return the priority of the recognizer; in this case, the priority as expressed by the fluid recognizer
     */
    public Priority getPriority() {
        return recognizerPriority;
    }

    /**
     * Execute analysis; in practice, instantiate a new fluid recognizer instance, and ask it to analyze a given file 
     * 
     * @param level Level
     * @param file File to be analyzed
     * @return The result
     */
    @Override
    public AnalysisResultObject analyze(AnalysisLevel level, FSNode file) {
        try {
            if (level == null || file == null) throw new NullPointerException("Level or file may not be null!");
            
            //Instantiate a new recognizer, prepare it and call it
            AbstractFluidRecognizer afr = this.instantiateNewRecognizer();
            afr.prepare(file);
            AnalysisResultObject aro = afr.commenceAnalyze(level);
            
            if (aro == null) throw new NullPointerException("Analyzer returned null!");
            return aro;
        } catch (FileNotFoundException ex) {
            //Analysis error, pure and simple. FnF is a common occurence
        }
        
        //Return an error
        return new AnalysisResultObject(AnalysisResultType.ANALYSIS_ERROR, null, null);
    }

    @Override
    public Set<AnalysisResultObject.AnalysisEntry> getPossibleSearchKeys() {
        return this.instantiateNewRecognizer().getPossibleKeys();
    }
    
}
