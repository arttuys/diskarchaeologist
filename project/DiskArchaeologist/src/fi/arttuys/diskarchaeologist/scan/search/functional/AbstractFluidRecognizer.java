/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.search.functional;

import fi.arttuys.diskarchaeologist.scan.node.FSNode;
import fi.arttuys.diskarchaeologist.scan.search.AnalysisResultObject;
import fi.arttuys.diskarchaeologist.scan.search.FileTypeRecognizer;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

/**
 *
 * A core adapter class for fluid recognizers.
 * <p>
 * A fluid recognizer is designed to be a DRY, simple way to describe file type recognizers.
 * All fluid recognizers are classes extended from this specific class, which defines certain helping methods and functionality for the class to use.
 * </p>
 * <p>
 * Each declaration for a method returning a result describing some kind of a property must be marked with a {@code AnalysisProperty} annotation.
 * That annotation will contain information on how it shall be displayed to the user.
 * </p>
 * <p>
 * All fluid recognizers should have one accessible parameterless constructor; it is not required for them to call the super() constructor, as internal preparations will be taken care of elsewhere.
 * </p>
 * <p>
 * The lifecycle of a fluid recognizer is as follows:
 * </p>
 * <ol>
 * <li>A new instance of a recognizer is instantiated. It is vital that the constructor should be accessible from the package <code>fi.arttuys.scan.search.functional</code></li>
   <li>The instance is prepared with a base <code>FSNode</code>, locking in what specific file we are handling</li>
   <li>The instance is called with <code>analyze</code> one (or in some cases, multiple) times. For each call of analyze, a new FileStream has been opened; however, the file will never change in the lifetime of the object.
   One important point; an analyzer must be able to gracefully handle cases where we immediately request a higher level analysis. However, the analyser must still behave gracefully and return an error if too high of a level is requested</li>
   <li>After calling <code>analyze</code>, functions returning different properties may be called in any order; including the function returning a type identifier. The functions called will depend on result, with rejection or error not triggering any further calls</li>
 * </ol>
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public abstract class AbstractFluidRecognizer {
    
    /**
     * Default initializer for AbstractFluidRecognizers; typically, these should not be initializable by anyone other than the adapter
     */
    protected AbstractFluidRecognizer() {
        //This is simply to ensure that subclasses have a default initializer that doesn't permit anyone outside to instantiate a class.
    }
   
    /**
     * The FSNode this recognizer instance is based on
     */
    private FSNode baseNode;
   
    /**
     * Has this class been prepared properly?
     */
    private boolean hasBeenPrepared = false;
    
    /**
     * Mapped buffer for the file data
     */
    private FileInputStream fileStream = null;
    /**
     * Prepares a recognizer so that it is ready to be used for action. This unusual method of initialization is utilized due to the way new fluid recognizers are to be spawned; it is undesirable to let them have direct access to properties or be able to subvert the construction process in any way. 
     * @param node Node that this instance should be bound to.
     * @throws java.io.FileNotFoundException If the relevant file is not found, this is thrown 
     */
    protected void prepare(FSNode node) throws FileNotFoundException {
        if (hasBeenPrepared) throw new IllegalArgumentException("An recognizer cannot be prepared twice!");
        this.baseNode = node;
        hasBeenPrepared = true;
    }
    
    /**
     * Returns a set of all possible keys that can be encountered in the analyzations as completed by this program.
     * @return A set of keys, or NULL if unable to return one
     */
    public Set<AnalysisResultObject.AnalysisEntry> getPossibleKeys() {
        try {
            return this.fetchResultsFromMethods(FileTypeRecognizer.AnalysisLevel.INDEPTH, true).keySet();
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return null;
        }
    }
    
    /**
     * Fluid recognizers define a method for each field. This method uses reflection to find the values of them; and constructs a map of the resulting values
     * @param level At which level we want the results. May not be POTENTIAL
     * @param justKeys If TRUE, just return valid entries, with no values!
     * @return An unmodifiable map of resulting keys
     */
    private Map<AnalysisResultObject.AnalysisEntry, Object> fetchResultsFromMethods(FileTypeRecognizer.AnalysisLevel level, boolean justKeys) throws IllegalAccessException, InvocationTargetException {
        if (level == null || level == FileTypeRecognizer.AnalysisLevel.POTENTIAL) throw new IllegalArgumentException("Level must be BASIC or IN-DEPTH");
        
        Class runtimeClass = this.getClass();
        Map<AnalysisResultObject.AnalysisEntry, Object> resultMap = new HashMap<>();
        
        //Get all declared, specific methods for this class
        for (Method m : runtimeClass.getDeclaredMethods()) {
            AnalysisProperty analysisProp = m.getDeclaredAnnotation(AnalysisProperty.class);
   
            if (analysisProp == null) continue; //Not the proper method.
            if (analysisProp.inDepthOnly() && level != FileTypeRecognizer.AnalysisLevel.INDEPTH) continue; //Requires indepth, but we are not running at that level.s
            
            //Bypass any accessor modifiers that may be blocking us
            m.setAccessible(true);
            
            //Create a new entry
            AnalysisResultObject.AnalysisEntry entry = new AnalysisResultObject.AnalysisEntry(analysisProp.category(), analysisProp.name());
            //And add it to the map, or the key only
            resultMap.put(entry, justKeys ? null : m.invoke(this));
        }
        
        return resultMap;
    }
    
    /**
     * Commences an analysis, and proceeds to execute analysis methods as desired. This is the internal call and not for end-users
     * @param level Level of analysis required
     * @return The result of an analysis.
     */
    protected AnalysisResultObject commenceAnalyze(FileTypeRecognizer.AnalysisLevel level) {
        try {
            if (!isPrepared()) throw new IllegalArgumentException("Must be prepared before calling!");
            //Prime the file stream IF it is not a potential
            fileStream = null;
            if (level != FileTypeRecognizer.AnalysisLevel.POTENTIAL) fileStream = new FileInputStream(this.baseNode.getUnderlyingFile());
            //Call the internal analysis procedure for the file
            FileTypeRecognizer.AnalysisResultType result = analyze(level);
            
            if (result == null) throw new NullPointerException("Null result returned!");
            
            // We do not fetch results IF: it is an error, rejection, potential result or we seek a potential result
            if (result == FileTypeRecognizer.AnalysisResultType.ANALYSIS_ERROR || result == FileTypeRecognizer.AnalysisResultType.FILE_REJECTED || result == FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_POTENTIAL || (level == FileTypeRecognizer.AnalysisLevel.POTENTIAL)) {
              return new AnalysisResultObject(result, null, (result != FileTypeRecognizer.AnalysisResultType.ANALYSIS_ERROR && result != FileTypeRecognizer.AnalysisResultType.FILE_REJECTED) ? getTypeIdentifier() : null);
            } else {
              return new AnalysisResultObject(result, fetchResultsFromMethods(level, false), getTypeIdentifier());
            }
        } catch (Exception e) {
            //Exception, catchall error
            return new AnalysisResultObject(FileTypeRecognizer.AnalysisResultType.ANALYSIS_ERROR, null, null);
        } catch (AnalysisRejectionError are) {
            //Analysis rejected due to an assertion, reject normally
            return new AnalysisResultObject(FileTypeRecognizer.AnalysisResultType.FILE_REJECTED, null, null);
        } finally {
            try {
                if (fileStream != null) fileStream.close();
            } catch (IOException ex) {
               //This we can not help. Just quietly ignore the exception
            }
            fileStream = null;
        }
    }
    
    /**
     * Method that asks the subclass to analyze the file as specified, and return a result. This method may be called multiple times; after each time, respective methods with results should be callable if that is possible
     * @param level Level of analysis required
     * @return Result of the analysis
     * @throws java.lang.Exception Any exception that may be raised by the recognizer
     */
    protected abstract FileTypeRecognizer.AnalysisResultType analyze(FileTypeRecognizer.AnalysisLevel level) throws Exception;
    
    /**
     * Returns the type identifier for this file; basically a passthrough mechanism for the FTR API. Not required to be callable before a successful analysis call
     * @return A single-line identifier for this specific file type
     */
    protected abstract String getTypeIdentifier();
    
    /**
     * Returns whenever the base of this class has been prepared properly
     * @return TRUE if yes, FALSE if no
     */
    public final boolean isPrepared() {
        return hasBeenPrepared;
    }
    
    /**
     * Asserts that the size of this file must match this specific condition
     * @param condition A predicate describing the size required
     */
    protected void requireSize(Predicate<BigInteger> condition) {
        if (!condition.test(this.baseNode.getLocalSizeProperty().get())) throw new AnalysisRejectionError("Size not according to the spec");
    }
    
    /**
     * Asserts that the expression set in condition is true
     * @param condition An expression returning a boolean
     */
    protected void assertTrue(boolean condition) {
        if (!condition) throw new AnalysisRejectionError("Assertion of truth failed");
    }
    
    /**
     * Asserts that the expression set in condition is false
     * @param condition An expression returning a boolean
     */
    protected void assertFalse(boolean condition) {
        assertTrue(!condition);
    }
    
    /**
     * Returns a file input stream to the file we are analyzing, if applicable
     * @return A file channel related to the file we are reading, or NULL if we are at a potential level and need not to access file data
     */
    protected InputStream getInputStream() {
        return fileStream;
    }
    
    /**
     * Returns the base node for this file
     * @return Node
     */
    protected FSNode getBaseNode() {
        return this.baseNode;
    }
}
