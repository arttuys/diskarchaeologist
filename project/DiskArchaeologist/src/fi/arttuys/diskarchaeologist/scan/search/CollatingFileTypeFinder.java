/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.search;

import fi.arttuys.diskarchaeologist.scan.node.FSNode;
import fi.arttuys.diskarchaeologist.scan.search.functional.FluidRecognizerAdapter;
import fi.arttuys.diskarchaeologist.scan.search.functional.recognizers.SimpleExtensionRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.functional.recognizers.TextDocumentRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.functional.recognizers.WAVFileRecognizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

/**
 *
 * A simple wrapper class for multiple <code>FileTypeRecognizer</code>s; this class will automatically identify the correct FileTypeRecognizer out of multiple ones presented. 
 * Each collating type finder is bound to a single file - contrary to <code>FileTypeRecognizer</code>s, no reuse is permitted.
 *
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class CollatingFileTypeFinder {

    
    private class FTRResultPair {
        public FileTypeRecognizer ftr = null;
        public AnalysisResultObject result = null;
        
        /**
         * Are both of these defined?
         * @return TRUE if yes, FALSE if no
         */
        public boolean bothDefined() {
            return result != null && ftr != null;
        }
        
        /**
         * Is this result considered to be accepted?
         * @return TRUE if yes (not an error or rejection), FALSE if no
         */
        public boolean acceptedResult() {
            if (!bothDefined()) throw new RuntimeException("Both must be defined to see if a result is accepted!");
            return result.getResultType() != FileTypeRecognizer.AnalysisResultType.ANALYSIS_ERROR && result.getResultType() != FileTypeRecognizer.AnalysisResultType.FILE_REJECTED;
        }
    }
    
    /**
     * A state enum, describing the current state of the type finder
     */
    public enum CFTFState {
        /**
         * Signifies that no analysis has occured yet
         */
        UNINITIALIZED,
        /**
         * Signifies that potential analysis has been completed, and there is some idea of what the file is
         */
        POTENTIAL_COMPLETE,
        /**
         * Signifies that the advanced analysis is complete, and there is now a high amount of content available
         */
        ADVANCED_COMPLETE
    }
    
    /**
     * The node in question
     */
    private final FSNode scopeFile;
    /**
     * The current state of this finder
     */
    private CFTFState currentState;
    
    /**
     * The latest result pair we've selected, and wish to expose
     */
    private FTRResultPair latestResultPair;
    
    /**
     * The latest results that have been collated by the iterative stepping
     */
    private List<FTRResultPair> latestCollatedResults;
    
    /**
     * Returns an ordered list of recognizers to try; from most important to least important.
     * Currently, this implementation returns a static list hard-coded; on TODO list is a more advanced plugin system
     * @return An ordered list of recognizers
     */
    private List<FileTypeRecognizer> prepareRecognizerSet() {
        ArrayList l = new ArrayList();
        l.add(new FluidRecognizerAdapter(WAVFileRecognizer.class));
        l.add(new FluidRecognizerAdapter(SimpleExtensionRecognizer.class));
        l.add(new FluidRecognizerAdapter(TextDocumentRecognizer.class));
        l.add(new CatchallFileNameRecognizer()); // Add a catchall recognized guaranteed to catch the file
        
        
        l.sort(null);
        
        return Collections.unmodifiableList(l);
    }
    
    /**
     * Creates a new collating finder for a specified file
     * @param f File for this finder
     */
    public CollatingFileTypeFinder(FSNode f) {
        if (f.isDirectory()) throw new IllegalArgumentException("Nodes must be files for analysis!");
        this.scopeFile = f;
        this.currentState = CFTFState.UNINITIALIZED;
        this.latestResultPair = null;
        this.latestCollatedResults = null;
    }
    
    /**
     * Finds the very first results, and updates the state accordingly if found
     */
    private boolean findFirstResult() {
        // Observe that for the recognizer set, the highest priority results are returned first. We also reject
        Stream<FTRResultPair> analysisResultStream = prepareRecognizerSet().stream().map(new Function<FileTypeRecognizer, FTRResultPair>() {
            @Override
            public FTRResultPair apply(FileTypeRecognizer t) {
                FTRResultPair res = new FTRResultPair();
                res.ftr = t;
                res.result = t.analyze(FileTypeRecognizer.AnalysisLevel.POTENTIAL, scopeFile);
                
                return res;
            }
        }).filter((x) -> x.acceptedResult());
        
        // Collate these findings to a list
        List<FTRResultPair> resultsAsList = analysisResultStream.collect(Collectors.toList());
        // Considering we are only looking for a potential result now, we need not concern ourselves with the qualities now. 
        // Let's just pick the first one in the list, as that is one returned by a recognizer of highest standing in specificity
        if (resultsAsList.isEmpty()) return false; //No results, nothing to do
        
        latestResultPair = resultsAsList.get(0);
        latestCollatedResults = resultsAsList;
        
        // Mark potential analysis as complete
        currentState = CFTFState.POTENTIAL_COMPLETE;

        return true;
    }
    
    /**
     * Attempts to find the most advanced result available. It is assumed that specificness wins over result; as in, a highly specific recognizer giving an imprecise result is better than an inspecific recognizer giving a highly detailed result
     * @return TRUE if we could locate one, FALSE if not
     */
    private boolean findAdvancedResult() {
        if (currentState != CFTFState.POTENTIAL_COMPLETE) throw new IllegalStateException("Must have completed basic analysis, but not advanced analysis!");
        
        // Sort first by the specificness of the analyzer, and then by advancedness
        latestCollatedResults.sort((FTRResultPair o1, FTRResultPair o2) -> {
            if (o1.ftr.getPriority() != o2.ftr.getPriority()) return o1.ftr.getPriority().compareTo(o2.ftr.getPriority()) * -1; //Defined from least to most important, reverse this order
            
            // If these match, then sort by advancedness of the last result. These are also defined from least to most, so reverse it as well
            return o1.result.getResultType().compareTo(o2.result.getResultType()) * -1;
        });
        
        for (FTRResultPair res : latestCollatedResults) {
            if (res.result.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_POTENTIAL) continue; //Not interested, only promised potentiality
            // Get a new result
            AnalysisResultObject aro =  res.ftr.analyze(res.result.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_FULL ? FileTypeRecognizer.AnalysisLevel.INDEPTH : FileTypeRecognizer.AnalysisLevel.BASIC, scopeFile);
            
            if (res.result.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_FULL && aro.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_BASIC) throw new IllegalStateException("FTR does not behave properly: returned a lesser level than requested (basic for full)!");
            
            if (aro.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_REJECTED && res.result.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_FULL) {
                // Try again with a lesser level?
                res.ftr.analyze(FileTypeRecognizer.AnalysisLevel.BASIC, scopeFile);
            }
            
            //If it is an error, don't bother trying again
            if (aro.getResultType() == FileTypeRecognizer.AnalysisResultType.ANALYSIS_ERROR) continue; //Errorneous, skip this recognizer
           
            //Check for this, to avoid sneaky bugs. Enforce consistency with spec: if an analysis to some level can not be done, reject the file instead
            if (aro.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_POTENTIAL) throw new IllegalStateException("FTR does not behave properly: returned a lesser level than requested (potential for basic/indepth)!");
            
            //////
            if (aro.getResultType() != FileTypeRecognizer.AnalysisResultType.FILE_REJECTED) {
                //Splendid! We have a valid file!
                FTRResultPair newResult = new FTRResultPair();
                newResult.ftr = res.ftr;
                newResult.result = aro;
                
                latestResultPair = newResult;
                currentState = CFTFState.ADVANCED_COMPLETE;
                return true;
            }
        }
        
        return false; //Something went wrong, could not complete
    }
    
    /**
     * Advances analysis to the next step
     * @return TRUE if this was possible, FALSE if not
     */
    public boolean advanceState() {
       
        switch (currentState) {
            case UNINITIALIZED:
                return findFirstResult();
            case POTENTIAL_COMPLETE:
                return findAdvancedResult();
            
        }
        
        return false; // Should not happen, obviously an error
    }
    
    /**
     * Gets the current state of this collating finder
     * @return An enum describing the state
     */
    public CFTFState getCurrentState() {
        return this.currentState;
    }
    
    /**
     * Returns the latest <code>FileTypeRecognizer</code> chosen as the authoritative one for the file, if available
     * @return FTR
     */
    public FileTypeRecognizer getLatestFTR() {
        return latestResultPair == null ? null : latestResultPair.ftr;
    }
    
    /**
     * Gets the latest result object, if available
     * @return Analysis result object
     */
    public AnalysisResultObject getLatestAnalysisResult() {
        return latestResultPair == null ? null : latestResultPair.result;
    }
}
