/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.search.functional.recognizers;

import fi.arttuys.diskarchaeologist.scan.search.FileTypeRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.functional.AbstractFluidRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.functional.AnalysisProperty;
import fi.arttuys.diskarchaeologist.scan.search.functional.FluidFileRecognizer;
import java.math.BigInteger;

/**
 *
 * An uncomplicated text document recognizer, simply counts the amount of words and characters
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
@FluidFileRecognizer(priority = FileTypeRecognizer.Priority.BASIC) // Very primitive, only TXT files and only some relatively general trivia over them 
public class TextDocumentRecognizer extends AbstractFluidRecognizer{

    private BigInteger characters = BigInteger.ZERO;
    private BigInteger words = BigInteger.ONE;
    
    /**
     * Returns the "words" in a file (sequences delimited by spaces)
     * @return Words
     */
    @AnalysisProperty(category = "Contents", inDepthOnly = false, name = "Words")
    public String words() {
        return words.toString();
    }
    
    /**
     * Returns the characters in a file
     * @return Characters
     */
    @AnalysisProperty(category = "Contents", inDepthOnly = false, name = "Characters")
    public String characters() {
        return characters.toString();
    }
    
    @Override
    protected FileTypeRecognizer.AnalysisResultType analyze(FileTypeRecognizer.AnalysisLevel level) throws Exception {
        String fileName = this.getBaseNode().getUnderlyingFile().getName();
        
        if (!fileName.toLowerCase().endsWith(".txt")) return FileTypeRecognizer.AnalysisResultType.FILE_REJECTED; // Definitely not a text file
        if (level == FileTypeRecognizer.AnalysisLevel.INDEPTH) return FileTypeRecognizer.AnalysisResultType.FILE_REJECTED; // We do not have an advanced distinction
        if (level == FileTypeRecognizer.AnalysisLevel.POTENTIAL) return FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_BASIC; //OK, we can accept at a basic level
        // Try to read
        int chr;
        while ((chr = this.getInputStream().read()) != -1) {
            characters = characters.add(BigInteger.ONE);
            if (Character.isWhitespace(chr)) words = words.add(BigInteger.ONE); // If a whitespace character, count as one
        }
        
        // OK, pass
        return FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_BASIC;
    }

    @Override
    protected String getTypeIdentifier() {
        return "A text document";
    }
    
}
