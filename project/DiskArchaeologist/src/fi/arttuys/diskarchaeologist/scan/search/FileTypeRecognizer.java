/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.search;

import fi.arttuys.diskarchaeologist.scan.node.FSNode;
import fi.arttuys.diskarchaeologist.scan.search.FileTypeRecognizer.Priority;
import java.util.Set;

/**
 *
 * An interface describing a file type recognizer; a file type recognizer takes a File, and identifies it to the best ability possible.
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public interface FileTypeRecognizer extends Comparable {
    
    /**
     * Types defining the priority of a type recognizer. Each file will be ran through several recognizers, and the result returned by the most advanced recognizer will be used
     * If multiple recognizers of the same priority recognize one file, selection shall be undefined.
     */
    public enum Priority {
        /**
         * Catchall, can recognize basically any file in a common fashion.
         */
        CATCHALL,
        /**
         * Basic recognizer, can recognize a wide set of files at some level.
         */
        BASIC,
        /**
         * A slightly more advanced recognizer than a basic one, can identify several types at some depth.
         */
        MULTITYPE,
        /**
         * Advanced recognizer, can identify only a few specific types in-depth. 
         */
        ADVANCED
    }

    /**
     * By default, it is relatively safe to assume FTRs are compared by their priority scores. Highest priority is first. This is a default implementation
     * @param o Other FTR
     * @return -1 if we are more important, 1 if less important
     */
    @Override
    public default int compareTo(Object o) {
        if (o == null || !(o instanceof FileTypeRecognizer)) throw new IllegalArgumentException("By default, FTRs cannot be sorted with other objects");
        FileTypeRecognizer ftr = (FileTypeRecognizer)o;
        return (this.getPriority().compareTo(ftr.getPriority()) * -1);
    }
    
    /**
     * Returns the priority of this recognizer; each recognizer has a set priority, which describes the detailedness of their results. Analyzers with higher priority should be chosen over those with lower priority, if possible
     * @return Priority of this recognizer
     */
    public Priority getPriority();
    
    /**
     * Analysis levels for requests; each file type recognizer should be able to commit multiple levels of analysis; this enum describes the level of analysis required
     */
    public enum AnalysisLevel {
        /**
         * Potential match; lowest standard, means that any operations should be very fast and cursory. File access should be avoided where possible
         */
        POTENTIAL,
        /**
         * Basic analysis; if possible, extract a few quick, basic details of the file at hand. It is not required to do any time-consuming analysis procedures. 
         */
        BASIC,
        /**
         * In-depth; basic analysis plus potentially slow in-depth operations.
         */
        INDEPTH
    }
    
    /**
     * Type of result for the analysis. It may be an error, or it may be an acceptance <strong>of the same or higher level than the requested analysis</strong>
     * Do note that an acceptance at a higher level <strong>only suggests it may be viable</strong>; conclusive results are only given when requested with a specific level you want.
     * 
     * Also, beware that a higher result type DOES NOT necessarily indicate extended properties; while it is strongly recommended to return extended properties when accepting a call with a higher-than-potential expectation level, it is not strictly required!
     */
    public enum AnalysisResultType {
        /**
         * Signifies that analysis did not complete normally, and that no clear judgement can be done. This always signifies an exceptional situation - typical rejections should be FILE_REJECTED
         */
        ANALYSIS_ERROR,
        /**
         * Signifies that the file analyzed is not recognized by this recognizer at this level. If some file is not even potentially recognized, it is not something accepted by this analyzer.
         */
        FILE_REJECTED,
        /**
         * Signifies that this file was accepted, but cannot be provided with any extended information.
         */
        FILE_ACCEPTED_POTENTIAL,
        /**
         * Signifies that this file was accepted, with indication that only basic analysis can be done.
         */
        FILE_ACCEPTED_BASIC,
        /**
         * Signifies that this file was accepted, with indication that in-depth analysis can be done, but is more likely to be much slower than basic analysis.
         */
        FILE_ACCEPTED_FULL
    }
    
    /**
     * Commits analysis on the given FSNode, and returns a result. 
     * If the level is appropriate and analysis can be done, the result object should contain all relevant properties for the file
     * <p>
     * After this method has been executed, getTypeIdentifier() may be called if needed.
     * 
     * @param level Analysis level
     * @param file A node describing the file to be analyzed
     * @return Result of this analysis. i
     */
    public AnalysisResultObject analyze(AnalysisLevel level, FSNode file);
    
    /**
     * May return a list of possible search keys; if a non-NULL list is returned, it should be assumed that results can be indexed with these keys
     * @return NULL, or a list of possible keys
     */
    public Set<AnalysisResultObject.AnalysisEntry> getPossibleSearchKeys();
    
}
