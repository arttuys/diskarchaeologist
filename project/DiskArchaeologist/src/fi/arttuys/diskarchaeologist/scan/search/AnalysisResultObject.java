/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.search;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

/**
 *
 * A simple wrapper encompassing the results of an analysis; the level, and the
 * resulting definitions the analyzer wants to set
 *
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class AnalysisResultObject {

    /**
     * An key object for an entry on the analysis results map. As this will be
     * used for a key, special methods must be used
     */
    public static class AnalysisEntry {

        @Override
        public int hashCode() {
            int hash = 37 * category.hashCode() + identifier.hashCode();
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final AnalysisEntry other = (AnalysisEntry) obj;
            if (!Objects.equals(this.getCategory(), other.getCategory())) {
                return false;
            }
            
            // If nothing else sticks, finally compare by identifier
            return Objects.equals(this.getIdentifier(), other.getIdentifier());
        }

        private final String category;
        private final String identifier;

        /**
         * Initializes a new analysis entry; basically a two-part key
         * @param category Category of this entry
         * @param identifier The actual title for this entry
         */
        public AnalysisEntry(String category, String identifier) {
            this.category = category;
            this.identifier = identifier;
        }

        /**
         * Gets the category of this entry
         * @return Category
         */
        public String getCategory() {
            return category;
        }

        /**
         * Gets the content identifier of this entry
         * @return Identifier
         */
        public String getIdentifier() {
            return identifier;
        }

    }

    /**
     * Type of results for this analysis
     */
    private final FileTypeRecognizer.AnalysisResultType resultType;
    /**
     * Properties related to the analysis, or NULL if inappicable
     */
    private final Map<AnalysisEntry, Object> properties;
    /**
     * Type identifier for the file described by this object
     */
    private final String typeIdentifier;

    /**
     * Constructs a new analysis result container, containing information about
     * the executed analysis
     *
     * @param resultType Type of result
     * @param properties If needed, a map of properties related to this file
     * @param typeIdentifier If needed, a single-line human-readable type
     * identifier for this file
     */
    public AnalysisResultObject(FileTypeRecognizer.AnalysisResultType resultType, Map<AnalysisEntry, Object> properties, String typeIdentifier) {
        this.resultType = resultType;
        this.properties = properties == null ? null : Collections.unmodifiableMap(properties);
        this.typeIdentifier = typeIdentifier;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.resultType);
        hash = 97 * hash + Objects.hashCode(this.properties);
        hash = 97 * hash + Objects.hashCode(this.typeIdentifier);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        //Equal in case of reference equality, or having a equal contents, result type and type identifier
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnalysisResultObject other = (AnalysisResultObject) obj;

        return (other.getProperties().equals(this.properties) && other.getResultType().equals(this.resultType) && other.getTypeIdentifier().equals(this.typeIdentifier));

    }

    /**
     * Returns the type of the result for this analysis result object
     *
     * @return Enum describing the result
     */
    public FileTypeRecognizer.AnalysisResultType getResultType() {
        return resultType;
    }

    /**
     * Gets the associated properties for this result. While it is strongly
     * desired for higher level requests to return extended metadata, it is not
     * strictly required.
     *
     * @return A Map containing the results or NULL if not available (error, or
     * request made at a potential level)
     */
    public Map<AnalysisEntry, Object> getProperties() {
        return properties;
    }

    /**
     * Gets the type identifier; as each analysis is bound to a file, each file
     * can have a single human-readable identifier on its type
     *
     * @return The type or NULL if inapplicable
     */
    public String getTypeIdentifier() {
        return typeIdentifier;
    }
}
