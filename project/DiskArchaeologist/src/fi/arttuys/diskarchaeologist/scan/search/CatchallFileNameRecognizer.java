/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.search;

import fi.arttuys.diskarchaeologist.scan.node.FSNode;
import java.util.Set;

/**
 *
 * A simple recognizer that does no analysis beyond the name, and accepts all files
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class CatchallFileNameRecognizer implements FileTypeRecognizer {

    @Override
    /**
     * Gets the priority for this analyzer; in this case, this is a catchall analyzer
     */
    public Priority getPriority() {
        return Priority.CATCHALL;
    }

    @Override
    public AnalysisResultObject analyze(AnalysisLevel level, FSNode file) {
        //Always accept
        return new AnalysisResultObject(AnalysisResultType.FILE_ACCEPTED_POTENTIAL, null, getTypeIdentifier(file));
    }

    private String getTypeIdentifier(FSNode file) {
        String fileName = file.getUnderlyingFile().getName();
        
        //We can safely assume that file names have at least one character
        boolean hiddenFile = fileName.charAt(0) == '.';
        int indexOfLastDot = fileName.lastIndexOf(".");
        
        if (indexOfLastDot == -1 || indexOfLastDot == fileName.length()-1) return "Unidentifiable file"; //No extension, or dot at end
        if (indexOfLastDot == 0) return "Unidentifiable, possibly hidden (*nix) file";
        
        //Cannot fail: dot not at end
        return fileName.substring(indexOfLastDot+1).toUpperCase() + " file";
    }

    @Override
    public Set<AnalysisResultObject.AnalysisEntry> getPossibleSearchKeys() {
        return null;
    }
    
    
}
