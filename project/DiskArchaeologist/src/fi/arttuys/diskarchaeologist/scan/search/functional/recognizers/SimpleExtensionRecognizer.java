/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.search.functional.recognizers;

import fi.arttuys.diskarchaeologist.scan.search.FileTypeRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.functional.AbstractFluidRecognizer;
import fi.arttuys.diskarchaeologist.scan.search.functional.FluidFileRecognizer;
import java.util.HashMap;

/**
 * 
 * A simple extension recognizer, designed to identify by extension several very common file types
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
@FluidFileRecognizer(priority = FileTypeRecognizer.Priority.BASIC) // This is a basic recognizer with not much ability beyond guesswork
public class SimpleExtensionRecognizer extends AbstractFluidRecognizer{

    private static final HashMap<String,String> EXTENSION_TO_TYPE_MAP = new HashMap<>();
    
    static {
        // Textual documents
        EXTENSION_TO_TYPE_MAP.put("TXT", "a text file");
        EXTENSION_TO_TYPE_MAP.put("DOC", "MS Word document");
        EXTENSION_TO_TYPE_MAP.put("PDF", "Adobe PDF document");
        EXTENSION_TO_TYPE_MAP.put("WRI", "Microsoft Write document");
        EXTENSION_TO_TYPE_MAP.put("RTF", "Rich Text Format document");
        EXTENSION_TO_TYPE_MAP.put("HLP", "Windows Help manual");
        EXTENSION_TO_TYPE_MAP.put("ODT", "Open Document Format document");

        // Source code files
        EXTENSION_TO_TYPE_MAP.put("C", "C source code");
        EXTENSION_TO_TYPE_MAP.put("CPP", "C++ source code");
        EXTENSION_TO_TYPE_MAP.put("JS", "JavaScript source code");
        EXTENSION_TO_TYPE_MAP.put("PAS", "Pascal source code");
        EXTENSION_TO_TYPE_MAP.put("JAVA", "Java source code");
        
        // Executables
        EXTENSION_TO_TYPE_MAP.put("EXE", "DOS/Windows executable");
        EXTENSION_TO_TYPE_MAP.put("COM", "DOS executable");
        EXTENSION_TO_TYPE_MAP.put("ELF", "ELF format executable");
        EXTENSION_TO_TYPE_MAP.put("SCR", "Windows screensaver");
        
        // Pictures
        EXTENSION_TO_TYPE_MAP.put("JPG", "JPEG image");
        EXTENSION_TO_TYPE_MAP.put("JPEG", "JPEG image");
        EXTENSION_TO_TYPE_MAP.put("GIF", "Graphics Interchange Format image");
        EXTENSION_TO_TYPE_MAP.put("NEF", "Nikon RAW format image");
        
        // Web
        EXTENSION_TO_TYPE_MAP.put("HTML", "Hypertext Markup Language source code");
        EXTENSION_TO_TYPE_MAP.put("HTM", "Hypertext Markup Language source code");
        EXTENSION_TO_TYPE_MAP.put("CSS", "Cascading Style Sheet file");
    }
    
    private String extensionData = null;
    private boolean extensionDataSet = false;
    /**
     * Gets an uppercase extension for a node
     * @return NULL if not identifiable, a string otherwise
     */
    private String getUppercaseExtension() {
        String fileName = this.getBaseNode().getUnderlyingFile().getName();
        
        //We can safely assume that file names have at least one character
        int indexOfLastDot = fileName.lastIndexOf(".");
        
        if (indexOfLastDot == -1 || indexOfLastDot == fileName.length()-1) return null; //No extension, or dot at end
        if (indexOfLastDot == 0) return null;
        
        //Cannot fail: dot not at end
        return fileName.substring(indexOfLastDot+1).toUpperCase();
    }
    
    /**
     * On first call, analyzes the extension and caches the data
     */
    private void processExtension() {
        if (!extensionDataSet) {
            extensionData = getUppercaseExtension();
            extensionDataSet = true;
        }
    }
    
    @Override
    protected FileTypeRecognizer.AnalysisResultType analyze(FileTypeRecognizer.AnalysisLevel level) throws Exception {
        processExtension();
        assertTrue(extensionData != null); // Must not be null
        assertTrue(level == FileTypeRecognizer.AnalysisLevel.POTENTIAL); // Must be at potential level
        assertTrue(EXTENSION_TO_TYPE_MAP.containsKey(extensionData)); // Must contain as key
        
        //OK, accepted
        return FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_POTENTIAL;
    }

    @Override
    protected String getTypeIdentifier() {
        return "Possibly " + EXTENSION_TO_TYPE_MAP.get(extensionData);
    }
    
}
