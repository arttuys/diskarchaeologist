/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.node.utils;

import java.text.Collator;
import java.util.Comparator;
import java.util.regex.Pattern;

/**
 * A helper class for working with extended property keys, as prescribed in <code>FSNode</code>, they do have a very set form required. Therefore it is pertinent to have a set of special helper methods for them
 * <br>
 * Each key must consist of arbitrary characters split by dots; there must not be two dots without an intervening explanation
 * This class used to use Collator to sort keys, but no longer does so due to efficiency requirements
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class ExtendedPropertyKeyComparator implements Comparator<String> {
    
    
    /**
     * Initialize an extended property key comparator; this doesn't require any special operations other than initializing the collator for alphabetic sorting
     */
    public ExtendedPropertyKeyComparator() {
    }

    /**
     * Verifies if a given string is a valid key split with zero or more dots
     * @param s A potential key
     * @return TRUE if yes, FALSE if no
     */
    public static boolean validKeyString(String s) {
        return Pattern.matches("^[^.\r\n]+(\\.[^.\r\n]+)*$", s);
    }

    /**
     * Compares two keys, and sorts them so that the leftmost keys are sorted first, and then recursively by deeper keys
     * @param o1 Key 1
     * @param o2 Key 2
     * @return -1 if o1 is 'less' than o2, 0 if equal, and 1 if greater
     */
    @Override
    public int compare(String o1, String o2) {
        //First, require that both keys are non-null
        if (o1 == null || o2 == null) throw new NullPointerException("One or both of the compared Strings are null!");
        if (!validKeyString(o1) || !validKeyString(o2)) throw new ClassCastException("One or both of the keys are invalid!");
        
        //Split the Strings
        String[] o1_split = o1.split(Pattern.quote("."));
        String[] o2_split = o2.split(Pattern.quote("."));
        
        //Compare until we hit the lowest shared bound for both; if not inequal before that, compare so that the less length-y key by dots if before
        for (int i = 0; i < Math.min(o1_split.length, o2_split.length); i++) {
            int collateResult = o1_split[i].compareTo(o2_split[i]);
            if (collateResult != 0) return collateResult;
        }
        
        return (Integer.compare(o1_split.length, o2_split.length));
    }
    
}
