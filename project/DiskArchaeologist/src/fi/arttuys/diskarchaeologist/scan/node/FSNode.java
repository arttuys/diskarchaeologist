/*
 * The MIT License
 *
 * Copyright 2016 Arttu Ylä-Sahra <arttuys@arttuys.fi>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.node;

import com.sun.javafx.collections.ObservableListWrapper;
import fi.arttuys.diskarchaeologist.scan.node.utils.ExtendedPropertyKeyComparator;
import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.WeakInvalidationListener;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlyListWrapper;
import javafx.beans.property.ReadOnlyMapProperty;
import javafx.beans.property.ReadOnlyMapWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableMap;

/**
 *
 * File system node: contains traits for a specific node, and any possible
 * subnodes. For connecting with a GUI, a node exposes public, read only,
 * observable properties
 * <p>
 * The node doesn't update itself AFTER instantiation - if the file or
 * directory, say, disappears after it has been created, this node shall not
 * stop working.
 * <p>
 * While directories, even when empty, have a measurable size within a file
 * system, even if empty, we simplify by assuming that directories are
 * zero-sized containers for files and other directories. For some systems, it
 * perhaps would not even be possible to determine the intristic size of a
 * folder (say, a network drive), so we shall not attempt to measure that.
 * <p>
 * <strong>A warning about thread safety; calls to the non-public methods of
 * FSNode may trigger side effects in observable fields. This effect is not
 * local, and may extend to other FSNode instances.
 * </strong>
 * <p>
 * <strong>
 * Therefore, if used in conjunction with JavaFX GUI, you should be careful not to cause changes in
 * live scene objects elsewhere than in the Application thread.
 * </strong><p>
 * All nodes also contain extended properties, which is expressed as a map of
 * arbitrary objects keyed by Strings. These properties are not strictly
 * controlled, but it is expected that categories should be split by dots, like
 * ordinary Java classes. Users may define their own meanings for specific keys,
 * but FSNode itself does not rely in any part on the content of the extended
 * properties
 * <p>
 * <strong>
 * All methods that are public can not trigger side effects, and are therefore safe
 * to call from anywhere.
 * </strong>
 *
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public final class FSNode {

    /**
     * A private change listener; this listener can check wherever the node
     * sending changes is still recognized by us as a child node, and block it
     * if needed It is a property of Java nested classes that they retain access
     * to their parent instances
     */
    private class FSNodeSizeChangeListener implements ChangeListener<BigInteger> {

        private FSNode sendingNode;

        public FSNodeSizeChangeListener(FSNode sendingNode) {
            if (sendingNode == null) {
                throw new IllegalArgumentException("The intended sending node must not be null!");
            }
            if (!sendingNode.isDirectory()) {
                throw new IllegalArgumentException("Sending node is NOT a directory!");
            }
            this.sendingNode = sendingNode;
        }

        @Override
        public void changed(ObservableValue<? extends BigInteger> observable, BigInteger oldValue, BigInteger newValue) {
            //Assuming that this event is triggered in changeContent(), which implicitly means that any other size changes cannot take place
            //Therefore, we can safely assume that for the duration of this method call, neither parentage or our size can change

            //Calculate the difference.
            BigInteger diff = newValue.subtract(oldValue);
            //Change the difference
            changeContentSize(diff, sendingNode);
        }

    }

    /**
     * A simple binding to calculate the amounts of subnodes on demand
     */
    private class FSNodeSubnodeCountBinding extends ObjectBinding<BigInteger> {

        private InvalidationListener subnodeInvalidator;
        private WeakInvalidationListener weakSubnodeInvalidator;

        public FSNodeSubnodeCountBinding() {
            if (FSNode.this.isDirectory()) {

                //Create a new invalidator
                subnodeInvalidator = new InvalidationListener() {
                    @Override
                    public void invalidated(Observable observable) {
                        synchronized (totalSubnodeCountChangeSynchObject) {
                            FSNodeSubnodeCountBinding.this.invalidate();
                        }
                    }

                };

                //And a weak adaptation. It will work as long as we retain a strong reference to our actual invalidator; which will not be passed outside this object - and this object will disappear when all nodes of a tree go out of scope
                this.weakSubnodeInvalidator = new WeakInvalidationListener(subnodeInvalidator);

                FSNode.this.subnodes.addListener(new ListChangeListener<FSNode>() {
                    @Override
                    public void onChanged(ListChangeListener.Change<? extends FSNode> c) {
                        synchronized (totalSubnodeCountChangeSynchObject) {
                            while (c.next()) {
                                //Update our subnode listeners where needed
                                if (c.wasAdded()) {
                                    for (FSNode subnode : c.getAddedSubList()) {
                                        subnode.getSubnodeCount().addListener(weakSubnodeInvalidator);
                                    }
                                }
                                if (c.wasRemoved()) {
                                    for (FSNode subnode : c.getRemoved()) {
                                        subnode.getSubnodeCount().removeListener(weakSubnodeInvalidator);
                                    }
                                }
                            }
                        }

                        //And invalidate ourselves
                        FSNodeSubnodeCountBinding.this.invalidate();
                    }
                });
            }

        }

        /**
         * Computes a value for the subnode count
         *
         * @return A total count of subnodes
         */
        @Override
        protected BigInteger computeValue() {
            if (!FSNode.this.isDirectory()) {
                return BigInteger.ZERO;
            } else {
                BigInteger calculation = BigInteger.ZERO;

                synchronized (totalSubnodeCountChangeSynchObject) { //Prevent a race condition by assuring that no modifications can take place during this calculation
                    for (FSNode subnode : FSNode.this.subnodes) {
                        calculation = calculation.add(BigInteger.ONE).add(subnode.getSubnodeCount().get()); //This plus undernodes
                    }
                }

                return calculation;
            }
        }

    }

    /**
     * Underlying file for this node. Naturally, cannot change - a node will
     * always point to a certain single file
     */
    private final File underlyingFile;

    /**
     * Parent node for this node. 
     * <p>
     * A strong reference is held, because the tree
     * will be irrelevant only when no part of it is accessible. It is assumed
     * Java is wise enough to be not tripped by circular references in a system
     * to which there are no external references
     */
    private volatile FSNode parentNode;

    /**
     * Length of this node; for a file, it is the length of the contents, for a
     * directory, it is zero. Will not change after creation
     */
    private volatile ReadOnlyObjectWrapper<BigInteger> localNodeSize;

    /**
     * Length of this node; for a file, length of contents, for a directory the
     * sum of its contents. May change after creation!
     */
    private volatile ReadOnlyObjectWrapper<BigInteger> totalNodeSize;

    /**
     * Count of subnodes; always zero for a file, zero or more for a folder.
     */
    private volatile ObjectBinding<BigInteger> subnodeCount;

    /**
     * Is this node a directory?
     */
    private boolean nodeIsDirectory;

    /**
     * List of all subnodes for this item
     */
    private volatile ReadOnlyListWrapper<FSNode> subnodes;

    /**
     * A synchronization lock for adding or removing subnodes; this ensures that
     * multiple threads do not manipulate the list of subnodes at a time
     */
    private final Object addRemoveSynchObject = new Object();

    /**
     * A synchronization lock for changing the total, apparent size of this
     * node.
     */
    protected final Object totalSizeChangeSynchObject = new Object();

    /**
     * A synchronization lock for changing the count of subnodes
     */
    protected final Object totalSubnodeCountChangeSynchObject = new Object();

    /**
     * Counts if the size has been assigned once before; all nodes can have at
     * least one size assignment, but only directories can have many! If FALSE,
     * there has been already one size assignment
     */
    private Boolean firstSizeAssign = true;

    /**
     * A map holding ChangeListeners, so that they don't get garbage collected
     * prematurely
     */
    private Map<FSNode, ChangeListener<BigInteger>> changeListenerHolder;

    /**
     * A map holding extended properties
     */
    private ReadOnlyMapWrapper<String, Object> extendedPropertyMap;

    /**
     * Creates a root node; that is, a node that is a directory, and has no
     * parent
     *
     * @param dir Directory
     * @return A new node
     * @throws java.io.FileNotFoundException If the given File does not exist
     */
    protected static FSNode createRootNode(File dir) throws FileNotFoundException {
        if (dir == null) {
            throw new IllegalArgumentException("The root node must not be null!");
        }
        if (!dir.exists()) {
            throw new FileNotFoundException("Root node must be an existent directory!");
        }
        if (!dir.isDirectory()) {
            throw new IllegalArgumentException("Root node must be a directory!");
        }
        return new FSNode(dir, null);
    }

    /**
     * Initializes an actual instance of the FSNode node object
     * 
     * @param underlyingFile File serving as the basis for this node.
     * @param parentNode Parent node for this file, if any
     *
     * Initializes a new FSNode, which will represent a certain file or
     * directory in the file system.
     */
    private FSNode(File underlyingFile, FSNode parentNode) throws FileNotFoundException {
        if (underlyingFile == null) {
            throw new IllegalArgumentException("File may not be null!");
        }

        //We should not store records of imaginary files
        if (!underlyingFile.exists()) {
            throw new FileNotFoundException("The given file or directory must exist!");
        }

        //We demand that the file must either be a perfectly normal, measurable (we can determine the size) file, or a directory.
        //While it could be technically possible that someone generates a directory loop (see XKCD #981), we shall not try to address it here due to complexity and rarity. 
        //We'll just hope no one presents us such a pathological directory structure.
        if (!underlyingFile.isFile() && !underlyingFile.isDirectory()) {
            throw new IllegalArgumentException("The given file is not a normal file, nor a directory");
        }

        //Assign
        this.underlyingFile = underlyingFile;
        this.parentNode = parentNode;

        //We cannot have a file node as a parent. Block that
        if (parentNode != null && !parentNode.isDirectory()) {
            throw new IllegalArgumentException("A file cannot be a parent node to any node!");
        }

        //Generate a new ChangeListener
        this.changeListenerHolder = Collections.synchronizedMap(new HashMap<>());

        //Initialize the list of subnodes. Use a synchronized list backing, so that the underlying list is somewhat thread-safe
        this.subnodes = new ReadOnlyListWrapper<>(FXCollections.synchronizedObservableList(new ObservableListWrapper(new ArrayList<>())));

        //Initialize a new property hash map
        this.extendedPropertyMap = new ReadOnlyMapWrapper<>(FXCollections.synchronizedObservableMap(FXCollections.observableMap(new TreeMap<>(new ExtendedPropertyKeyComparator()))));

        //Populate internal variables
        populateInternalVariables();

        //We're done.
    }

    /**
     * Is this node a directory node?
     *
     * @return TRUE if yes, FALSE if no
     */
    public boolean isDirectory() {
        return this.nodeIsDirectory;
    }

    /**
     * Returns the parent node of this node
     *
     * @return Node, or NULL if none
     */
    public FSNode getParentNode() {
        return this.parentNode;
    }

    /**
     * Populates certain internal variables related to the file; as defined, we
     * shall cache the features we need
     */
    private void populateInternalVariables() {
        this.nodeIsDirectory = this.underlyingFile.isDirectory();

        //If this is a directory, its size is zero at initialization; in our system, directories have no intristic size - only underlying files have.
        this.localNodeSize = new ReadOnlyObjectWrapper<>(this.nodeIsDirectory ? BigInteger.ZERO : new BigInteger(Long.toString(this.underlyingFile.length())));
        //Initialize our total size
        this.totalNodeSize = new ReadOnlyObjectWrapper<>(BigInteger.ZERO);
        //Update our total size
        changeContentSize(this.localNodeSize.get(), null);

        this.subnodeCount = new FSNodeSubnodeCountBinding();
    }

    /**
     * Returns a read-only observable property for the intristic size of this
     * specific node AND only this one specific node. Naturally, it follows that
     * the size of the directory will always remain zero This will never change
     * after instantiation
     *
     * @return Size of this node as an observable property
     */
    public ReadOnlyObjectProperty<BigInteger> getLocalSizeProperty() {
        return this.localNodeSize.getReadOnlyProperty();
    }

    /**
     * Returns a read-only observable property for the size of this node plus
     * total sizes of all direct subnodes. It follows that since all subnodes
     * also do the same, this tells the total size of this node, its direct
     * subnodes and any further nodes reachable from subnodes This may change
     * after instantiation.
     *
     * @return Total size of this node as an observable property
     */
    public ReadOnlyObjectProperty<BigInteger> getTotalSizeProperty() {
        return this.totalNodeSize.getReadOnlyProperty();
    }

    /**
     * Returns a read-only list of all subnodes for this node.
     *
     * @return Subnodes in an observable, read-only list
     */
    public ReadOnlyListProperty<FSNode> getSubnodeProperty() {
        return this.subnodes.getReadOnlyProperty();
    }

    /**
     * Returns the underlying file or directory for this node.
     *
     * @return Underlying file or directory, as it was given to this node on
     * initialization
     */
    public File getUnderlyingFile() {
        return this.underlyingFile;
    }

    /**
     * Changes the size of this node, and informs the parent node if applicable
     *
     * @param diff Difference between old and new size
     * @param changeInstigator NULL if internal, a FSNode if external.
     */
    private void changeContentSize(BigInteger diff, FSNode changeInstigator) {
        //Require that no size changes are taking place upon calling
        synchronized (totalSizeChangeSynchObject) {
            if (diff.equals(BigInteger.ZERO)) {
                return; //No change, no further action needed
            }
            if (!this.nodeIsDirectory && !firstSizeAssign) {
                throw new IllegalStateException("This node is not a directory, and therefore its size cannot change after creation");
            }

            //Verify if our instigator still exists here
            if (changeInstigator != null) {
                if (changeInstigator.getParentNode() != this || !subnodes.contains(changeInstigator)) {
                    return; //Not recognized by one party, do not accept.
                }
            }

            BigInteger oldTotalSize = this.totalNodeSize.get();
            BigInteger newTotal = oldTotalSize.add(diff);

            if (newTotal.compareTo(BigInteger.ZERO) == -1) {
                throw new IllegalArgumentException("Total size would be less than zero with this change!");
            }

            //Set our length
            this.totalNodeSize.set(newTotal);

            //Prevent reassignment to a file!
            firstSizeAssign = false;
        }
    }

    /**
     * Adds a new node under this node. This will be immediately reflected in
     * the size of this node, and possibly in parent nodes. The equality of
     * files will not be tested
     *
     * @param file File
     * @return The node that was added
     * @throws java.io.FileNotFoundException If the given File does not exist
     */
    protected FSNode addNewSubnode(File file) throws FileNotFoundException {
        synchronized (addRemoveSynchObject) {
            if (!this.nodeIsDirectory) {
                throw new IllegalArgumentException("This is not a directory, and therefore should not have child nodes!");
            }

            //Let's initialize the subnode
            FSNode fsn = new FSNode(file, this);

            //Add it to our list
            this.subnodes.add(fsn);

            if (fsn.isDirectory()) {
                //A directory. Add a listener
                ChangeListener<BigInteger> changeObserver = new FSNodeSizeChangeListener(fsn);

                //Add a weak reference observer; this avoids memory leaks
                fsn.getTotalSizeProperty().addListener(new WeakChangeListener<>(changeObserver));

                //Add it to our map
                changeListenerHolder.put(fsn, changeObserver);

                //A directory has a total size of zero. We do not yet need to alter our size
            } else {
                //A file node. Just adjust our size, as it does not change
                changeContentSize(fsn.getLocalSizeProperty().get(), null);
            }

            //Return the node we just added
            return fsn;
        }
    }

    /**
     * Detaches this node from its parent node, making it a "root node" of its
     * own. Note that nodes cannot be retached once detached!. Also returns the
     * size that was in force during the time detachment was done
     *
     * @return The total size of this node at the point when it was disjointed
     * from its parent node
     */
    protected BigInteger disjointFromParent() {
        //Require that no size changes are taking place while we detach. Size changes may happen inside add/remove operations, but not vis a versa - so let's ensure that we do not accidentally cause a deadlock
        synchronized (addRemoveSynchObject) {
            synchronized (totalSizeChangeSynchObject) {
                this.parentNode = null;
                return this.totalNodeSize.get();
            }
        }
    }

    /**
     * Removes a subnode from this node.
     *
     * @param child Node to be removed
     */
    protected void removeSubnode(FSNode child) {
        synchronized (addRemoveSynchObject) {
            if (!this.nodeIsDirectory) {
                throw new IllegalArgumentException("This is not a directory, and therefore should not have child nodes!");
            }
            if (!this.subnodes.contains(child)) {
                throw new IllegalArgumentException("This node is not a parent for the given node");
            }

            //Detach from the parent. It is guaranteed that once this method completes, our size cannot change due to the child. It also ensures that we get the latest size that was seen by us aswell
            BigInteger negSize = child.disjointFromParent().negate();

            //Remove from the list
            this.subnodes.remove(child);

            //Remove our listener
            changeListenerHolder.remove(child);

            //Update the size of the content, having removed this
            changeContentSize(negSize, null);

        }
    }

    /**
     * Removes ALL subnodes of this node, and resets size back to zero.
     */
    protected void removeAllSubnodes() {
        //Our removal procedure so far is a bit complicated.. let's for now alias it
        for (FSNode fsn : new ArrayList<>(subnodes)) {
            removeSubnode(fsn);
        }

    }

    /**
     * Returns an unrestricted view to the extended property map.
     * <strong>Be advised that editing this map may cause side effects in views,
     * and therefore should be done with utmost consideration (e.g JavaFX
     * application thread)!</strong>
     *
     * @return An unrestricted view to this node's map
     */
    protected ObservableMap<String, Object> getExtendedPropertyMap() {
        return this.extendedPropertyMap.get();
    }

    /**
     * Returns a binding to the current subnode count for this node and any of
     * its subnodes
     *
     * @return Binding
     */
    public ObjectBinding<BigInteger> getSubnodeCount() {
        return subnodeCount;
    }

    /**
     * Returns a read-only view to the extended properties of this map
     *
     * @return A read-only view to extended properties
     */
    public ReadOnlyMapProperty<String, Object> getReadOnlyExtendedPropertyMap() {
        return extendedPropertyMap.getReadOnlyProperty();
    }
}
