/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.node;

import fi.arttuys.diskarchaeologist.scan.search.AnalysisResultObject;
import fi.arttuys.diskarchaeologist.scan.search.AnalysisResultObject.AnalysisEntry;
import fi.arttuys.diskarchaeologist.scan.search.CollatingFileTypeFinder;
import fi.arttuys.diskarchaeologist.scan.search.FileTypeRecognizer;
import fi.arttuys.diskarchaeologist.utils.CallableWrapper;
import fi.arttuys.diskarchaeologist.utils.EditBoxParser;
import fi.arttuys.diskarchaeologist.utils.SizeToStringConverter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.ObservableMap;

/**
 *
 * FileTreeManager, which manages the scanning and other mass operations
 * presented on FSNode trees.
 *
 * FTM can support a 'wrapper generator', which is intended to assist with the
 * usage of JavaFX application thread. 
 * <br>
 * If so desired, <strong>almost all FSNode-
 * and observable state-altering changes</strong> are executed using the wrapper
 * generator. <strong>This is only guaranteed for functionality originated from
 * FileTreeManager, other users of this class may have other constraints or
 * requirements</strong>
 *
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class FileTreeManager {


    /**
     * A helper that enumerates files until interrupted or succeeded A private
     * inner class for FTM; not being a static inner class, it can access FTM's
     * internal structures which are vital.
     *
     */
    private class FileTreeEnumerator implements Runnable {

        private final BlockingQueue<FSNode> foldersToBeScanned;

        public FileTreeEnumerator(FSNode root) {
            foldersToBeScanned = new LinkedBlockingQueue<>();
            foldersToBeScanned.add(root);
        }

        @Override
        public void run() {
            //We shall utilize the old paradigm of having a stack for folders to scan. 
            try {

                while (!foldersToBeScanned.isEmpty()) {
                    if (requestToStop) {
                        throw new InterruptedException("Interrupted at the start of a cycle");
                    }

                    FSNode nextNode = foldersToBeScanned.poll(); //Is never empty
                    changeStateString("Scanning folder " + nextNode.getUnderlyingFile().getAbsolutePath());
                    // Start enumerating
                    final List<File> foundFiles = new ArrayList<>();
                    final List<File> foundFolders = new ArrayList<>();

                    File[] filesUnderRoot = nextNode.getUnderlyingFile().listFiles();

                    if (filesUnderRoot != null) {
                        for (File subfile : filesUnderRoot) {
                            if (subfile.isDirectory()) {
                                //Folder. Add to the folder list
                                foundFolders.add(subfile);
                            } else if (subfile.isFile()) {
                                //File. Add to the file list
                                foundFiles.add(subfile);
                            } //No other situations are possible.
                        }
                    }

                    //Define a callable action that actually adds the subnodes
                    Callable<List<FSNode>> fsNodeAction = new Callable<List<FSNode>>() {
                        @Override
                        public List<FSNode> call() {
                            for (File f : foundFiles) {
                                //Add a file
                                try {
                                    FSNode newNode = nextNode.addNewSubnode(f);
                                    // It is implicitly assumed that creating potential analyzes will not take excessively long
                                    CollatingFileTypeFinder cftf = new CollatingFileTypeFinder(newNode);
                                    //Advance the state forward, namely getting basic analysis data
                                    cftf.advanceState();
                                    newNode.getExtendedPropertyMap().put(FTMConstants.FSN_COLLATING_FINDER_STORAGE_KEY, cftf);
                                    newNode.getExtendedPropertyMap().put(FTMConstants.FSN_ANALYSIS_RESULT_OBJECT_KEY, cftf.getLatestAnalysisResult());
                                } catch (FileNotFoundException fe) {
                                    //Ignore, irrelevant now
                                }
                            }

                            //Return a stream access to our list of folders
                            return foundFolders.stream().map((Function<? super File, FSNode>) (t) -> {
                                try {
                                    return nextNode.addNewSubnode(t);
                                } catch (FileNotFoundException fe) {
                                    return null;
                                }
                            }).filter((t) -> {
                                return t != null;
                            }).collect(Collectors.toList());

                        }
                    };

                    // Call our callable
                    List<FSNode> folderNodes;
                    Callable<List<FSNode>> wrappedCall = wrapperGenerator.wrapCallable(fsNodeAction);
                    folderNodes = wrappedCall.call();

                    if (folderNodes == null) {
                        throw new NullPointerException("Did not get a list of folder nodes from our enumerator!");
                    }

                    if (requestToStop) {
                        throw new InterruptedException("Interrupted at the start of a cycle");
                    }

                    folderNodes.iterator().forEachRemaining((t) -> {
                        // Add any folder nodes to our queue
                        foldersToBeScanned.add(t);
                    });

                    //We're finished now
                }
            } catch (InterruptedException ie) {
                changeStateString(null);
                taskAboutToStop(State.STOPPED_MUST_REINIT);
            } catch (Exception e) {
                changeStateString("Exception while enumerating: " + e.toString() + ". Enumeration stopped.");
                taskAboutToStop(State.STOPPED_MUST_REINIT);
            } finally {
                if (currentState.get() == State.ENUMERATING) {
                    taskAboutToStop(State.IDLE_SUCCESS);
                }
            }
        }

    }

    /**
     * State enumerations; describing current processing state
     */
    public enum State {
        /**
         * Enumeration has not started yet, must be done first
         */
        NOT_STARTED,
        /**
         * Enumeration in progress
         */
        ENUMERATING,
        /**
         * Postprocessing operation of some kind in progress
         */
        POSTPROCESSING,
        /**
         * Successfully completed last operation, may call any postprocessing
         * operation. Any IDLE_SUCCESS implies that enumeration has completed
         * properly at least once.
         */
        IDLE_SUCCESS,
        /**
         * Fail or a stop, in such a way that a new instance of FTM should be
         * instantiated.
         */
        STOPPED_MUST_REINIT,
        /**
         * Fail or stop (likely in postprocessing), in such a way that it may be
         * retried
         */
        STOPPED_MAY_RETRY,

    }

    /**
     * The root node for this <code>FileTreeManager</code>; fixed at creation
     */
    private FSNode treeManagerRootNode;
    /**
     * The wrapper generator; all FSNode-altering calls are executed from here
     */
    private final CallableWrapper wrapperGenerator;

    /**
     * The current state of this FTM
     */
    private final ReadOnlyObjectWrapper<State> currentState;

    /**
     * If <code>FileTreeManager</code> wishes to indicate something, it will be done via this string.
     */
    private final ReadOnlyStringWrapper stateString;

    /**
     * Current task thread running
     */
    private volatile Thread currentManagedTaskThread;

    /**
     * A flag that denotes a thread should stop safely at the earliest
     * opportunity. It seems that the InterruptedException can pop up in
     * undesirable places, so we should use a special thread-safe flag variable
     * to have the same functionality
     */
    private volatile boolean requestToStop;

    /**
     * A flag denoting that even though a thread may be running, it is
     * guaranteed to stop very soon and should for all intents and purposes to
     * be considered not alive
     */
    private volatile boolean taskStoppedCleanly;

    /**
     * The user may request a grid to be generated from the found files. This
     * variable stores that
     */
    private volatile FileGrid latestGrid;

    /**
     * Initializes a <code>FileTreeManager</code> for a certain root directory, utilizing the
     * indicated wrapper generator
     *
     * @param startingPoint The starting path
     * @param wrapperGenerator If special threading considerations are required,
     * a wrapper generator ensure that all observable changes are executed using
     * this thread
     * @throws FileNotFoundException If the file was not found
     * @throws IOException If there was some other error
     */
    public FileTreeManager(File startingPoint, CallableWrapper wrapperGenerator) throws FileNotFoundException, IOException {
        this.treeManagerRootNode = FSNode.createRootNode(startingPoint);

        //Change the root node's display name to something representative
        this.treeManagerRootNode.getExtendedPropertyMap().put(FTMConstants.FSN_ALTERNATIVE_NAME_KEY, this.treeManagerRootNode.getUnderlyingFile().getCanonicalPath());
        //If null, do not utilize
        this.wrapperGenerator = wrapperGenerator == null ? new CallableWrapper() {
            @Override
            public <T> Callable<T> wrapCallable(Callable<T> callable) {
                return callable;
            }
        } : wrapperGenerator;

        this.currentState = new ReadOnlyObjectWrapper<>(State.NOT_STARTED);
        this.taskStoppedCleanly = true;
        this.requestToStop = false;
        this.stateString = new ReadOnlyStringWrapper(null);
        this.latestGrid = null;
    }

    /**
     * Sets some thread as the current (managed) task thread and start it
     * @param t 
     */
    private void setAsManagedThreadAndStart(Thread t) {
        requestToStop = false;
        taskStoppedCleanly = false;
        currentManagedTaskThread = t;
        t.start();
    }

    /**
     * A helper method to denote when a task has completed even though it is
     * nominally still running, and therefore should be considered so for
     * intents of observable variables
     *
     * @param newState The state we should now change to
     */
    private void taskAboutToStop(State newState) {
        this.taskStoppedCleanly = true;
        changeStateInWrapper(currentState, newState);
    }

    /**
     * A directory-oriented processor; for each call of the analysis function a
     * root node, and a mapping from subnodes to their extended properties is
     * provided. Unlike the simple postprocessing function, this function can be
     * guaranteed to be safe for UI purposes, as any Callable returned by the
     * function is executed using the wrapper. It is guaranteed that all
     * subfolders have been processed before a given folder has been processed.
     *
     * @param analysisFunction The analysis function; it is given the root
     * directory, and all of its subfiles with their extended properties. It may
     * return a callable to be called in a wrapper
     * @param includeDirectoriesInMap If this is TRUE, directories are also
     * included in the map; if FALSE, they are not
     * @param statusMessage Status message to be shown while analyzing
     * @param postProcessCall A call that is executed immediately after analysis concludes, still in the analysis thread
     * @param postProcessWrapperCall A call to be called in the wrapper after
     * the main procedures have completed - but BEFORE declaring the process as
     * closed.
     */
    public void wrapperSafeDirectoryPostProcessing(BiFunction<FSNode, Map<FSNode, ObservableMap<String, Object>>, Callable> analysisFunction, boolean includeDirectoriesInMap, String statusMessage, Runnable postProcessCall, Runnable postProcessWrapperCall) {
        if (hasNotEnumerated() || !isIdle(true)) {
            throw new IllegalStateException("Postprocessing may only be committed when idle");
        }
        if (analysisFunction == null) {
            throw new IllegalArgumentException("Consumer function may not be null!");
        }
        try {
            // Change the status message if applicable
            changeStateInWrapper(currentState, State.POSTPROCESSING);
            if (statusMessage != null) {
                changeStateString(statusMessage);
            }

            // Initialize the directory analysis thread
            Thread analysisThread = new Thread(new Runnable() {
                private void analyzeNode(FSNode f) throws Exception {

                    if (requestToStop) {
                        throw new InterruptedException("Interrupted at the start of a cycle");
                    }

                    HashMap nodemap = new HashMap(f.getSubnodeProperty().getSize()); // We already know the amount that we need.

                    // Process each subnode
                    for (FSNode subNode : f.getSubnodeProperty().get()) {
                        if (subNode.isDirectory()) {
                            analyzeNode(subNode); // Recursive call to the directory subnode
                        }
                        if (includeDirectoriesInMap || !subNode.isDirectory()) {
                            nodemap.put(subNode, subNode.getExtendedPropertyMap());
                        }
                    }

                    Callable wrapperFunc = analysisFunction.apply(f, nodemap); // Call the analysis function with the date

                    // If we were returned a separate function for wrapper calls, execute it
                    if (wrapperFunc != null) {
                        wrapperGenerator.wrapCallable(new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                wrapperFunc.call();
                                return null;
                            }
                        }).call();
                    }
                }

                @Override
                public void run() {
                    try {
                        analyzeNode(treeManagerRootNode);

                        // If required, in-thread post process call
                        if (postProcessCall != null) {
                            postProcessCall.run();
                        }

                        if (postProcessWrapperCall != null) {
                            wrapperGenerator.wrapCallable(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    postProcessWrapperCall.run();
                                    return null;
                                }
                            }).call();
                        }

                        // Complete
                        changeStateString(null);
                        taskAboutToStop(State.IDLE_SUCCESS);
                    } catch (InterruptedException ie) {
                        changeStateString(null);
                        changeStateInWrapper(currentState, State.STOPPED_MAY_RETRY);
                    } catch (Exception e) {
                        changeStateString("Postprocessing stopped due to an exception inside processing thread: " + e.toString());
                        changeStateInWrapper(currentState, State.STOPPED_MUST_REINIT);
                    }

                }
            });

            analysisThread.setName("DiskArchaeologist-AnalysisThread");
            setAsManagedThreadAndStart(analysisThread);

        } catch (Exception e) {
            changeStateString("Postprocessing stopped due to an exception: " + e.toString());
            changeStateInWrapper(currentState, State.STOPPED_MUST_REINIT);
        }
    }

    /**
     * Triggers the generation of a new file grid as a postprocessing event;
     *
     * @param xSize X-size of the grid
     * @param ySize Y-size of the grid
     * @param wrapperPostprocessCall After grid generation has completed, this call is executed in the wrapper
     */
    public void generateNewGrid(int xSize, int ySize, Runnable wrapperPostprocessCall) {
        // Calculate the minimum size
        if (xSize < 1 || ySize < 1) {
            throw new IllegalArgumentException("Grid dimensions must be at least 1 on both");
        }

        BigInteger gridArea = new BigInteger(Integer.toString(xSize)).multiply(new BigInteger(Integer.toString(ySize)));
        BigInteger divisionSize = this.getRootNode().getTotalSizeProperty().get(); // Ensure that we do not try to divide with zero
        BigInteger minimumSize;
        if (!divisionSize.equals(BigInteger.ZERO)) {
            BigInteger[] divisionArr = divisionSize.divideAndRemainder(gridArea);
            minimumSize = divisionArr[0];
        } else {
            minimumSize = BigInteger.ZERO;
        }
        FileGrid grid = new FileGrid(xSize, ySize, minimumSize);

        this.wrapperSafeDirectoryPostProcessing((FSNode t, Map<FSNode, ObservableMap<String, Object>> u) -> {
            for (FSNode file : u.keySet()) {
                if (file.getLocalSizeProperty().get().compareTo(minimumSize) == -1) {
                    continue; // Too small.
                }
                grid.addNode(file); //OK
            }
            return null; //No directory-specific processing required
        }, false, "Generating a grid view", new Runnable() {
            @Override
            public void run() {
                grid.completeGrid();
                FileTreeManager.this.latestGrid = grid; // Set our latest grid as this
            }
        }, wrapperPostprocessCall);

    }

    /**
     * Triggers a search query; an utility tool is used to convert queries to
     * data.
     * <br>
     * Keys are intepreted as following: - "Name" = file name - "Size" = the
     * human-readable size - any other key is intepreted verbatim as an analysis
     * result data key
     * <br>
     * Multiple queries are intepreted as an AND, meaning ALL query strings must
     * match!
     * <br>
     * Values are intepreted as regexes. If any regex encountered is invalid, an
     * exception for an invalid query is raised
     * <br>
     * This method may throw an exception if the content is not valid
     *
     * @param contentBox The search query
     */
    public void executeSearchQueries(String contentBox) {
        // First, pull in the query data
        Map<String, String> queryStringMap = EditBoxParser.parseStringToMap(contentBox);
        boolean emptyMap = queryStringMap.isEmpty();
        // Then, try to convert this data to Pattern objects
        Map<String, Pattern> queryRegexMap = new HashMap<>();
        for (String key : queryStringMap.keySet()) {
            queryRegexMap.put(key, Pattern.compile(queryStringMap.get(key)));
        }

        // Alright, we now presumably have preconverted objects. Call away
        wrapperSafeDirectoryPostProcessing((FSNode directory, Map<FSNode, ObservableMap<String, Object>> extendedProperties) -> {
            // First, we now go through the items
            boolean subnodeWithDataFound = false;
            Map<FSNode, Boolean> changesToBePosted = new HashMap<>();

            for (FSNode subnode : extendedProperties.keySet()) {
                if (subnode.isDirectory() && extendedProperties.get(subnode).get(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY) != null && extendedProperties.get(subnode).get(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY).equals(true)) {
                    // As guaranteed by the directory postprocessor, all subdirectories should have got processed at this point
                    // If we can find at least one subdirectory which indicates a search result, we should also mark our directory as such
                    subnodeWithDataFound = true;
                } else if (!subnode.isDirectory()) { // Otherwise, it must be a file
                    // Get the map; and by default, assume that there is no match
                    ObservableMap<String, Object> propertyMap = extendedProperties.get(subnode);

                    changesToBePosted.put(subnode, false);

                    Object o = propertyMap.get(FTMConstants.FSN_ANALYSIS_RESULT_OBJECT_KEY);
                    if (o == null || !(o instanceof AnalysisResultObject)) {
                        continue; // No valid ARO available. Consider this a potential problem and do not flag this as a valid file for search purposes
                    }

                    AnalysisResultObject aro = (AnalysisResultObject) o;

                    int specialKeyDiff = 0; // This counts the amount of special keys found

                    // First, check the name and sizes if available
                    if (queryRegexMap.containsKey("Name")) {
                        specialKeyDiff += 1;
                        if (!queryRegexMap.get("Name").matcher(subnode.getUnderlyingFile().getName()).find()) {
                            continue; // Name exists as a key, but does not match. Next
                        }
                    }

                    if (queryRegexMap.containsKey("Size")) {
                        specialKeyDiff += 1;
                        if (!queryRegexMap.get("Size").matcher(SizeToStringConverter.bigIntegerToSize(subnode.getLocalSizeProperty()).get()).find()) {
                            continue; // Size exists as a key, but does not match. Next
                        }
                    }

                    if ((aro.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_BASIC || aro.getResultType() == FileTypeRecognizer.AnalysisResultType.FILE_ACCEPTED_FULL) && aro.getProperties() != null) {
                        boolean mismatchedRegex = false; // If we find something that doesn't match, set this to true

                        int requiredKeyCount = queryRegexMap.size() - specialKeyDiff; // We need this many more keys over special keys

                        for (AnalysisEntry ae : aro.getProperties().keySet()) { // We cannot directly iterate over the identifiers, so we must go through all entries
                            if (!queryRegexMap.containsKey(ae.getIdentifier())) {
                                continue; // Not interested, not in the map
                            }
                            if (!queryRegexMap.get(ae.getIdentifier()).matcher(aro.getProperties().get(ae).toString()).find()) {
                                mismatchedRegex = true; //If we cannot match anything with the regex we get for the identifier, it is mismatched
                                break; // Nothing more to do
                            }
                            requiredKeyCount--;
                        }

                        if (mismatchedRegex || requiredKeyCount > 0) {
                            continue; // If it is mismatched or we did not hit all keys, continue
                        }
                    } else { // Not a valid analysis result, but we could accept it ONLY if there are no other keys
                        if (specialKeyDiff != queryRegexMap.size()) {
                            continue; // Other keys found, do not accept
                        }
                    }

                    //We've now satisfied that we have a valid result
                    subnodeWithDataFound = true;
                    changesToBePosted.put(subnode, emptyMap ? null : true);
                }
            }

            final boolean finalSubnodeResult = subnodeWithDataFound; // Fix the final result in place

            return new Callable() {
                @Override
                public Object call() throws Exception {
                    directory.getExtendedPropertyMap().put(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY, finalSubnodeResult);
                    for (FSNode key : changesToBePosted.keySet()) {
                        extendedProperties.get(key).put(FTMConstants.FSN_LATEST_SEARCH_QUERY_RESULT_KEY, changesToBePosted.get(key));
                    }

                    return null;
                }
            };
        }, true, "Searching...", null, null);
    }

    /**
     * Executes further file analysis, per folder in an unspecified order.
     *
     * @param postProcessingMethod The method that is to be ran right after processing
     */
    public void executeFurtherAnalysis(Runnable postProcessingMethod) {
        if (hasNotEnumerated() || !isIdle(true)) {
            throw new IllegalStateException("Analysis may not be executed before enumeration");
        }
        wrapperSafeDirectoryPostProcessing((FSNode dirNode, Map<FSNode, ObservableMap<String, Object>> keys) -> {
            final HashMap<ObservableMap<String, Object>, AnalysisResultObject> changeableResults = new HashMap<>();

            for (FSNode f : keys.keySet()) {
                // Retrieve the analyzer object where available
                ObservableMap<String, Object> propertyMap = keys.get(f);
                Object o = propertyMap.get(FTMConstants.FSN_COLLATING_FINDER_STORAGE_KEY);
                if (o == null || !(o instanceof CollatingFileTypeFinder)) {
                    continue; // No valid CTF available
                }
                CollatingFileTypeFinder ctf = (CollatingFileTypeFinder) o;

                if (!ctf.advanceState()) {
                    continue; // Unable to advance forward, leave type data as is
                }
                // OK. Pull in the latest data
                changeableResults.put(propertyMap, ctf.getLatestAnalysisResult());
            }

            return new Callable() {
                @Override
                public Object call() throws Exception {
                    changeStateString("Analyzed folder: " + dirNode.getUnderlyingFile().getAbsolutePath());

                    for (ObservableMap<String, Object> map : changeableResults.keySet()) {
                        map.put(FTMConstants.FSN_ANALYSIS_RESULT_OBJECT_KEY, changeableResults.get(map));
                    }

                    return null;
                }
            };
        }, false, "Executing in-depth analysis...", null, postProcessingMethod);
    }

    /**
     * Commits a read only analysis for the <code>FSNode</code> tree. This is ran on its own
     * thread, and upon termination, it will report itself as complete. Any
     * interruptions will result in an incomplete run, but those are indicated
     * by a suitable state change
     *
     * @param analysisFunction The function that is fed each FSNode file, one at
     * a time
     * @param statusMessage The status message to show while processing
     * @param postProcessCall If applicable, this call is called in the wrapper
     * to permit some UI-sensitive operations. This is called the very last,
     * after any observable variables have changed
     */
    public void simpleFilePostProcessing(Consumer<FSNode> analysisFunction, String statusMessage, Runnable postProcessCall) {
        if (hasNotEnumerated() || !isIdle(true)) {
            throw new IllegalStateException("Readonly analysis may only be committed when idle");
        }
        if (analysisFunction == null) {
            throw new IllegalArgumentException("Consumer function may not be null!");
        }
        try {
            changeStateInWrapper(currentState, State.POSTPROCESSING);

            // Change the status message if applicable
            if (statusMessage != null) {
                changeStateString(statusMessage);
            }

            // Initialize the analysis thread
            Thread analysisThread = new Thread(new Runnable() {
                private void analyzeNode(FSNode f) throws InterruptedException {
                    if (requestToStop) {
                        throw new InterruptedException("Interrupted at the start of a cycle");
                    }
                    for (FSNode subNode : f.getSubnodeProperty().get()) {
                        if (subNode.isDirectory()) {
                            analyzeNode(subNode);
                        } else {
                            analysisFunction.accept(subNode);
                        }
                    }
                }

                @Override
                public void run() {
                    try {
                        analyzeNode(treeManagerRootNode);

                        // Complete
                        changeStateString(null);
                        taskAboutToStop(State.IDLE_SUCCESS);

                        if (postProcessCall != null) {
                            wrapperGenerator.wrapCallable(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    postProcessCall.run();
                                    return null;
                                }
                            }).call();
                        }

                    } catch (InterruptedException ie) {
                        changeStateString("");
                        changeStateInWrapper(currentState, State.STOPPED_MAY_RETRY);
                    } catch (Exception e) {
                        changeStateString("Postprocessing stopped due to an exception inside processing thread: " + e.toString());
                        changeStateInWrapper(currentState, State.STOPPED_MUST_REINIT);
                    }

                }
            });

            analysisThread.setName("DiskArchaeologist-AnalysisThread");
            setAsManagedThreadAndStart(analysisThread);

        } catch (Exception e) {
            changeStateString("Postprocessing stopped due to an exception: " + e.toString());
            changeStateInWrapper(currentState, State.STOPPED_MUST_REINIT);
        }

    }

    /**
     * Starts an analysis thread; it asynchronusly updates the <code>FSNode</code> tree until
     * interrupted for one reason or another. The analyzer thread will maintain
     * a weak reference to the <code>FileTreeManager</code>, as to ensure, in eventuality of a
     * reinstantiation, that the <code>FileTreeManager</code> can be garbage collected. In that event,
     * the thread will also exit automatically and cease updating the tree
     */
    public void startScanAndAnalyze() {
        if (this.currentState.get() != State.NOT_STARTED) {
                throw new IllegalStateException("An enumeration can be ran only once, and must be started from an idle state");
        }
        
        try {
            
            changeStateInWrapper(currentState, State.ENUMERATING);
            Thread scanThread = new Thread(new FileTreeEnumerator(treeManagerRootNode)); // Start an enumeration
            scanThread.setName("DiskArch-Enumerator");
            setAsManagedThreadAndStart(scanThread);
        } catch (Exception e) {
            changeStateString("Scanning stopped due to an exception: " + e.toString());
            changeStateInWrapper(currentState, State.STOPPED_MUST_REINIT);
        }
    }

    /**
     * Checks if the current state value is something that implies an active <code>FileTreeManager</code>
     * (in other words; if this returns false, <code>FileTreeManager</code> is either idle or has
     * experienced an error)
     *
     * @return TRUE if active, FALSE if idle or stopped by error
     */
    public boolean hasActiveState() {
        State state = this.currentState.getValue();
        return !(state == State.NOT_STARTED || state == State.IDLE_SUCCESS || state == State.STOPPED_MAY_RETRY || state == State.STOPPED_MUST_REINIT);
    }

    /**
     * Returns TRUE if the state is "clean" - aka, it does not indicate any
     * abnormality
     *
     * @return TRUE if clean, FALSE if not
     */
    public boolean hasCleanState() {
        State state = this.currentState.getValue();
        return state != State.STOPPED_MUST_REINIT;
    }

    /**
     * Returns knowledge if there's an active task which has not declared it has
     * stopped safely or interrupted.
     *
     * @return TRUE if there is a task defined, running and not declared as
     * stopped safely
     */
    public boolean hasActiveTask() {
        return currentManagedTaskThread != null && currentManagedTaskThread.isAlive() && !taskStoppedCleanly && !requestToStop;
    }

    /**
     * Returns a boolean describing if enumeration has not been started at all
     * @return TRUE if so, FALSE if in progress or complete
     */
    public boolean hasNotEnumerated() {
        return this.currentState.get() == State.NOT_STARTED;
    }
    
    /**
     * Returns TRUE if <code>FileTreeManager</code> is cleanly idle; as in, there is no task thread
     * running, and the state matches appropriately. 
     * <br>
     * It may return FALSE if there is no thread but a running state (an error condition if
     * persistent), or when there is an active thread but the state implies
     * idleness (an error condition always). If a task thread has declared
     * itself to be safely stopped, it will be considered stopped even if
     * technically alive.
     *
     * @param mustBeRestartable If TRUE, rejects if the state is
     * STOPPED_MUST_REINIT - which means that the machine is indeed idle, but
     * stopped for an error.
     * @return TRUE if condition met, FALSE otherwise
     */
    public boolean isIdle(boolean mustBeRestartable) {
        return (!hasActiveState()) && (!hasActiveTask()) && (!mustBeRestartable || hasCleanState());
    }

    /**
     * Change the value of an observable property so that it is wrapped in the
     * wrapper generator (e.g JavaFX thread)
     *
     * @param <X> Type of value
     * @param property Property to change
     * @param newValue New value of that property
     */
    private <X> void changeStateInWrapper(ReadOnlyObjectWrapper<X> property, X newValue) {
        try {
            Callable<Void> callable = wrapperGenerator.wrapCallable(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    property.set(newValue);
                    return null;
                }
            });

            callable.call();
        } catch (Exception e) {
            e.printStackTrace();
            //This should not fail in most circumstances. If it does, then just assume something broke and stop all
            this.currentState.setValue(State.STOPPED_MUST_REINIT);
        }
    }

    private void changeStateString(String newValue) {
        try {
            Callable<Void> callable = wrapperGenerator.wrapCallable(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    stateString.set(newValue);
                    return null;
                }
            });

            callable.call();
        } catch (Exception e) {
            e.printStackTrace();
            //This should not fail in most circumstances. If it does, then just assume something broke and stop all
            this.currentState.setValue(State.STOPPED_MUST_REINIT);
        }
    }

    /**
     * Gets the observable property for the state
     *
     * @return State as an observable object property
     */
    public ReadOnlyObjectProperty<State> getStateProperty() {
        return currentState.getReadOnlyProperty();
    }

    /**
     * Returns a state string. This is used to: 1) Indicate status occurences
     * DURING execution (state will indicate current processing) 2) Indicate
     * special messages to stoppages (applied BEFORE a state change to a stopped
     * state)
     *
     * If there is nothing to indicate, it will be null.
     *
     * @return The state string
     */
    public ReadOnlyStringProperty getStateStringProperty() {
        return stateString.getReadOnlyProperty();
    }

    /**
     * Waits until the current task thread has terminated; if the thread becomes
     * interrupted or nothing is running, returns immediately
     *
     * @throws InterruptedException An InterruptedException may be thrown, if
     * the thread waiting is interrupted
     */
    public void waitUntilStopped() throws InterruptedException {
        if (currentManagedTaskThread != null && currentManagedTaskThread.isAlive()) {
            currentManagedTaskThread.join();
        }
    }

    /**
     * Requests that the current thread interrupt its task gently.
     *
     * @param shouldWait If TRUE, we should not return until the thread has
     * terminated
     * @throws java.lang.InterruptedException If this thread is interrupted for
     * one reason or another while waiting
     */
    public void requestInterrupt(boolean shouldWait) throws InterruptedException {
        if (currentManagedTaskThread != null && currentManagedTaskThread.isAlive()) {
            requestToStop = true;
            if (shouldWait) {
                currentManagedTaskThread.join();
            }
        }
    }

    /**
     * Gets the applicable root node for this <code>FileTreeManager</code>
     *
     * @return The root node of the currently enumerated structure
     */
    public FSNode getRootNode() {
        return treeManagerRootNode;
    }

    /**
     * Return the latest grid we have
     *
     * @return The latest grid
     */
    public FileGrid getLatestGrid() {
        return latestGrid;
    }
}
