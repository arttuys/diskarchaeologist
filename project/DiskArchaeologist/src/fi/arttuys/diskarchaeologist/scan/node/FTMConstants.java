/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.node;

/**
 *
 * A helper class to contain certain constants used by FileTreeManager. This class is solely used to help clarify the true dependencies of different classes
 * 
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class FTMConstants {

    /**
     * For any potential GUI renderers; this key will hold the alternative
     * identifier that could be displayed in place of a file name; e.g in case
     * of the root node, a full path
     */
    public static final String FSN_ALTERNATIVE_NAME_KEY = "ftm.alternative_display_name"; //If there is an alternate display name that should be shown by the file tree displayer, what it is?
    /**
     * The key for a collating finder. <strong>Contrary to most keys, this key
     * is NOT guaranteed to be wrapper-safe; this is due to perfomance
     * reasons.</strong>
     */
    public static final String FSN_COLLATING_FINDER_STORAGE_KEY = "ftm.internal_collating_finder";
    /**
     * If this key appears with a TRUE value in a node, it is considered to be a
     * part of the current search results. If nonexistent, inteprete implicitly
     * as a result that should be shown. Guaranteed to be changed only from the
     * wrapper/UI thread
     */
    public static final String FSN_LATEST_SEARCH_QUERY_RESULT_KEY = "ftm.flagged_in_latest_query";
    /**
     * The key used for the latest result. The usage of this key carries a
     * guarantee of being safe to bind from GUI classes, as all operations to
     * this very key are committed using a wrapper
     */
    public static final String FSN_ANALYSIS_RESULT_OBJECT_KEY = "ftm.latest_analysis_result";
    
    /**
     * This class should not be initializable
     */
    private FTMConstants() {}
    
}
