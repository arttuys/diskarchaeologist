/*
 * The MIT License
 *
 * Copyright 2017 Arttu Ylä-Sahra.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package fi.arttuys.diskarchaeologist.scan.node;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;
import javafx.util.Pair;

/**
 *
 * A FileGrid is intended to lay out files in a grid format, satisfying
 * following conditions: 
 * <ol>
 * <li>There is a minimum size, and no files below this
 * size may be included in the grid</li> 
 * <li>If you click on a point on the grid, you
 * MAY (but not always will) find a file.</li> 
 * <li>Each FSNode will respectively be
 * pointed to the grid object, which will have a hashmap to point to a given
 * position</li> 
 * <li>In relation to 3, all FSNodes are continuous on the grid - one
 * node may not be disjointly placed. It is possible to traverse the whole area
 * of a node without stepping over foreign nodes.</li>
 * </ol>
 * While this class is publicly accessible to all, it can only be instantiated
 * by a <code>FileTreeManager</code>, which will also automatically populate the grid.
 * 
 * A grid can be in two states: <ol>
 * <li>Populating: the grid is being populated, and
 * no definite locations have yet been assigned to nodes. This is so that
 * certain optimizations can be made to avoid leftover empty space.</li>
 * <li> Completed: all containing nodes have definite locations, and every nonempty
 * location has a definite node. The grid can not be changed after the grid is
 * in a completed state</li>
 * </ol>
 * 
 * The class is designed for performance optimization over memory usage; due to
 * the large amount of nodes typically seen, even small performance penalties
 * can cause major impact
 *
 * @date 26/5/2017
 * @author Arttu Ylä-Sahra
 */
public class FileGrid {

    /**
     * The hashmap that holds the pairings between a node, and the points on the grid
     */
    private HashMap<FSNode, List<Pair<Integer, Integer>>> nodeToGridMap;
    /**
     * The table that respectively holds the matching node for a specific
     * coordinate
     */
    private FSNode[][] gridToNodeTable;
    /**
     * X-size of the grid
     */
    private int xSize;
    /**
     * Y-size of the grid
     */
    private int ySize;

    /**
     * If we assume each file is a 1x1 point, how much can we store?
     */
    private BigInteger maxCount;

    /**
     * The minimum size for a file to be considered; due to space limitations,
     * we necessarily can not fit all the files on the grid, so we need to use
     * some criteria of importance. The classic one (e.g used in WinDirStat) is
     * the size, and that's what we'll be using here. The minimum size will
     * correspond to one 1x1 square.
     */
    private BigInteger minimumSize;

    /**
     * An intermediate construct; before setting up the definite grid, all data
     * is collected to this list, which allows us to optimize the sizes on the
     * grid without clumsy moving-around needed. It is important that the order
     * is kept as to not scramble the directory structures, and therefore we
     * need to use a list with a definite order
     */
    private ArrayList<Pair<FSNode, BigInteger>> sizeList;

    /**
     * To facilitate proper search features, we will also store indirect
     * references via directories; for a directory, this map leads to a
     * collection of files which are in this grid
     */
    private HashMap<FSNode, Set<FSNode>> indirectReferenceList;

    /**
     * Are we complete yet? If TRUE yes, otherwise no, we are still populating
     */
    private boolean isComplete;

    /**
     * Initialize a grid.
     *
     * @param xSize The X size
     * @param ySize The Y size
     * @param minimumSize The minimum size, as pre-determined by the instanting
     * class. This size is considered to be the equivalent of one square
     */
    protected FileGrid(int xSize, int ySize, BigInteger minimumSize) {
        if (xSize < 1 || ySize < 1) {
            throw new IllegalArgumentException("X,Y must both be more than one");
        }
        if (minimumSize == null) {
            throw new IllegalArgumentException("Minimum size must be non-null!");
        }

        this.xSize = xSize;
        this.ySize = ySize;

        this.maxCount = new BigInteger(Integer.toString(xSize)).multiply(new BigInteger(Integer.toString(ySize)));
        this.minimumSize = minimumSize;
        // Initialize the node table
        gridToNodeTable = new FSNode[xSize][ySize];
        for (int x = 0; x < xSize; x++) {
            for (int y = 0; y < ySize; y++) {
                gridToNodeTable[x][y] = null;
            }
        }

        // Initialize the node hashmap
        nodeToGridMap = new HashMap<>();

        // Initialize the indirect reference list
        indirectReferenceList = new HashMap<>();

        // Initialize the size list
        sizeList = new ArrayList<>();

        // Set us as incomplete
        isComplete = false;
    }

    /**
     * Adds an indirect reference to a given file node, going through all the
     * way up to the topmost node
     *
     * @param node Node to be indirectly referenced
     */
    private void addIndirectReference(FSNode node) {
        FSNode parent = node.getParentNode();

        // While we can process parent nodes
        while (parent != null) {
            Set<FSNode> listOfNodes = indirectReferenceList.get(parent);
            if (listOfNodes == null) {
                //Nonexistent? Create one
                listOfNodes = new HashSet<>();
                indirectReferenceList.put(parent, listOfNodes);
            }

            listOfNodes.add(node); // Set guarantees we will not get duplicates, so no need to check for that.
            parent = parent.getParentNode(); // Jump up to the parent node
        }
    }

    /**
     * Adds a node to the staging/populating list, automatically calculating its
     * approximate expected size
     *
     * @param node Node to add
     */
    protected synchronized void addNode(FSNode node) {
        if (isComplete) {
            throw new IllegalStateException("The grid is complete, no changes may be executed!");
        }
        if (node == null || node.isDirectory()) {
            throw new IllegalArgumentException("Node must not be null nor a directory!");
        }
        if (node.getLocalSizeProperty().get().compareTo(this.minimumSize) == -1) {
            throw new IllegalArgumentException("This file is smaller than the minimum size!");
        }

        if (maxCount.compareTo(new BigInteger(Integer.toString(this.sizeList.size() + 1))) == -1) {
            throw new IllegalStateException("This grid is full, and can not fit any more files!");
        }

        // OK. Add first to the indirect reference list
        addIndirectReference(node);

        // Calculate the approximate size, ensuring that the size is at least one
        BigInteger size = BigInteger.ONE.max(node.getLocalSizeProperty().get().divide(BigInteger.ONE.max(minimumSize)));

        // Add to the size registry
        sizeList.add(new Pair<>(node, size));
    }

    /**
     * Adjusts the intermediate sizes so that the items exactly line up to the
     * maximum grid size
     */
    private void adjustSizes() {
        if (sizeList.isEmpty()) {
            return; // Nothing to do, empty.
        }
        // Figure out the total size of the sizes found
        BigInteger totalSize = sizeList.stream().map((Pair<FSNode, BigInteger> t) -> {
            return t.getValue();
        }).reduce(BigInteger.ZERO, (BigInteger t, BigInteger u) -> {
            return t.add(u);
        });

        // Get the difference from the expected value and the actual value. This expression allows an easy construction:
        // If the actual value is over the expected value, the result will be negative; and taking the sign function of that, we can easily count to the correct direction
        BigInteger diff = maxCount.subtract(totalSize);
        BigInteger dir = new BigInteger(Integer.toString(diff.signum()));

        // Next, build an index array to accelerate the editing process. Due to the Java's primitive types, we need to sort it in a slightly complex fashion
        int[] indexes = IntStream.range(0, sizeList.size()).boxed().sorted((o1, o2) -> {
            return sizeList.get(o1).getValue().compareTo(sizeList.get(o2).getValue()) * -1; // Compare the size values, and prefer large over small
        }).mapToInt(i -> i).toArray();

        // Start from zero
        int currentIndex = 0;

        // As there are less/the same amount of files that there are pixels, we can guarantee that we can eventually find some solution 
        while (!diff.equals(BigInteger.ZERO)) {
            // Get item at the current index
            Pair<FSNode, BigInteger> pair = sizeList.get(indexes[currentIndex]);

            if (!pair.getValue().equals(BigInteger.ONE) || dir.equals(BigInteger.ONE)) { //Process this only if: 1) it is not at one, or 2) we are adding to sizes, which have no bounds and can be therefore done in any case
                Pair<FSNode, BigInteger> newPair = new Pair<>(pair.getKey(), pair.getValue().add(dir));
                sizeList.set(indexes[currentIndex], newPair); // OK. Set as the new pair
                diff = diff.subtract(dir); // Change the diff, namely subtract the difference
            }

            // Move on to the next valid index
            currentIndex = ((currentIndex + 1) % indexes.length);
        }
    }

    /**
     * Triggers completion of the grid, namely calculating the exact placements
     * for nodes
     */
    protected synchronized void completeGrid() {
        adjustSizes(); // Adjust sizes so that the grid matches as full as possible. Unless the grid is empty, it is now guaranteed that the contents will exactly line up

        // We will walk through the grid, and try to place files on the grid by gradually allocating them their places
        // Each of these moves will be tried by turn
        List<Pair<Integer, Integer>> movementList = new ArrayList<>();
        movementList.add(new Pair<>(0, 1)); // Down
        movementList.add(new Pair<>(0, -1)); // Up
        movementList.add(new Pair<>(1, 0)); // Right

        int currentGridX = 0;
        int currentGridY = -1;

        for (Pair<FSNode, BigInteger> pair : sizeList) {
            BigInteger placesRemaining = pair.getValue();
            List<Pair<Integer,Integer>> positions = new ArrayList<>();

            // While we must still find places to assign
            while (placesRemaining.compareTo(BigInteger.ZERO) == 1) {
                Integer deltaX = null;
                Integer deltaY = null;

                // Try to find a move
                for (Pair<Integer, Integer> move : movementList) {
                    int newX = currentGridX + move.getKey();
                    int newY = currentGridY + move.getValue();

                    if (newX < 0 || newY < 0) {
                        continue; // Out of bounds
                    }
                    if (newX >= xSize || newY >= ySize) {
                        continue; // Out of bounds
                    }
                    if (gridToNodeTable[newX][newY] != null) continue; //Not empty, not selectable
                    //OK. Select these

                    deltaX = move.getKey();
                    deltaY = move.getValue();
                    break;
                }

                if (deltaX == null || deltaY == null) {
                    throw new IllegalStateException("Unable to align pieces on a grid - unable to select a move."); // This should not happen, but instead of failing silently, cause an exception
                }
                
                // OK, set our current position to the new pos
                currentGridX += deltaX;
                currentGridY += deltaY;
                
                // Add to our list
                positions.add(new Pair<>(currentGridX, currentGridY));
                
                // Assign to the grid table
                gridToNodeTable[currentGridX][currentGridY] = pair.getKey();
                
                // Subtract from the places remaining
                placesRemaining = placesRemaining.subtract(BigInteger.ONE);
            }
            
            // OK, now all that remains is assigning a list of positions to the map
            nodeToGridMap.put(pair.getKey(), Collections.unmodifiableList(positions));
        }

        isComplete = true; // We've completed this now!
    }
    
    /**
     * Returns a read-only view to the mapping between the node and the positions on the grid. If the grid population has not been completed, throws an exception
     * @return A mapping
     */
    public Map<FSNode, List<Pair<Integer,Integer>>> getNodeToGridMap() {
        if (!isComplete) throw new IllegalStateException("Grid is still being populated - do not use yet!");
        return Collections.unmodifiableMap(this.nodeToGridMap);
    }
    
    /**
     * Accesses the node grid; for each grid position, there may or may not be a node
     * @param x X
     * @param y Y
     * @return A node, or NULL if not available / out of grid
     */
    public FSNode pointAtGrid(int x, int y) {
        if (x < 0 || y < 0) return null;
        if (x >= xSize || y >= ySize) return null;
        return gridToNodeTable[x][y];
    }
    
    /**
     * Gets a mapping of indirect references; this contains directories encountered, and each directory maps to a set of file nodes under this directory. This may be useful for, say, highlighting all relevant files when a directory is selected
     * @return A mapping
     */
    public Map<FSNode, Set<FSNode>> getIndirectReferences() {
        if (!isComplete) throw new IllegalStateException("Grid is still being populated - do not use yet!");
        return Collections.unmodifiableMap(this.indirectReferenceList);
    }
    
    /**
     * Returns the width of this grid
     * @return Width
     */
    public int getGridWidth() {
        return this.xSize;
    }
    
    /**
     * Returns the height of this grid
     * @return Height
     */
    public int getGridHeight() {
        return this.ySize;
    }
}
