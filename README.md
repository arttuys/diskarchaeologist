This program is an university course project designed to analyze folder structures for interesting files and patterns; it was designed from ground up to facilitate in-depth analysis which studies file contents.

This repository is a copy of the final version sent in for review, plus the requisite manual _(only in Finnish)_ for the program.

# To run

To run, you will need __NetBeans__ and a __recent version of Java with JavaFX__. After obtaining those, it should be as simple as opening the project files in the _project_ folder, and running.

# Notes

__This program is still fairly incomplete.__ Due to a lack of time, there aren't really any too notable analysis components. In other words: the walls, the roof and the office furniture exists - but the actual analysts are still missing. There are also several architectural designs (e.g. lack of multithreading) that would benefit from optimization and refactoring.

When running tests, one should use the _test-temp_ directory as their working directory; that directory contains a test file structure, which is required for certain structural tests to work properly.