% !TeX encoding = utf8

\documentclass[utf8,a4paper,12pt]{report}
\usepackage{tgadventor}
\usepackage[utf8]{inputenc}
\usepackage[finnish]{babel}
\usepackage{titlesec}
\usepackage{csquotes}
\usepackage{graphicx}
\usepackage{hyperref}

\graphicspath{ {gfx/} }

\titleformat{\chapter}[display]{\normalfont\bfseries}{}{0pt}{\Large}

\setlength{\parskip}{0.5em}
\begin{document}

\title{Käyttöohje ``DiskArchaeologist''-ohjelmointityölle}
\author{Arttu Ylä-Sahra (arttuys@arttuys.fi)}
\date{\today}

\maketitle

\tableofcontents

\chapter{Käyttöohje}

\section{Perusteet}

\subsection{Mitä ohjelma tekee?}\label{prog_target}

 Parhaan selityksen ohjelman luonteesta tarjoaa oheinen lainaus syksyllä 2016 laaditusta aihe-ehdotusasiakirjasta kurssityöksi hyväksymiseksi:

\begin{displayquote}
\emph{
Ohjelman (työnimi ”Disk Archaeologist”) ideana on toimia eräänlaisena isojen tiedosto- ja kansiokokonaisuuksien tutkimis- ja analyysityökaluna. Ohjelman tarkoituksena on auttaa vastaamaan tietynlaisiin kysymyksiin, kuten: 
\begin{itemize}
\item{Mitkä tiedostotyypit syövät eniten tilaa, ja missä?}
\item{Onko jossain päin levyä suurta määrää pieniä, hitaasti kopioitavia (ja ehkäpä turhia) tiedostoja?} 
\item{Missä on .ISO-levykuva jonka levyn otsake on XYZ?}
\item{Mistä päätteettömästä tekstitiedostosta löytyy jotain, joka täsmää regexpiin \emph{''(suklaa \textbar chocolate)[?!]*''} ?} 
\item{Mitkä kuvatiedostot liittyvät kameraan sarjanro. 9201481?}
\end{itemize}
 Aihe on valittu henkilökohtaisten tarpeen pohjalta; tarkemmin ottaen siitä syystä, että haluan analysoida omasta hyllystä löytyviä, vanhoja ja hyvin täysiä kovalevyjä. Tämä on käsityönä erittäin hidasta, joten tarvitsen ohjelmaa joka osaa itsenäisesti tutkia hakemistopuita ja kertoa mikäli se löytää jotakin mitä olen määritellyt ”kiinnostavaksi”
}
\end{displayquote}

Tiivistetysti siis, \emph{DiskArchaeologist} on työkalu suurien tiedostomäärien kartoittamiseen ja tutkimiseen. Monista muista vastaavista ohjelmista \emph{DiskArchaeologist} poikkeaa esimerkiksi seuraavin tavoin:

\begin{itemize}
\item{\textbf{Käyttöjärjestelmäriippumattomuus}: ohjelma on toteutettu Javalla pelkästään standardikirjastoa käyttäen, eikä siten vaadi esim. tiettyä käyttöjärjestelmää. Ohjelma ei myöskään tee mitään oletuksia käy\-ttö\-jär\-jes\-tel\-män suhteen.}
\item{\textbf{Pura ja suorita}: ohjelma ei vaadi mitään erillisiä asennuksia tai säätö\-tiedostoja; pelkkä kopio JAR-ohjelma\-tiedostosta riittää}
\item{\textbf{Syväanalyysimahdollisuus}: vaikka tässä kehitysvaiheessa tiedostotulkkeja ei juuri vielä olekaan, ohjelma on rakennettu erityisesti tarkempia hakuja silmälläpitäen. Ohjelma kykenee tekemään myös tiedostojen sisältöön kohdistuvia suureanalyysejä ja pystyy hyödyntämään niitä esim. hakulausekkeiden yhteydessä}
\item{\textbf{Helppo- ja varmatoiminen}: ohjelma on rakennettu mutkattomuus edellä, ja lähtökohtaisesti kenen tahansa perustiedot tiedostojärjestelmien toiminnasta omaavan tulisi kyetä käyttämään myös tätä ohjelmaa. Ohjelma on myöskin yritetty rakentaa mahdollisimman vakaaksi, että käyttäjän ei tarvitse pelätä teknisiä ongelmia}
\item{\textbf{Havainnollinen}: Isoja tiedostomassoja on mahdollista esittää graafisesti, siten että pikaisella vilkaisulla on mahdollista purkaa paljon tietoa}
\end{itemize}

\subsection{Järjestelmävaatimukset ja käynnistämisohjeet}

Ohjelma vaatii toimiakseen tuoreen, vähintään Java 8:n tai vastaavaa tukevan suoritusympäristön, ja JavaFX-kirjastot. Muita erikoisedellytyksiä ei ole; ohjelma ei vaadi järjestelmänvalvojan oikeuksia, eikä edes kirjoitusoikeuksia mihinkään kansioon. Mitään erityisiä muita riippuvuuksia ei myöskään ole, joten ohjelman tulisi toimia missä tahansa edellämainitut vaatimukset täyttävässä Java-suoritusympäristössä

Ohjelman mukana ei välttämättä saavu erillistä asennusohjelmaa; ohjelma voidaan käynnistää esimerkiksi kaksoisklikkaamalla JAR-tiedostoa Windowsin resurssienhallinnassa, tai vaihtoehtoisesti komentoriviltä. Ohjelma ei myöskään tallenna mitään asetustiedostoja tai vaadi niitä käynnistyäkseen, joten pelkän ohjelmatiedoston kopiointi on riittävä toimenpide esim. edelleenjakamiseen tai varmuuskopiointiin - ohjelman voi myös käynnistää suoraan esim. CD-levyltä ilman ongelmia.

\subsection{Lisensointi ja vastuuvapauslauseke}

Ohjelma on lisensoitu \emph{MIT}-lisenssillä, ja on siten avointa lähdekoodia. Tiivistetysti tämä lisenssi sallii useimmat yleiset käyttötarkoitukset kaupallisuudesta riippumatta, ja ohjelmaa tahi sen johdannaisia saa jakaa vapaasti. Ehtona on tekijänoikeusmerkintöjen säilyttäminen ja lisenssin mukana jakaminen. 

\emph{Tämä ohjelma toimitetaan sellaisena kuin se on, ilman minkäänlaista takuuta tai sitomusta toimivuudesta, sopivuudesta johonkin käyttötarkoitukseen tai esim. ohjelmistopatenttisuojan rikkomattomuudesta. Vaikka ohjelma onkin pyritty rakentamaan ja testaamaan kohtuullisissa harjoitustyölle sopivissa rajoissa ettei näitä ongelmia syntyisi, käytät ohjelmaa silti täysin omalla vastuullasi. Ohjelman käytön ehtona on tämän vastuuvapauden hyväksyminen!}

\section{Ohjelman yleisesittely}
\nopagebreak
Mikäli ohjelma on käynnistynyt oikein, avautuu ulkomuodoltaan seuraavanlaisen kaltainen pääikkuna; kuvassa esitetyssä ikkunassa on jo suoritettu kansion tutkinta. Välittömästi ohjelman käynnistyksen jälkeen näkyvässä ikkunassa ei ole näkyvissä hakemistopuuta tai tiedostospektriä, ja lisäksi tekstit sekä nappien valittavuus eroavat.

\includegraphics[width=\linewidth]{hinted-main.png}

Käydään seuraavaksi läpi kuvaan numeroituja elementtejä numerojärjestyksessä. Alaluvun numero on sama kuin elementin numero.

\subsection{Toimintopalkki}

\includegraphics[width=\linewidth]{bar.png}

Tästä palkista ohjataan ohjelman päätoimintoja.

\begin{itemize}
\item{\textbf{Select folder...} (valitse hakemisto): avaa hakemistonvalintaikkunan, josta hakemiston valitsemalla skannaus alkaa}
\item{\textbf{Deep analyze} (syväanalyysi): käynnistää hakemistopuun svyäanalyysin; ensimmäisellä skannauksella tiedostoja tarkastellaan vain pintapuolisesti tutkimatta niiden sisältöä, kun taas syväanalyysissä tarkastellaan myös tiedostojen sisältöä, poimien niistä mielenkiintoisia suureita}
\item{\textbf{Interrupt operation} (keskeytä toiminto): yrittää keskeyttää nykyisen toiminnon, mikäli tämä on mahdollista. Keskeytymisen onnistumista ei taata, vaan tämä painike ainoastaan lähettää pyynnön keskeyttää toiminto hallitusti.}
\end{itemize}

\subsection{Hakemistopuuselain}

\includegraphics[width=\linewidth]{tree.png}

Tämä osio esittää hakemistorakenteen puuna, jota pystyy selaamaan. Mikäli valitset jonkin puun osion, koko kyseistä kansiota tai tiedostoa kuvaava kohta korostuu seuraavassa alaluvussa esitellyssä \emph{tiedostospektrissä}.

Seuraavaksi eri sarakkeiden merkitykset

\begin{itemize}
\item{\textbf{Name} (nimi): tiedoston tai kansion nimi; ylimmän kansion tapauksessa koko polku kyseiseen kansioon}
\item{\textbf{Total size} (kokonaiskoko): kyseisen tiedoston tai kansion kokonaiskoko; kansion kokonaiskoko lasketaan tiedostokokojen yksinkertaisena summana. Tämä tarkoituksella jättäen huomiotta kansion intristisen koon, joka on keskimäärin varsin pieni ja hankaloittaisi muuten laskutoimituksia}
\item{\textbf{Subitems} (alatietueet): kuinka monta alatietuetta kansiolla on rekursiivisesti laskettuna; alatietueeksi lasketaan mikä tahansa tiedosto tai kansio, joka on hakemistopuussa määrätyn kansion sisällä}
\item{\textbf{Type} (tyyppi): tiedoston lyhyt tyyppimääritelmä}
\item{\textbf{Query match} (hakuosuma): kuvastaa sitä onko tiedostossa hakuosuma, tai sitä että sisältääkö kansio positiivisia hakuosumia. Jos tämän kohdalla ei lue mitään, hakua ei ole tehty. \textbf{true} tarkoittaa että haku täsmää tähän tiedostoon tai kansioon, \textbf{false} puolestaan sitä että haku ei kosketa tätä tiedostoa tai kansiota}
\end{itemize}

\subsection{Tiedostospektrografi}\label{manual:spectrograph}

\includegraphics[width=\linewidth]{spectre-closeup.png}

Tämä elementti esittää hakemistorakenteen eräänlaisena \emph{spektrinä}. Eri värit kuvastavat eri tiedostotyyppejä, ja mitä repaleisempi jokin väri/\emph{raita} on, sitä enemmän pieniä tiedostoja kyseisestä tyypistä löytyy toistensa läheltä. Kahden raidan läheisyys merkitseekin sitä että näiden kahden tyypin edustamat tiedostot ovat hakemistohyppyinä laskettuna suhteellisen lähellä toisiaan.

Kuvan spektristä huomaame esimerkiksi sen, että kansion XML-tiedostoja ei ole kovin paljon ja ne ovat suurehkoja kooltaan (oliivinvihreät raidat, esim. numeron 3 alla yleiskuvassa), kun taas HTML-tiedostoja (kirkkaanvihreät raidat, suurimmasta oliviinvihreästä raidasta vasemmalle) on määrällisesti todella paljon suhteessa niiden kokonaiskokoon.

Mikäli painat jotain kohtaa tiedostospektrissä, valitaan se tiedosto jota kyseinen kohta koskee. Tämä kohta korostuu spektrissä, ja lisäksi kyseinen tiedoston sijainti paljastetaan hakemistopuuselaimessa nähtäväksi.

On huomattava, että tiedostospektri \emph{ei välttämättä sisällä kaikkia tiedostoja} - mikäli tiedosto on todella pieni verrattuna tiedostojen kokonaiskokoon ja spektrille annettuun tilaan, sitä ei välttämättä edes yritetä sovittaa spektrille.

\subsection{Tyyppiluettelo}

\includegraphics[width=\linewidth]{types.png}

Tämä luettelo kuvastaa nykyisestä puusta löydettyjä eri tiedostotyyppejä. Tyyppinimien värit ovat samat kuin mitä tiedostospektrissä, ja mikäli valitset jonkin tyypin tästä luettelosta, kaikki tätä tyyppiä olevat tiedostot korostuvat tiedostospektristä.

\begin{itemize}
\item{\textbf{Type name} (tyypin nimi): lyhyt jotain tiettyä tiedostotyyppiä kuvaava otsikko.}
\item{\textbf{Count}: kuinka monta samalla otsikolla olevaa tyyppiä on tunnistettu }
\end{itemize}

\subsection{Tiedostokohtaiset tilastot}

\includegraphics[width=\linewidth]{stats.png}

Tämä ruudukko esittelee tietuekohtaisia tilastoja; kun valitset hakemistopuuselaimesta jonkin tietueen, tähän puuhun avautuu tietoja kyseisestä tiedostosta.

\begin{itemize}
\item{\textbf{File} (tiedosto): yleistietoja tiedostosta; nimi, polku, koko tiedostojärjestelmästä (voi kansiolla olla eri kuin sisällön koko), sisältökoko, ja täsmäävyys nykyiseen hakuun}
\item{\textbf{Analysis} (analyysi): tietoja analyysin tuloksista; esimerkkikuvassa tiedostoa ei ole syväanalysoitu (siitä merkintä \emph{potential}; \emph{basic} ja \emph{advanced} tarkoittaisivat tarkempaa tulosta, ja \emph{rejected} sitä ettei mikään tunnistanut tiedostoa), eikä tarkempia tuloksia ole siten saatavilla. Tiedosto on kuitenkin päätteen perusteella tunnistettu suhteellisen varmasti XML-tiedostoksi} 
\end{itemize}

\subsection{Edistynyt haku}
\includegraphics[width=\linewidth]{search.png}

Tätä laatikkoa käyttämällä voit suorittaa erilaisia hakuja tiedostopuuhun. Syöttämällä \emph{hakuavaimen} hakasulkeisiin, ja sopivan \emph{säännöllisen lausekkeen} kulmasulkeisiin perään, voit tehdä monimutkaisiakin hakuoperaatioita. Mikäli monta avain-lausekeparia on syötetty, on niiden \emph{kaikkien} täytyttävä - eli siis, jokaiselle tiedostolle on löydyttävä annetun hakuavaimen arvosta jotain, joka täsmää annettuun säännölliseen lausekkeeseen. Ylläolevan kuvan esimerkkihaku valitsee kaikki \emph{.xml}-päätteiset tiedostot, koosta riippumatta (haun regex sallii mitkä tahansa merkit).

Hakuavain voi olla:
\begin{enumerate}
\item{\textbf{Name} (tiedostonimi): hae tiedostonimen perusteella}
\item{\textbf{Size} (tiedostokoko): hae \emph{ihmisluettavan koon} perusteella - eli siis, vertaa hakemistopuuselaimessa näkyvän \emph{Size}-sarakkeen sisältöön}
\item{Mikä tahansa analyysiavain: jokaisella suureella on jokin nimi, ja niitä voi käyttää hakuavaimena} 
\end{enumerate}

On huomattava tiettyjä teknisiä rajoitteita; syöttötavasta johtuen esim. hakasulkeita \emph{ei} voi toistaiseksi käyttää lausekkeissa, joten esim. merkkiluokkien käyttäminen ei onnistu. Lisäksi on huomioitava haun kesto; liian monimutkaisissa hauissa voi kestää todella kauan, jos lausekkeet ovat kovin monimutkaisia.

Kun haku on loppunut, tiedostospektristä korostetaan haussa löytyneet tiedostot. Tämä korostus \emph{ei häviä} vaikka muita valintoja tehtäisiinkin; jotta haun tuottama korostus häviäisi, tulee suorittaa haku tyhjällä syötteellä. Tämä nollaa hakutulostiedot puurakenteesta, takaisin samanlaiseen tilaan kuin ennen ensimmäistä hakua.

\subsection{Tilapalkki}

\includegraphics[width=\linewidth]{status.png}

Tilapalkissa näytetään ohjelman nykyinen tila lyhyesti. Seuraavaksi joitakin yleisimpiä tilailmoituksia ja niiden selityksiä

\begin{itemize}
\item{\textbf{Idle} (jouten): ohjelma ei tee tällä hetkellä mitään, ja on valmis toimenpiteisiin}
\item{\textbf{Enumeration in progress...} (kartoitus käynnissä): ohjelma kartoittaa hakemistopuuta, ja lisää sitä näkyville käyttäjälle}
\item{\textbf{Scanning folder (...)} (skannataan hakemistoa ...): ohjelma on juuri kartoittamassa tilapalkissa näkyvää hakemistoa}
\item{\textbf{Successfully enumerated, ready for deeper analysis.} (kartoitus valmis, syväanalyysi voidaan aloittaa): hakemistopuu on nyt alustavasti kartoitettu, ja syväanalyysin tekeminen on nyt mahdollista}
\item{\textbf{Collecting statistics on file types..} (kerätään tiedostotyyppitilastoja): ohjelma kerää tiedostotyyppien määriä tyyppitaulukkoa varten}
\item{\textbf{Generating a grid view...} (luodaan ruudukkonäkymää): ohjelma koostaa tiedostospektriä ruudukkoon, jotta se voidaan esittää käyttäjälle}
\item{\textbf{Searching...} (haetaan): ohjelma suorittaa hakua annetuilla hakulausekkeilla}
\item{\textbf{Executing in-depth analysis...} (suoritetaan syväanalyysiä): ohjelma suorittaa tiedostojen syväanalyysiä ja koostaa niiden sisältöä}
\end{itemize}

Joskus voi myös käydä niin että ohjelma kohtaa ongelmia suorituksensa aikana. Seuraavaksi joitakin yleisimpiä ilmoituksia, mitä tilapalkkiin voi ilmestyä näissä tilanteissa.

\begin{itemize}

\item{\textbf{Interrupted} (keskeytynyt): nykyinen toimenpide on syystä tai toisesta keskeytynyt poikkeavalla tavalla (mahdollisesti käyttäjän keskeytyspyyntöön). Toiminto kuitenkin voidaan aloittaa uudelleen}
\item{\textbf{Interrupted, please restart file enumeration} (keskeytynyt, aloita tiedostokartoitus uudelleen): sama kuin yllä, mutta tiedostokartoitus on tehtävä keskeytyksen laadun vuoksi uudelleen}
\item{\textbf{Exception while enumerating: (...). Enumeration stopped. / scanning stopped due to an exception} (poikkeus kartoittaessa syystä ... kartoitus pysäytetty / skannaus pysäytetty poikkeuksen vuoksi): Hakemistokartoituksessa tapahtui tilapalkissa kuvattu poikkeama, ja toiminto on siksi keskeytynyt}
\item{\textbf{Postprocessing stopped due to an exception} (jälkikäsittely pysäytetty poikkeuksen vuoksi): jälkikäsittelyoperaatiossa tapahtui poikkeama, joka keskeytti toiminnon}
\end{itemize}

Tilapalkki onkin erityisen tärkeä seurata, sillä se kertoo olennaisia tietoja ohjelman senhetkisestä tilasta ja toiminnasta. Joissakin erityisen poikkeavissa tilanteissa tilapalkin viesti voidaan toistaa myös erillisessä virheikkunassa.

\section{Kysymyksiä ja vastauksia ongelmatilanteisiin}

Ohessa hyödyllisiä, mahdollisesti eteen tulevia kysymyksiä ja vastauksia ongelmiin liittyen.

\textbf{Apua, tämä ohjelma heitti eteeni virheviestin ponnahdusikkunassa, mitä teen?}

Älä hätäänny; virheikkuna on ohjelman tapa varoittaa äkillisistä ja arvaamattomista virheistä. Joskus nämä virheet eivät ole vaarallisia (esim. tiedostoa ei löydy), mutta useimmiten ne kuitenkin ovat sellaisia jotka vaarantavat ohjelman vakauden.

 Voit kokeilla, virheviestin luettuasi, oman harkintasi mukaisesti jatkaa ohjelman käyttöä - mutta on erittäin suositeltavaa käynnistää ohjelma uudelleen jonkin tälläisen odottamattoman virheen sattuessa.

\textbf{Ohjelma nykii kovasti, miten voin parantaa suorituskykyä?}

Ohjelma käsittelee hakemistosta riippuen huomattavia määriä tiedostoja yhtenä isona rakenteena, ja vaatii siten sekä runsaasti muistia että saatavilla olevia prosessointitehoja. Erityisesti spektrin koostaminen on vaativaa, ja voi koosta riippuen vaatia huomattavankin ajan esim. koonmuutoksen yhteydessä. Jatkuvat suuret rakennemuutokset voivat jättää muistiin suuuren määrän roskaa, jonka siivoaminen myös hidastaa ohjelman toimintaa. Myös esim. skannatun verkkoaseman hitaus voi hidastaa ohjelman toimintaa.

Kokeile ensimmäisenä pienempiä hakemistorakenteita tai ikkunan koon pienentämistä, ja viimeisenä konstina ohjelman uudelleenkäynnistämistä. Mikäli nämä toimenpiteet eivät auta, voi syynä yksinkertaisesti olla tietokoneen heikko teho tai esim. juuri edellämainittu viive verkkoasemien yhteydessä.

\textbf{Voiko ohjelma tahtomatta muokata tiedostoja tai aiheuttaa muita arvaamattomia sivuvaikutuksia? Onko ohjelmaa turvallista käyttää forensista tarkkuutta vaativiin tutkimuksiin?}

\emph{Ei yleisimmissä tilanteissa}, ohjelma voi korkeintaan avata tiedoston ja lukea sen sisältöä, mutta ei koskaan muokkaa sitä tai poista sitä. Ohjelmaan ei yksinkertaisesti ole koodattu toimintoja tätä varten, joten edes kaatumisen ei pitäisi tätä aiheuttaa. Tämä siis \emph{normaalien} tiedostojen kanssa; mikäli kyseessä on jokin \emph{erikoistiedosto}, eivät yllämainitut lainalaisuudet välttämättä ole voimassa koska erikoistiedostojen kanssa jopa lukeminen voi aiheuttaa sivuvaikutuksia muualla (esim. satunnaisgeneraattorivirta).

Ohjelmalle \emph{ei anneta mitään takuuta} digitaaliseen forensiikkaan perustuviin tutkimuksiin soveltumisesta, ja käyttö siihen on edellämainittujen lisenssiehtojen mukaisesti on täysin käyttäjän omalla vastuulla.

\textbf{Ohjelma kaatuilee tietyn hakemistorakenteen kanssa, missä vika?}

Mikäli ohjelmalla on ongelma jonkin tietyn hakemiston kanssa, on vika todennäköisimmin jossakin hakemistopuun tiedostossa tai sitten jossakin muussa hakemiston erikoisominaisuudessa (esim. dynaamisesti generoituva, loputtoman syvä puu). Kokeile siirtyä alikansioihin ja paikantaa mahdollinen ongelmakohta. 

\textbf{Tiedostosta ei saa ulos analyysituloksia tai se kaataa ohjelman, miksi?}

Tämä on todennäköisimmin merkki viallisesta, bugisesta analyysimodulista ja/tai kummallisesti käyttäytyvästä tahi rikkinäisestä tiedostosta. Kokeile selvittää, mikä tiedostossa on ''epätavallista''

\textbf{Löysin vian, ja nyt osaan särkeä ohjelman aina halutessani palasiksi! / Mitäs jos toiminto X, se olisi kiva?}

Onnitteluni tästä - ilmoitathan tästä sähköpostitse minulle. Tätä ohjelmaa ei välttämättä huolleta kovin aktiivisesti (jos laisinkaan), mutta kehitysehdotukset ovat siitä huolimatta aina tervetulleita.

Lisäksi, lisenssiehtojen mukaisesti, sinulla on täysi oikeus ottaa kehitystyö haltuusi ja alkaa kehittämään omaa varianttiasi tästä ohjelmasta, jos haluat parannuksesi nopeimmiten yleisön tietoon ja käyttöön :)

\section{Lopetus}

Ohjelma voidaan sulkea yksinkertaisesti pääikkuna sulkemalla; mahdolliset toiminnot keskeytyvät samalla automaattisesti.

Tähän myös päättyy tämä käyttöohje. Toivottavasti se auttoi sinua ohjelman käyttämisessä ja sen toiminnan ymmärtämisessä :)

\end{document}